import org.gradle.api.tasks.testing.logging.TestExceptionFormat.*
import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
   `java-library`
   id("io.skerna.libs.gradle.base")
   id("org.graalvm.buildtools.native").version("0.9.0")
       `maven-publish`
}
fun version(moduleName: String) = project.property(moduleName)
fun kotlinx(module: String) = "org.jetbrains.kotlinx:kotlinx-$module:${version("kotlin_coroutines_version")}"


packageSpec{
  metadata {
    organization("skerna")
    description("Arch core, this library provide modular jvm software development")
    versionControlRepository("https://gitlab.com/skerna/arch-project/arch-core")
    websiteUrl("https://gitlab.com/skerna/arch-project/arch-core")
    licences {
    }
    developers {
      developer(email = "ronfravi", name="Ronald Cardenas")
    }
  }
}

repositories {
  mavenLocal()
  mavenCentral()
}

dependencies {
  //implementation(platform("io.skerna.arch:arch-platform:${project.version}"))
  api("javax.annotation:javax.annotation-api:1.3.2")
  api("com.vdurmont:semver4j:3.1.0")
  api("javassist:javassist:3.12.1.GA")
  api("io.github.classgraph:classgraph:4.8.90")
  api("org.ow2.asm:asm:9.2")
  api("com.github.zafarkhaja:java-semver:0.9.0")
  api("org.json:json:20210307")
  api("io.skerna.libs:logger-jvm:0.1.0")
  api("org.atteo.classindex:classindex:3.4")
  testImplementation("org.junit.jupiter:junit-jupiter:${version("junit_version")}")
  testImplementation("org.junit.jupiter:junit-jupiter-engine:${version("junit_version")}")
  testImplementation("org.junit.platform:junit-platform-launcher:1.5.2")
  testImplementation("org.slf4j:slf4j-api:1.7.5")
  testImplementation("org.apache.logging.log4j:log4j-core:2.11.2")
  testImplementation("org.apache.logging.log4j:log4j-slf4j-impl:2.11.2")
  testImplementation("org.hamcrest:hamcrest:2.2")
  testImplementation("org.hamcrest:hamcrest-core:2.2")
  testImplementation("org.mockito:mockito-core:3.12.4")
  testImplementation("com.google.guava:guava:20.0")
  testImplementation("com.google.truth:truth:0.30")
  testImplementation("com.google.testing.compile:compile-testing:0.10")
}

java {
  targetCompatibility=JavaVersion.VERSION_11
  sourceCompatibility=JavaVersion.VERSION_11
}
tasks.named<Test>("test") {
  useJUnitPlatform()
 testLogging {
        events(FAILED, STANDARD_ERROR, SKIPPED)
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.skerna.arch"
            artifactId = "arch-core"
            version = "0.1.5"
            from(components["java"])
        }
    }
    repositories  {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/23434284/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = "NV9AKzwRLK1rmGiyxkNB"
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
