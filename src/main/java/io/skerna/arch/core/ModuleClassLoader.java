package io.skerna.arch.core;

import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Optional;

/**
 * One instance of this class should be created by moduleInstance manager for every available plug-in.
 * By default, this class loader is a Parent Last ClassLoader - it loads the classes from the moduleInstance's jars
 * before delegating to the parent class loader.
 * Use {@link #parentFirst} to change the loading strategy.
 * <p>
 * *
 */
public class ModuleClassLoader extends URLClassLoader {

  private static final String JAVA_PACKAGE_PREFIX = "java.";
  private static final String PLUGIN_PACKAGE_PREFIX = "io.skerna.arch.";
  protected final Logger log = LoggerFactory.logger(ModuleClassLoader.class);
  private ModuleManager pluginManager;
  private ModuleDescriptor pluginDescriptor;
  private boolean parentFirst;


  public ModuleClassLoader(ModuleManager pluginManager, ModuleDescriptor pluginDescriptor, ClassLoader parent) {
    // TODO por defecto se ha cambiado ha parent first
    this(pluginManager, pluginDescriptor, parent, true);
  }

  /**
   * If {@code parentFirst} is {@code true}, indicates that the parent {@link ClassLoader} should be consulted
   * before trying to load the a class through this loader.
   */
  public ModuleClassLoader(ModuleManager pluginManager, ModuleDescriptor pluginDescriptor, ClassLoader parent, boolean parentFirst) {
    super(new URL[0], parent);

    this.pluginManager = pluginManager;
    this.pluginDescriptor = pluginDescriptor;
    this.parentFirst = parentFirst;
  }

  @Override
  public void addURL(URL url) {
    log.atDebug().log("Add '{}'", url);
    super.addURL(url);
  }

  public void addFile(File file) {
    try {
      addURL(file.getCanonicalFile().toURI().toURL());
    } catch (IOException e) {
//            throw new RuntimeException(e);
      log.atError().log(e.getMessage(), e);
    }
  }

  private ClassLoader getReference() {
    return this;
  }

  /**
   * By default, it uses a child first delegation model rather than the standard parent first.
   * If the requested class cannot be found in this class loader, the parent class loader will be consulted
   * via the standard {@link ClassLoader#loadClass(String)} mechanism.
   * Use {@link #parentFirst} to change the loading strategy.
   */
  @Override
  public Class<?> loadClass(String className) throws ClassNotFoundException {
    synchronized (getClassLoadingLock(className)) {
      // first check whether it's a system class, delegate to the system loader
      if (className.startsWith(JAVA_PACKAGE_PREFIX)) {
        return findSystemClass(className);
      }

      // if the class is part of the moduleInstance engine use parent class loader
      if (className.startsWith(PLUGIN_PACKAGE_PREFIX) && !className.startsWith("io.skerna.arch")) {
//                log.atTrace().log("Delegate the loading of PF4J class '{}' to parent", className);
        return getParent().loadClass(className);
      }

      log.atTrace().log("Received request to load class '{}' , '{}'", className,this);

      // second check whether it's already been loaded
      Class<?> loadedClass = findLoadedClass(className);
      if (loadedClass != null) {
        log.atTrace().log("Found loaded class '{}'", className);
        return loadedClass;
      }

      if (!parentFirst)  {
        log.atTrace().log("Not found try load using loader {}",this);
        // nope, try to load locally
        try {
          loadedClass = findClass(className);
          log.atTrace().log("Found class '{}' in moduleInstance classpath", className);
          return loadedClass;
        } catch (ClassNotFoundException e) {
          // try next step
        }

        // look in dependenciespluginDescriptor
        loadedClass = loadClassFromDependencies(className);
        if (loadedClass != null) {
          log.atTrace().log("Found class '{}' in dependencies", className);
          return loadedClass;
        }

        log.atTrace().log("Couldn't find class '{}' in moduleInstance classpath. Delegating to parent", className);

        // use the standard ClassLoader (which follows normal parent delegation)
        return super.loadClass(className);
      } else {
        // try to load from parent
        try {
          return super.loadClass(className);
        } catch (ClassCastException e) {
          // try next step
        }

        log.atTrace().log("Couldn't find class '{}' in parent. Delegating to moduleInstance classpath", className);

        // nope, try to load locally
        try {
          loadedClass = findClass(className);
          log.atTrace().log("Found class '{}' in moduleInstance classpath", className);
          return loadedClass;
        } catch (ClassNotFoundException e) {
          // try next step
        }

        // look in dependencies
        loadedClass = loadClassFromDependencies(className);
        if (loadedClass != null) {
          log.atTrace().log("Found class '{}' in dependencies", className);
          return loadedClass;
        }

        throw new ClassNotFoundException(className);
      }
    }
  }

  /**
   * Load the named resource from this moduleInstance.
   * By default, this implementation checks the moduleInstance's classpath first then delegates to the parent.
   * Use {@link #parentFirst} to change the loading strategy.
   *
   * @param name the name of the resource.
   * @return the URL to the resource, {@code null} if the resource was not found.
   */
  @Override
  public URL getResource(String name) {
    log.atTrace().log("Received request to load resource '{}'", name);
    if (!parentFirst) {
      URL url = findResource(name);
      if (url != null) {
        log.atTrace().log("Found resource '{}' in moduleInstance classpath", name);
        return url;
      }

      log.atTrace().log("Couldn't find resource '{}' in moduleInstance classpath. Delegating to parent", name);

      return super.getResource(name);
    } else {
      URL url = super.getResource(name);
      if (url != null) {
        log.atTrace().log("Found resource '{}' in parent", name);
        return url;
      }

      log.atTrace().log("Couldn't find resource '{}' in parent", name);

      return findResource(name);
    }
  }

  protected Class<?> loadClassFromLocal(String className) throws ClassNotFoundException {
    Class loadedClass = null;
    loadedClass = findClass(className);
    log.atTrace().log("Found class '{}' in moduleInstance classpath", className);
    return loadedClass;
  }

  protected Class<?> loadClassFromDependencies(String className) {
    log.atTrace().log("Search in dependencies for class '{}'", className);
    List<ModuleDependecy> dependencies = pluginDescriptor.getDependencies();
    for (ModuleDependecy dependency : dependencies) {
        Optional<ClassLoader> classLoader = pluginManager.getPluginLoaderManager().getPluginClassLoader(dependency.getPluginId());

      // If the dependency is marked as optional, its class loader might not be available.
      if (classLoader.isPresent() == false && dependency.isOptional()) {
        continue;
      }

      // Since allowing several plugins per Jar (classloader url) , avoid recursion and search in dependecy that
      // have same classloader
      if (getReference().equals(classLoader.get())) {
        log.atDebug().log("Attemp seach in same classloader infinite vertx detect");
        continue;
      }
      try {
        Class aClass = classLoader.get().loadClass(className);
        if (aClass == null) {
          log.atWarning().log("Unable resolve class {} from dependencies with pluginDescriptor {}", className, pluginDescriptor.toString());
        }
        return aClass;
      } catch (ClassNotFoundException e) {
        log.atWarning().log("Unable resolve class {} from dependencies with pluginDescriptor {} ", className, pluginDescriptor.toString(), e);
        // try next dependency
      }
    }

    return null;
  }

}
