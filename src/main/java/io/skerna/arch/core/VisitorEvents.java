package io.skerna.arch.core;


public interface VisitorEvents {

  void onInstalled(ModuleStateEvent event);

  void onUninstalled(ModuleStateEvent event);

  void onCreated(ModuleStateEvent event);

  void onDisabled(ModuleStateEvent event);

  void onResolved(ModuleStateEvent event);

  void onStarted(ModuleStateEvent event);

  void onStopped(ModuleStateEvent event);
}
