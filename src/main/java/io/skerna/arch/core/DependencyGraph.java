package io.skerna.arch.core;

import io.skerna.arch.core.util.DirectedGraph;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.*;

/**
 * This class builds a dependency graph for a list of plugins (descriptors).
 * The entry point is the {@link #resolve(List)} method, method that returns a {@link Result} object.
 * The {@code Result} class contains nice information about the result of resolve operation (if it's a cyclic dependency,
 * they are not found dependencies, they are dependencies with wrong version).
 * This class is very useful for if-else scenarios.
 * <p>
 * Only some attributes (moduleId, dependencies and pluginVersion) from {@link ModuleDescriptor} are used in
 * the process of {@code resolve} operation.
 * <p>
 * *
 */
public class DependencyGraph {
  private static final Logger log = LoggerFactory.logger(DependencyGraph.class);

  private VersionManager versionManager;

  private DirectedGraph<ModuleID> dependenciesGraph; // the value is 'moduleId'
  private DirectedGraph<ModuleID> dependentsGraph; // the value is 'moduleId'
  private DirectedGraph<ModuleID> configurationsGraph;

  private boolean resolved;

  public DependencyGraph(VersionManager versionManager) {
    this.versionManager = versionManager;
  }

  public Result resolve(List<ModuleDescriptor> plugins) {
    // create graphs
    dependenciesGraph = new DirectedGraph<>();
    dependentsGraph = new DirectedGraph<ModuleID>();
    configurationsGraph = new DirectedGraph<>();



    // populate graphs
    Map<ModuleID, ModuleDescriptor> pluginByIds = new HashMap<>();
    for (ModuleDescriptor plugin : plugins) {
      addPlugin(plugin);
      pluginByIds.put(plugin.getModuleID(), plugin);
    }

    log.atDebug().log("Graph: {}", dependenciesGraph);

    // get a sorted list of dependencies
    List<ModuleID> sortedPlugins = dependenciesGraph.reverseTopologicalSort();
    log.atDebug().log("Plugins order: {}", sortedPlugins);

    // create the result object
    Result result = new Result(sortedPlugins);

    resolved = true;

    if (sortedPlugins != null) { // no cyclic dependency
      // detect not found dependencies
      for (ModuleID moduleId : sortedPlugins) {
        if (!pluginByIds.containsKey(moduleId)) {
          result.addNotFoundDependency(moduleId);
        }
      }
    }

    // check dependencies versions
    for (ModuleDescriptor plugin : plugins) {
      ModuleID moduleId = plugin.getModuleID();
      String existingVersion = plugin.getVersion();

      ArrayList<ModuleID> dependents = new ArrayList<>(getDependents(moduleId));
      while (!dependents.isEmpty()) {
        ModuleID dependentId = dependents.remove(0);
        ModuleDescriptor dependent = pluginByIds.get(dependentId);
        String requiredVersion = getDependencyVersionSupport(dependent, moduleId);
        boolean ok = checkDependencyVersion(requiredVersion, existingVersion);
        if (!ok) {
          result.addWrongDependencyVersion(new WrongDependencyVersion(moduleId, dependentId, existingVersion, requiredVersion));
        }
      }
    }

    return result;
  }

  /**
   * Retrieves the plugins ids that the given moduleInstance id directly depends on.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return
   */
  public List<ModuleID> getDependencies(ModuleID moduleId) {
    checkResolved();
    return dependenciesGraph.getNeighbors(moduleId);
  }

  /**
   * Retrieves the plugins ids that the given content is a direct dependency of.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return
   */
  public List<ModuleID> getDependents(ModuleID moduleId) {
    checkResolved();
    return dependentsGraph.getNeighbors(moduleId);
  }

  /**
   * Check if an existing version of dependency is compatible with the required version (from moduleInstance descriptor).
   *
   * @param requiredVersion
   * @param existingVersion
   * @return
   */
  protected boolean checkDependencyVersion(String requiredVersion, String existingVersion) {
    return versionManager.checkVersionConstraint(existingVersion, requiredVersion);
  }

  private void addPlugin(ModuleDescriptor descriptor) {
    ModuleID moduleId = descriptor.getModuleID();
    List<ModuleDependecy> dependencies = descriptor.getDependencies();
    if (dependencies.isEmpty()) {
      dependenciesGraph.addVertex(moduleId);
      dependentsGraph.addVertex(moduleId);
    } else {
      boolean edgeAdded = false;
      for (ModuleDependecy dependency : dependencies) {
        // Don't register optional plugins in the dependency graph to avoid automatic disabling of the moduleInstance,
        // if an optional dependency is missing.
        if (!dependency.isOptional()) {
          edgeAdded = true;
          dependenciesGraph.addEdge(moduleId, dependency.getPluginId());
          dependentsGraph.addEdge(dependency.getPluginId(), moduleId);
        }
      }

      // Register the moduleInstance without dependencies, if all of its dependencies are optional.
      if (!edgeAdded) {
        dependenciesGraph.addVertex(moduleId);
        dependentsGraph.addVertex(moduleId);
      }
    }
  }

  private void checkResolved() {
    if (!resolved) {
      throw new IllegalStateException("Call 'resolve' method first");
    }
  }

  private String getDependencyVersionSupport(ModuleDescriptor dependent, ModuleID dependencyId) {
    List<ModuleDependecy> dependencies = dependent.getDependencies();
    for (ModuleDependecy dependency : dependencies) {
      if (dependencyId.equals(dependency.getPluginId())) {
        return dependency.getPluginVersionSupport();
      }
    }

    throw new IllegalStateException("Cannot find a dependency with id '" + dependencyId +
      "' for moduleInstance '" + dependent.getModuleID() + "'");
  }

  public static class Result {

    private boolean cyclicDependency;
    private List<ModuleID> notFoundDependencies; // value is "moduleId"
    private List<ModuleID> sortedPlugins; // value is "moduleId"
    private List<WrongDependencyVersion> wrongVersionDependencies;

    Result(List<ModuleID> sortedPlugins) {
      if (sortedPlugins == null) {
        cyclicDependency = true;
        this.sortedPlugins = Collections.emptyList();
      } else {
        this.sortedPlugins = new ArrayList<ModuleID>(sortedPlugins);
      }

      notFoundDependencies = new ArrayList<>();
      wrongVersionDependencies = new ArrayList<>();
    }

    /**
     * Returns true is a cyclic dependency was detected.
     */
    public boolean hasCyclicDependency() {
      return cyclicDependency;
    }

    /**
     * Returns a list with dependencies required that were not found.
     */
    public List<ModuleID> getNotFoundDependencies() {
      return notFoundDependencies;
    }

    /**
     * Returns a list with dependencies with wrong version.
     */
    public List<WrongDependencyVersion> getWrongVersionDependencies() {
      return wrongVersionDependencies;
    }

    /**
     * Get the list of plugins in dependency sorted order.
     */
    public List<ModuleID> getSortedPlugins() {
      return sortedPlugins;
    }

    void addNotFoundDependency(ModuleID moduleId) {
      notFoundDependencies.add(moduleId);
    }

    void addWrongDependencyVersion(WrongDependencyVersion wrongDependencyVersion) {
      wrongVersionDependencies.add(wrongDependencyVersion);
    }

  }

  public static class WrongDependencyVersion {

    private ModuleID dependencyId; // value is "moduleId"
    private ModuleID dependentId; // value is "moduleId"
    private String existingVersion;
    private String requiredVersion;

    WrongDependencyVersion(ModuleID dependencyId, ModuleID dependentId, String existingVersion, String requiredVersion) {
      this.dependencyId = dependencyId;
      this.dependentId = dependentId;
      this.existingVersion = existingVersion;
      this.requiredVersion = requiredVersion;
    }

    public ModuleID getDependencyId() {
      return dependencyId;
    }

    public ModuleID getDependentId() {
      return dependentId;
    }

    public String getExistingVersion() {
      return existingVersion;
    }

    public String getRequiredVersion() {
      return requiredVersion;
    }

  }

  /**
   * It will be thrown if a cyclic dependency is detected.
   */
  public static class CyclicDependencyException extends ReactorException {

    public CyclicDependencyException() {
      super("Cyclic dependencies");
    }

  }

  /**
   * Indicates that the dependencies required were not found.
   */
  public static class DependenciesNotFoundException extends ReactorException {

    private List<ModuleID> dependencies;

    public DependenciesNotFoundException(List<ModuleID> dependencies) {
      super("Dependencies '{}' not found", dependencies);

      this.dependencies = dependencies;
    }

    public List<ModuleID> getDependencies() {
      return dependencies;
    }

  }

  /**
   * Indicates that some dependencies have wrong version.
   */
  public static class DependenciesWrongVersionException extends ReactorException {

    private List<WrongDependencyVersion> dependencies;

    public DependenciesWrongVersionException(List<WrongDependencyVersion> dependencies) {
      super("Dependencies '{}' have wrong version", dependencies);

      this.dependencies = dependencies;
    }

    public List<WrongDependencyVersion> getDependencies() {
      return dependencies;
    }

  }

}
