package io.skerna.arch.core;

public class DefaultModuleManagerBuilder extends ModuleManagerBuilder<DefaultModuleManagerBuilder,DefaultModuleManager> {
  @Override
  protected DefaultModuleManager prebuild() {
    return new DefaultModuleManager(this);
  }
}
