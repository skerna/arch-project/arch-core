package io.skerna.arch.core.finders;

import io.skerna.arch.core.*;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.processor.modules.AbstractModuleProcessor;
import io.skerna.arch.core.processor.modules.PluginAnnotationReader;
import io.github.classgraph.*;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.IteratorUtils;
import io.skerna.arch.core.vo.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;


public class ClassgraphModuleDescriptorFinder implements ModuleDescriptorFinder {
  private static final Logger log = LoggerFactory.logger(JsonModuleDescriptorFinder.class);

  private static final String DEFAULT_PROPERTIES_FILE_NAME = "/META-INF/plugins";

  protected String propertiesFileName;
  private Set<AbstractModuleProcessor> processorList = new HashSet<>();

  public ClassgraphModuleDescriptorFinder() {
    this(DEFAULT_PROPERTIES_FILE_NAME);
    this.processorList = IteratorUtils.toSet(ServiceLoader.load(AbstractModuleProcessor.class).iterator());
  }

  public ClassgraphModuleDescriptorFinder(String propertiesFileName) {
    this.propertiesFileName = propertiesFileName;
  }

  public ClassgraphModuleDescriptorFinder addModuleProcessor(AbstractModuleProcessor abstractModuleProcessor) {
    this.processorList.add(abstractModuleProcessor);
    return this;
  }

  @Override
  public Set<ModuleDescriptor> find(SourceModule sourceModule) throws ArchException {
    Set<ModuleDescriptor> descriptors = new HashSet<>();
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    URLClassLoader urlClassLoader = null;
    try {
      URL url[] = new URL[]{sourceFileModule.file.toUri().toURL()};
      urlClassLoader = new URLClassLoader(url);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    ScanResult scanResult = new ClassGraph()
      .enableExternalClasses()
      .enableAnnotationInfo()
      .addClassLoader(urlClassLoader)
      .setMaxBufferedJarRAMSize(8)
      //.overrideClasspath("/lib", sourceFileModule.file.toAbsolutePath() + "!classes")
      .scan();

    ClassInfoList plugins = scanResult.getClassesWithAnnotation(ModuleDefinition.class.getCanonicalName());
    ClassInfoList runners = scanResult.getClassesWithAnnotation(RunWith.class.getCanonicalName());

    for (ClassInfo plugin : plugins) {
      //Module activador = plugin.getAnnotation(Module.class);
      AnnotationInfo annotationInfo = plugin.getAnnotationInfo(ModuleDefinition.class.getCanonicalName());
      AnnotationInfo runWithAnnInfo = findAnnotation(plugin, RunWith.class.getCanonicalName());
      AnnotationInfo buidWithAnnInfo = findAnnotation(plugin, BuildWith.class.getCanonicalName());

      Annotation annotationValue = annotationInfo.loadClassAndInstantiate();
      ModuleDefinition activador = (ModuleDefinition) annotationValue;

      DefaultModuleDescriptor.Builder pluginDescriptorBuilder = fromAnnotation(activador, plugin.getName());

      if (runWithAnnInfo != null) {
        RunWith runWith = (RunWith) runWithAnnInfo.loadClassAndInstantiate();
        pluginDescriptorBuilder.setModuleRunnerID(ModuleRunnerID.of(runWith.value()));
      }
      if (buidWithAnnInfo != null) {
        BuildWith buildWith = (BuildWith) buidWithAnnInfo.loadClassAndInstantiate();
        pluginDescriptorBuilder.setModuleFactoryID(ModuleFactoryID.of(buildWith.value()));
      }
      ModuleDescriptor moduleDescriptor = pluginDescriptorBuilder.build();
      descriptors.add(moduleDescriptor);
      log.atWarning().log("Annotated {} not is assignable to {}", plugin, Module.class);
    }
    return descriptors;
  }

  private AnnotationInfo findAnnotation(ClassInfo classInfo, String annotation) {
    AnnotationInfo annotationInfo = classInfo.getAnnotationInfo(annotation);
    if (annotationInfo != null) {
      return annotationInfo;
    }
    ClassInfo lastSuperClass = null;
    for (ClassInfo superclass : classInfo.getSuperclasses()) {
      lastSuperClass = superclass;
      annotationInfo = superclass.getAnnotationInfo(annotation);
      if (annotationInfo != null) {
        break;
      }
    }

    if (annotationInfo == null && classInfo.getSuperclass() != null) {
      return findAnnotation(classInfo.getSuperclass(), annotation);
    } else {
      return annotationInfo;
    }
  }

  private DefaultModuleDescriptor.Builder fromAnnotation(ModuleDefinition pluginActivador, String className) {
    Map<String, Object> mapValues = PluginAnnotationReader.readPluginActivadorInMap(pluginActivador, className);

    PluginAnnotationReader readerValues = PluginAnnotationReader.ofClass(pluginActivador, className);
    ModuleID moduleID = ModuleID.of(readerValues.moduleId());

    DefaultModuleDescriptor.Builder builder = DefaultModuleDescriptor.builder(moduleID, className)
      .setModuleDescription(readerValues.pluginDescription())
      .setVersion(readerValues.version())
      .setRequires(readerValues.requires())
      .setProvider(readerValues.provider())
      .setModuleDependecies(readerValues.dependecies())
      .setLicence(readerValues.license())
      .setModuleType(ModuleType.valueOf(readerValues.moduleType()));


    return builder;
  }

  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    if (!ClassUtils.isOfType(sourceModule, SourceFileModule.class)) {
      return false;
    }
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    return sourceFileModule.exists() && (sourceFileModule.isDirectory() || sourceFileModule.isJarFile());
  }

}
