package io.skerna.arch.core.finders;

import io.skerna.arch.core.*;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.processor.modules.AbstractModuleProcessor;
import io.skerna.arch.core.processor.modules.DefaultModuleProcessor;
import io.skerna.arch.core.processor.modules.PluginAnnotationReader;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.IteratorUtils;
import io.skerna.arch.core.vo.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.*;


public class InstanceModuleDescriptorFinder implements ModuleDescriptorFinder {
  private static final Logger log = LoggerFactory.logger(InstanceModuleDescriptorFinder.class);
  private Set<AbstractModuleProcessor> processorList = new HashSet<>();

  public InstanceModuleDescriptorFinder(List<AbstractModuleProcessor> moduleProcessors) {
    for (AbstractModuleProcessor moduleProcessor : moduleProcessors) {
      addModuleProcessor(moduleProcessor);
    }
  }

  public InstanceModuleDescriptorFinder() {
    Set<AbstractModuleProcessor> processors = new HashSet();
    processors.add(new DefaultModuleProcessor());
    processors.forEach(this::addModuleProcessor);
  }

  public InstanceModuleDescriptorFinder addModuleProcessor(AbstractModuleProcessor abstractModuleProcessor){
    this.processorList.add(abstractModuleProcessor);
    return this;
  }


  public InstanceModuleDescriptorFinder populateWithServiceLoader(){
    Set<AbstractModuleProcessor> abstractModuleProcessors = IteratorUtils.toSet(ServiceLoader.load(AbstractModuleProcessor.class).iterator());
    for (AbstractModuleProcessor abstractModuleProcessor : abstractModuleProcessors) {
      addModuleProcessor(abstractModuleProcessor);
    }
    return this;
  }

  @Override
  public Set<ModuleDescriptor> find(SourceModule sourceModule) {
    SourceObjectModule sourceObjectModule = (SourceObjectModule) sourceModule;
    Set<ModuleDescriptor> descriptors = new HashSet<>();

    for (AbstractModuleProcessor abstractModuleProcessor : processorList) {
      Class target = abstractModuleProcessor.getTarget();
      Class<?> plugin = sourceObjectModule.instance.getClass();
        //Module activador = plugin.getAnnotation(Module.class);
        Annotation annotationValue = plugin.getAnnotation(target);
        if(annotationValue == null){
          continue;
        }
        ModuleDefinition activador =  abstractModuleProcessor.adaptAnnotation(annotationValue);
        RunWith runWith = plugin.getAnnotation(RunWith.class);
        BuildWith buildWith = plugin.getAnnotation(BuildWith.class);

        DefaultModuleDescriptor.Builder pluginDescriptorBuilder = fromAnnotation(activador, plugin.getCanonicalName());


        if(runWith == null){
          throw new ArchException("ERROR_MODULER_RUNNER")
            .appendErrorCode("ERROR_MODULER_RUNNER_NOT_DEFINED")
            .appendMessage("Module ID" +pluginDescriptorBuilder.moduleId)
            .appendMessage("Module class " + pluginDescriptorBuilder.moduleClass);
        }
        if(buildWith == null){
          throw new ArchException("ERROR_MODULE_BUILDER")
            .appendErrorCode("ERROR_MODULER_BUILDER_NOT_DEFINED")
            .appendMessage("Module ID" +pluginDescriptorBuilder.moduleId)
            .appendMessage("Module class " + pluginDescriptorBuilder.moduleClass);
        }

        if (Module.class.isAssignableFrom(plugin)) {
            pluginDescriptorBuilder.setModuleRunnerID(ModuleRunnerID.of(runWith.value()))
            .setModuleFactoryID(ModuleFactoryID.of(buildWith.value()))
            .build();
          ModuleDescriptor moduleDescriptor = pluginDescriptorBuilder.build();
          descriptors.add(moduleDescriptor);
        } else {
          log.atWarning().log("Annotated {} not is assignable to {}", plugin, Module.class);
        }
    }

    return descriptors;
  }

  private DefaultModuleDescriptor.Builder fromAnnotation(ModuleDefinition pluginActivador, String className) {
    Map<String, Object> mapValues = PluginAnnotationReader.readPluginActivadorInMap(pluginActivador, className);


    PluginAnnotationReader readerValues = PluginAnnotationReader.ofClass(pluginActivador, className);
    ModuleID moduleID = ModuleID.of(readerValues.moduleId());

    DefaultModuleDescriptor.Builder builder =  DefaultModuleDescriptor.builder(moduleID,className)
      .setModuleDescription(readerValues.pluginDescription())
      .setVersion(readerValues.version())
      .setRequires(readerValues.requires())
      .setProvider(readerValues.provider())
      .setModuleDependecies(readerValues.dependecies())
      .setLicence(readerValues.license())
      .setModuleType(ModuleType.valueOf(readerValues.moduleType()));


    return builder;
  }


  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    return ClassUtils.isOfType(sourceModule,SourceObjectModule.class);
  }

}
