package io.skerna.arch.core.finders;

import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ReactorException;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CompoundModuleDescriptorFinder implements ModuleDescriptorFinder {
  private static final Logger log = LoggerFactory.logger(CompoundModuleDescriptorFinder.class);

  private List<ModuleDescriptorFinder> finders = new ArrayList<>();

  public CompoundModuleDescriptorFinder add(ModuleDescriptorFinder finder) {
    if (finder == null) {
      throw new IllegalArgumentException("null not allowed");
    }

    finders.add(finder);

    return this;
  }

  public int size() {
    return finders.size();
  }

  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    for (ModuleDescriptorFinder finder : finders) {
      if (finder.isApplicable(sourceModule)) {
        return true;
      }
    }

    return false;
  }

  /**
   * find, find method return {@link Set< ModuleDescriptor >}
   *
   * @param sourceModule
   * @return
   * @throws ReactorException
   */
  @Override
  public Set<ModuleDescriptor> find(SourceModule sourceModule) throws ReactorException {
    // lazy initi
    List<Exception> collectionExceptions = null;
    Set<ModuleDescriptor> descriptorSet = new HashSet<>();
    for (ModuleDescriptorFinder finder : finders) {
      if (finder.isApplicable(sourceModule)) {
        log.atDebug().log("'{}' is applicable for plugin '{}'", finder, sourceModule);
        try {
          Set<ModuleDescriptor> pluginDescriptors = finder.find(sourceModule);
          if (pluginDescriptors != null) {
            //return pluginDescriptor;
            descriptorSet.addAll(pluginDescriptors);
          }
        } catch (Exception e) {
          System.out.println("--------- JODEr");
          e.printStackTrace();
          if(collectionExceptions == null){
            collectionExceptions = new ArrayList<>();
          }
          collectionExceptions.add(e);
          if (finders.indexOf(finder) == finders.size() - 1) {
            // it's the last finder
            log.atError().log(e.getMessage(), e);
          } else {
            if(e.getMessage()!=null){
              log.atWarning().log(e.getMessage());
            }
            // log the exception and continue with the next finder
            log.atWarning().log("Try to continue with the next finder");
          }
        }
      } else {
        log.atDebug().log("'{}' is not applicable for plugin '{}'", finder, sourceModule);
      }
    }
    if(collectionExceptions!=null){
      FinderDescriptorExcepion finderDescriptorExcepion = (FinderDescriptorExcepion) new FinderDescriptorExcepion()
        .appendMessage(String.format("plugin path '{%s}'",sourceModule));
      collectionExceptions.stream()
        .forEach(finderDescriptorExcepion::addSuppressed);
      throw finderDescriptorExcepion;
    }
    return descriptorSet;

  }

}
