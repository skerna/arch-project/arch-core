//package io.skerna.arch.core.finders;
//
//import io.skerna.arch.core.ArchException;
//import io.skerna.arch.core.ModuleDescriptor;
//import io.skerna.arch.core.ReactorException;
//import io.skerna.arch.core.util.FileUtils;
//import io.skerna.common.logger.Logger;
//import io.skerna.common.logger.LoggerFactory;
//import org.json.JSONObject;
//import org.json.JSONTokener;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.List;
//import java.util.Set;
//
//public class JsonExtModuleDescritorFinder implements ModuleDescriptorFinder {
//  private Logger log = LoggerFactory.logger(JsonExtModuleDescritorFinder.class);
//  private String fileDescriptorDirectory;
//
//  public JsonExtModuleDescritorFinder(String fileDescriptorDirectory) {
//    this.fileDescriptorDirectory = fileDescriptorDirectory;
//  }
//
//  @Override
//  public boolean isApplicable(Path pluginPath) {
//    return Files.exists(pluginPath) && (Files.isDirectory(pluginPath) || FileUtils.isJarFile(pluginPath));
//  }
//
//  @Override
//  public Set<ModuleDescriptor> find(Path pluginPath) throws ReactorException {
//    return null;
//  }
//
//  protected JSONObject readJson(Path pluginPath, String jsonDescriptor) throws ArchException {
//    Path jsonPluginsPath = getJsonPluginFilePath(pluginPath, jsonDescriptor);
//    if (jsonPluginsPath == null) {
//      throw new FinderException("Cannot find the properties path");
//    }
//
//    log.atDebug().log("Lookup plugin descriptor in '{}'", jsonPluginsPath);
//    if (Files.notExists(jsonPluginsPath)) {
//      throw new FinderException(String.format("Cannot find %s' path", jsonPluginsPath));
//    }
//
//    JSONObject pluginsDescriptors = new JSONObject();
//    try (InputStream input = Files.newInputStream(jsonPluginsPath)) {
//      JSONTokener jsonTokener = new JSONTokener(input);
//      pluginsDescriptors = new JSONObject(jsonTokener);
//    } catch (IOException e) {
//      throw new FinderException(e);
//    }
//
//    return pluginsDescriptors;
//  }
//
//  protected Path getJsonPluginFilePath(Path pluginPath, String propertiesFileName) throws ArchException {
//    if (Files.isDirectory(pluginPath)) {
//      return pluginPath.resolve(Paths.get(propertiesFileName));
//    } else {
//      // it's a jar file
//      try {
//        return FileUtils.getPath(pluginPath, propertiesFileName);
//      } catch (IOException e) {
//        throw new FinderException(e);
//      }
//    }
//  }
//
//  protected List<Path> listPluginPath(Path pluginPath, String extension) throws ArchException {
//    if (Files.isDirectory(pluginPath)) {
//
//      return pluginPath.resolve(Paths.get(propertiesFileName));
//    } else {
//      // it's a jar file
//      try {
//        return FileUtils.getPath(pluginPath, propertiesFileName);
//      } catch (IOException e) {
//        throw new FinderException(e);
//      }
//    }
//  }
//}
