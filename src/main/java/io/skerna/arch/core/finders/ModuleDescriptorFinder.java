package io.skerna.arch.core.finders;

import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ReactorException;
import io.skerna.arch.core.vo.SourceModule;

import java.nio.file.Path;
import java.util.Set;

/**
 * Find a plugin descriptor for a plugin path.
 * You can find the plugin descriptor in manifest file {@link JsonModuleDescriptorFinder},
 * properties file {@link JsonModuleDescriptorFinder}, xml file,
 * java services (with {@link java.util.ServiceLoader}), etc.
 * <p>
 */
public interface ModuleDescriptorFinder {

  /**
   * Returns true if this finder is applicable to the given {@link Path}.
   *
   * @param sourceModule
   * @return
   */
  boolean isApplicable(SourceModule sourceModule);

  Set<ModuleDescriptor> find(SourceModule sourceModule) throws ReactorException;

}
