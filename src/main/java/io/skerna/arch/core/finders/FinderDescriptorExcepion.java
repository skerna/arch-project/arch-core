package io.skerna.arch.core.finders;

import io.skerna.arch.core.ArchException;
import org.jetbrains.annotations.Nullable;

public class FinderDescriptorExcepion  extends ArchException {
  public FinderDescriptorExcepion() {
    super("ERROR_MODULE_DESCRIPTOR");
  }

  public FinderDescriptorExcepion(@Nullable String message) {
    super("ERROR_MODULE_DESCRIPTOR", message);
  }

  public FinderDescriptorExcepion(@Nullable String message, @Nullable Throwable cause) {
    super("ERROR_MODULE_DESCRIPTOR", message, cause);
  }

  public FinderDescriptorExcepion(@Nullable String message, @Nullable Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super("ERROR_MODULE_DESCRIPTOR", message, cause, enableSuppression, writableStackTrace);
  }
}
