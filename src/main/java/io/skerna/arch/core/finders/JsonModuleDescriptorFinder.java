package io.skerna.arch.core.finders;

import io.skerna.arch.core.ArchException;
import io.skerna.arch.core.DefaultModuleDescriptor;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ModuleType;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.util.StringUtils;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class JsonModuleDescriptorFinder implements ModuleDescriptorFinder {
  private static final Logger log = LoggerFactory.logger(JsonModuleDescriptorFinder.class);

  private static final String DEFAULT_PROPERTIES_FILE_NAME = "/META-INF/plugins";

  protected String propertiesFileName;

  public JsonModuleDescriptorFinder() {
    this(DEFAULT_PROPERTIES_FILE_NAME);
  }

  public JsonModuleDescriptorFinder(String propertiesFileName) {
    this.propertiesFileName = propertiesFileName;
  }


  @Override
  public Set<ModuleDescriptor> find(SourceModule sourceModule) throws ArchException {
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;

    Set<ModuleDescriptor> mds = new HashSet<>();
    try {
      List<String> rawDescriptors =getPluginDescriptors(sourceFileModule.file);
      for (String file : rawDescriptors) {
        JSONObject jsonObject = readJson(sourceFileModule.file,file);
        ModuleDescriptor description = createPluginDescriptor(jsonObject);
        mds.add(description);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return mds;
  }
  protected List<String> getPluginDescriptors(Path pluginPath) throws IOException {
    List<String> rawDescriptors=new ArrayList<>();
    ZipFile zipFile = new ZipFile(String.valueOf(pluginPath));
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while(entries.hasMoreElements()){
      ZipEntry entry = entries.nextElement();
      if(!entry.isDirectory()&& entry.getName().contains("META-INF/plugins/")){
        rawDescriptors.add(entry.getName());
      }
    }
    return rawDescriptors;
  }

  protected JSONObject readJson(Path pluginPath, String fileEntry) throws ArchException {

    Path jsonPluginsPath = getJsonPluginFilePath(pluginPath, fileEntry);
    if (jsonPluginsPath == null) {
      throw new FinderException("Cannot find the properties path");
    }

    log.atDebug().log("Lookup plugin descriptor in '{}'", jsonPluginsPath);
    if (Files.notExists(jsonPluginsPath)) {
      throw new FinderException().appendMessage("Cannot find '{}' path" + jsonPluginsPath);
    }
    JSONObject pluginsDescriptors = new JSONObject();
    try (InputStream input = Files.newInputStream(jsonPluginsPath)) {
      JSONTokener jsonTokener = new JSONTokener(input);
      pluginsDescriptors = new JSONObject(jsonTokener);
    } catch (IOException e) {
      throw new FinderException(e);
    }

    return pluginsDescriptors;
  }

  protected Path getJsonPluginFilePath(Path pluginPath, String propertiesFileName) throws ArchException {

    if (Files.isDirectory(pluginPath)) {
      return pluginPath.resolve(Paths.get(propertiesFileName));
    } else {
      // it's a jar file
      try {
        return FileUtils.getPath(pluginPath, propertiesFileName);
      } catch (IOException e) {
        throw new FinderException(e);
      }
    }
  }

  protected ModuleDescriptor createPluginDescriptor(JSONObject flatDescritor) {
    String clazz = flatDescritor.optString("pluginclass", "");
    ModuleID id = ModuleID.of(flatDescritor.optString("moduleId", ""));
    DefaultModuleDescriptor.Builder pluginDescriptor = DefaultModuleDescriptor.builder(id, clazz);


    String description = flatDescritor.optString("pluginDescription", "");
    pluginDescriptor.setModuleDescription("");


    String version = flatDescritor.optString("pluginVersion", "");
    if (StringUtils.isNotNullOrEmpty(version)) {
      pluginDescriptor.setVersion(version);
    }

    String provider = flatDescritor.optString("pluginProvider", "");
    pluginDescriptor.setProvider(provider);

    String dependencies = flatDescritor.optString("pluginDependencies", "");
    pluginDescriptor.setModuleDependecies(dependencies);

    String requires = flatDescritor.optString("pluginRequires", "");
    if (StringUtils.isNotNullOrEmpty(requires)) {
      pluginDescriptor.setRequires(requires);
    }

    pluginDescriptor.setLicence(flatDescritor.optString("pluginLicence", ""));

    ModuleType moduleType = ModuleType.valueOf(flatDescritor.optString("moduleType", ""));

    pluginDescriptor.setModuleType(moduleType);


    return pluginDescriptor.build();
  }

  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    if(!ClassUtils.isOfType(sourceModule,SourceFileModule.class)){
      return false;
    }
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    return sourceFileModule.exists() && (sourceFileModule.isDirectory() || sourceFileModule.isJarFile());
  }

}
