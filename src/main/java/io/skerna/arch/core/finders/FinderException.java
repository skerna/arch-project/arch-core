package io.skerna.arch.core.finders;

import io.skerna.arch.core.ArchException;
import org.jetbrains.annotations.Nullable;

public class FinderException extends ArchException {
  public FinderException() {
    super("ERROR_MODULE_FINDER");
  }

  public FinderException(String message) {
    super("ERROR_MODULE_FINDER", message);
  }

  public FinderException(@Nullable Throwable cause) {
    super("ERROR_MODULE_FINDER", cause);
  }


  public FinderException(@Nullable String message, @Nullable Throwable cause) {
    super("ERROR_MODULE_FINDER", message, cause);
  }


  public FinderException(@Nullable String message, @Nullable Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super("ERROR_MODULE_FINDER", message, cause, enableSuppression, writableStackTrace);
  }
}
