package io.skerna.arch.core.finders;

import io.skerna.arch.core.asm.ExtensionInfo;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.*;

public abstract class AbstractExtensionFinder implements ExtensionFinder, ModuleStateObserver {
  private static final Logger log = LoggerFactory.logger(AbstractExtensionFinder.class);

  protected ModuleManager pluginManager;
  protected volatile Map<ModuleID, Set<String>> entries; // cache by moduleId
  protected volatile Map<String, ExtensionInfo> extensionInfos; // cache extension infos by class name
  protected Boolean checkForExtensionDependencies = null;

  public AbstractExtensionFinder(ModuleManager pluginManager) {
    this.pluginManager = pluginManager;
  }

  public abstract Map<ModuleID, Set<String>> readPluginsStorages();

  public abstract Map<ModuleID, Set<String>> readClasspathStorages();

  @Override
  @SuppressWarnings("unchecked")
  public <T> List<ExtensionWrapper<T>> find(Class<T> type) {
    log.atDebug().log("Finding extensions of extension point '{}'", type.getName());
    Map<ModuleID, Set<String>> entries = getEntries();
    List<ExtensionWrapper<T>> result = new ArrayList<>();

    // add extensions found in classpath and plugins
    for (ModuleID moduleId : entries.keySet()) {
      // classpath's extensions <=> moduleId = null
      List<ExtensionWrapper<T>> pluginExtensions = find(type, moduleId);
      result.addAll(pluginExtensions);
    }

    if (entries.isEmpty()) {
      log.atDebug().log("No extensions found for extension point '{}'", type.getName());
    } else {
      log.atDebug().log("Found {} extensions for extension point '{}'", result.size(), type.getName());
    }

    // sort by "ordinal" property
    Collections.sort(result);

    return result;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> List<ExtensionWrapper<T>> find(Class<T> type, ModuleID moduleId) {
    log.atDebug().log("Finding extensions of extension point '{}' for plugin '{}'", type.getName(), moduleId);
    List<ExtensionWrapper<T>> result = new ArrayList<>();

    // classpath's extensions <=> moduleId = null
    Set<String> classNames = findClassNames(moduleId);
    if (classNames == null || classNames.isEmpty()) {
      return result;
    }

    if (moduleId != null) {
      ModuleWrapper pluginWrapper = pluginManager.getModule(moduleId);
      if (ModuleState.STARTED != pluginWrapper.getModuleState()) {
        //return result;
      }

      log.atTrace().log("Checking extensions from plugin '{}'", moduleId);
    } else {
      log.atTrace().log("Checking extensions from classpath");
    }

    ClassLoader classLoader;

    if (moduleId != null) {
      Optional<ClassLoader> loader = pluginManager.getPluginLoaderManager().getPluginClassLoader(moduleId);
      classLoader = loader.orElseGet(() -> getClass().getClassLoader());
    } else {
      classLoader = getClass().getClassLoader();
    }


    for (String className : classNames) {
      try {
        if (isCheckForExtensionDependencies()) {
          // Load extension annotation without initializing the class itself.
          //
          // If optional dependencies are used, the class loader might not be able
          // to load the extension class because of missing optional dependencies.
          //
          // Therefore we're extracting the extension annotation via asm, in order
          // to extract the required plugins for an extension. Only if all required
          // plugins are currently available and started, the corresponding
          // extension is loaded through the class loader.
          ExtensionInfo extensionInfo = getExtensionInfo(className, classLoader);
          if (extensionInfo == null) {
            log.atError().log("No extension annotation was found for '{}'", className);
            continue;
          }

          // Make sure, that all plugins required by this extension are available.
          List<ModuleID> missingPluginIds = new ArrayList<>();
          for (ModuleID requiredPluginId : extensionInfo.getPlugins()) {
            ModuleWrapper requiredPlugin = pluginManager.getModule(requiredPluginId);
            if (requiredPlugin == null || !ModuleState.STARTED.equals(requiredPlugin.getModuleState())) {
              missingPluginIds.add(requiredPluginId);
            }
          }
          if (!missingPluginIds.isEmpty()) {
            StringBuilder missing = new StringBuilder();
            for (ModuleID missingPluginId : missingPluginIds) {
              if (missing.length() > 0) missing.append(", ");
              missing.append(missingPluginId);
            }
            log.atTrace().log("Extension '{}' is ignored due to missing plugins: {}", className, missing);
            continue;
          }
        }

        log.atTrace().log("Loading class '{}' using class loader '{}'", className, classLoader);
        Class<?> extensionClass = classLoader.loadClass(className);

        log.atTrace().log("Checking extension type '{}'", className);
        if (type.isAssignableFrom(extensionClass)) {
          ExtensionWrapper extensionWrapper = createExtensionWrapper(extensionClass);
          result.add(extensionWrapper);
          log.atTrace().log("Added extension '{}' with ordinal {}", className, extensionWrapper.getOrdinal());
        } else {
          log.atTrace().log("'{}' is not an extension for extension point '{}'", className, type.getName());
          if (RuntimeMode.DEVELOPMENT.equals(pluginManager.getRuntimeMode())) {
            checkDifferentClassLoaders(type, extensionClass);
          }
        }
      } catch (ClassNotFoundException e) {
        log.atError().log(e.getMessage(), e);
      }
    }

    if (result.isEmpty()) {
      log.atDebug().log("No extensions found for extension point '{}'", type.getName());
    } else {
      log.atDebug().log("Found {} extensions for extension point '{}'", result.size(), type.getName());
    }

    // sort by "ordinal" property
    Collections.sort(result);

    return result;
  }

  @Override
  public List<ExtensionWrapper> find(ModuleID moduleId) {
    log.atDebug().log("Finding extensions from plugin '{}'", moduleId);
    List<ExtensionWrapper> result = new ArrayList<>();

    Set<String> classNames = findClassNames(moduleId);
    if (classNames.isEmpty()) {
      return result;
    }

    if (moduleId != null) {
      ModuleWrapper pluginWrapper = pluginManager.getModule(moduleId);
      if (ModuleState.STARTED != pluginWrapper.getModuleState()) {
        return result;
      }

      log.atTrace().log("Checking extensions from plugin '{}'", moduleId);
    } else {
      log.atTrace().log("Checking extensions from classpath");
    }

    ClassLoader classLoader;

    if (moduleId != null) {
      Optional<ClassLoader> loader = pluginManager.getPluginLoaderManager().getPluginClassLoader(moduleId);
      classLoader = loader.orElseGet(() -> getClass().getClassLoader());
    } else {
      classLoader = getClass().getClassLoader();
    }

    for (String className : classNames) {
      try {
        log.atDebug().log("Loading class '{}' using class loader '{}'", className, classLoader);
        Class<?> extensionClass = classLoader.loadClass(className);

        ExtensionWrapper extensionWrapper = createExtensionWrapper(extensionClass);
        result.add(extensionWrapper);
        log.atDebug().log("Added extension '{}' with ordinal {}", className, extensionWrapper.getOrdinal());
      } catch (ClassNotFoundException e) {
        log.atError().log(e.getMessage(), e);
      }
    }

    if (result.isEmpty()) {
      log.atDebug().log("No extensions found for plugin '{}'", moduleId);
    } else {
      log.atDebug().log("Found {} extensions for plugin '{}'", result.size(), moduleId);
    }

    // sort by "ordinal" property
    Collections.sort(result);

    return result;
  }

  @Override
  public Set<String> findClassNames(ModuleID moduleId) {
    return getEntries().get(moduleId);
  }

  @Override
  public void pluginStateChanged(ModuleStateEvent event) {
    // TODO optimize (do only for some transitions)
    // clear cache
    entries = null;

    // By default we're assuming, that no checks for extension dependencies are necessary.
    //
    // A plugin, that has an optional dependency to other plugins, might lead to unloadable
    // Java classes (NoClassDefFoundError) at application runtime due to possibly missing
    // dependencies. Therefore we're enabling the check for optional extensions, if the
    // started plugin contains at least one optional plugin dependency.
    if (checkForExtensionDependencies == null && ModuleState.STARTED.equals(event.getPluginState())) {
      for (ModuleDependecy dependency : event.getPlugin().getDescriptor().getDependencies()) {
        if (dependency.isOptional()) {
          log.atDebug().log("Enable check for extension dependencies via ASM.");
          checkForExtensionDependencies = true;
          break;
        }
      }
    }
  }

  /**
   * Returns true, if the extension finder checks extensions for its required plugins.
   * This feature has to be enabled, in order check the availability of
   * {@link ExtensionDefinition#plugins()} configured by an extension.
   * <p>
   * This feature is enabled by default, if at least one available plugin makes use of
   * optional plugin dependencies. Those optional plugins might not be available at runtime.
   * Therefore any extension is checked by default against available plugins before its
   * instantiation.
   * <p>
   * Notice: This feature requires the optional <a href="https://asm.ow2.io/">ASM library</a>
   * to be available on the applications classpath.
   *
   * @return true, if the extension finder checks extensions for its required plugins
   */
  public final boolean isCheckForExtensionDependencies() {
    return Boolean.TRUE.equals(checkForExtensionDependencies);
  }

  /**
   * Plugin developers may enable / disable checks for required plugins of an extension.
   * This feature has to be enabled, in order check the availability of
   * {@link ExtensionDefinition#plugins()} configured by an extension.
   * <p>
   * This feature is enabled by default, if at least one available plugin makes use of
   * optional plugin dependencies. Those optional plugins might not be available at runtime.
   * Therefore any extension is checked by default against available plugins before its
   * instantiation.
   * <p>
   * Notice: This feature requires the optional <a href="https://asm.ow2.io/">ASM library</a>
   * to be available on the applications classpath.
   *
   * @param checkForExtensionDependencies true to enable checks for optional extensions, otherwise false
   */
  public void setCheckForExtensionDependencies(boolean checkForExtensionDependencies) {
    this.checkForExtensionDependencies = checkForExtensionDependencies;
  }

  protected void debugExtensions(Set<String> extensions) {
    log.atDebug().log(()->{
      if(extensions.isEmpty()){
        return "No extensions found";
      }else {
        return "Found possible {} extensions:"+ extensions.size();
      }
    });
    for (String extension : extensions) {
      log.atDebug().log("   " + extension);
    }
  }

  private Map<ModuleID, Set<String>> readStorages() {
    Map<ModuleID, Set<String>> result = new LinkedHashMap<>();

    result.putAll(readClasspathStorages());
    result.putAll(readPluginsStorages());

    return result;
  }

  private Map<ModuleID, Set<String>> getEntries() {
    if (entries == null) {
      entries = readStorages();
    }

    return entries;
  }

  /**
   * Returns the parameters of an {@link ExtensionDefinition} annotation without loading
   * the corresponding class into the class loader.
   *
   * @param className   name of the class, that holds the requested {@link ExtensionDefinition} annotation
   * @param classLoader class loader to access the class
   * @return the contents of the {@link ExtensionDefinition} annotation or null, if the class does not
   * have an {@link ExtensionDefinition} annotation
   */
  private ExtensionInfo getExtensionInfo(String className, ClassLoader classLoader) {
    if (extensionInfos == null) {
      extensionInfos = new HashMap<>();
    }

    if (!extensionInfos.containsKey(className)) {
      log.atTrace().log("Load annotation for '{}' using asm", className);
      ExtensionInfo info = ExtensionInfo.load(className, classLoader);
      if (info == null) {
        log.atWarning().log("No extension annotation was found for '{}'", className);
        extensionInfos.put(className, null);
      } else {
        extensionInfos.put(className, info);
      }
    }

    return extensionInfos.get(className);
  }

  private ExtensionWrapper createExtensionWrapper(Class<?> extensionClass) {
    int ordinal = 0;
    if (extensionClass.isAnnotationPresent(ExtensionDefinition.class)) {
      ordinal = extensionClass.getAnnotation(ExtensionDefinition.class).ordinal();
    }
    ExtensionDescriptor descriptor = new ExtensionDescriptor(ordinal, extensionClass);

    return new ExtensionWrapper<>(descriptor, pluginManager.getExtensionFactory());
  }

  private void checkDifferentClassLoaders(Class<?> type, Class<?> extensionClass) {
    ClassLoader typeClassLoader = type.getClassLoader(); // class loader of extension point
    ClassLoader extensionClassLoader = extensionClass.getClassLoader();
    boolean match = ClassUtils.getAllInterfacesNames(extensionClass).contains(type.getSimpleName());
    if (match && !extensionClassLoader.equals(typeClassLoader)) {
      // in this scenario the method 'isAssignableFrom' returns only FALSE
      // see http://www.coderanch.com/t/557846/java/java/FWIW-FYI-isAssignableFrom-isInstance-differing
      log.atError().log("Different class loaders: '{}' (E) and '{}' (EP)", extensionClassLoader, typeClassLoader);
    }
  }

}
