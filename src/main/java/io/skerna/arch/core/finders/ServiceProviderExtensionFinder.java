package io.skerna.arch.core.finders;

import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.IdentifierException;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * The {@link java.util.ServiceLoader} base implementation for {@link ExtensionFinder}.
 * This class lookup extensions in all extensions index files {@code META-INF/services}.
 * <p>
 */
public class ServiceProviderExtensionFinder extends AbstractExtensionFinder {
  private static final Logger log = LoggerFactory.logger(ServiceProviderExtensionFinder.class);

  public ServiceProviderExtensionFinder(ModuleManager pluginManager) {
    super(pluginManager);
  }

  private static String getExtensionsResource() {
    //return ServiceExtensionsStore.EXTENSIONS_RESOURCE;
    throw new IdentifierException("fixmoe");
  }

  @Override
  public Map<ModuleID, Set<String>> readClasspathStorages() {
    log.atDebug().log("Reading extensions storages from classpath");
    Map<ModuleID, Set<String>> result = new LinkedHashMap<>();

    final Set<String> bucket = new HashSet<>();
    try {
      Enumeration<URL> urls = getClass().getClassLoader().getResources(getExtensionsResource());
      if (urls.hasMoreElements()) {
        collectExtensions(urls, bucket);
      } else {
        log.atDebug().log("Cannot find '{}'", getExtensionsResource());
      }

      debugExtensions(bucket);

      result.put(null, bucket);
    } catch (IOException | URISyntaxException e) {
      log.atError().log(e.getMessage(), e);
    }

    return result;
  }

  @Override
  public Map<ModuleID, Set<String>> readPluginsStorages() {
    log.atDebug().log("Reading extensions storages from plugins");
    Map<ModuleID, Set<String>> result = new LinkedHashMap<>();

    List<ModuleWrapper> plugins = pluginManager.getModules();
    for (ModuleWrapper plugin : plugins) {
      ModuleID moduleId = plugin.getDescriptor().getModuleID();
      log.atDebug().log("Reading extensions storages for plugin '{}'", moduleId);
      final Set<String> bucket = new HashSet<>();

      try {
        Enumeration<URL> urls = (getClass().getClassLoader().getResources(getExtensionsResource()));
        if (urls.hasMoreElements()) {
          collectExtensions(urls, bucket);
        } else {
          log.atDebug().log("Cannot find '{}'", getExtensionsResource());
        }

        debugExtensions(bucket);

        result.put(moduleId, bucket);
      } catch (IOException | URISyntaxException e) {
        log.atError().log(e.getMessage(), e);
      }
    }

    return result;
  }

  private void collectExtensions(Enumeration<URL> urls, Set<String> bucket) throws URISyntaxException, IOException {
    while (urls.hasMoreElements()) {
      URL url = urls.nextElement();
      log.atDebug().log("Read '{}'", url.getFile());
      collectExtensions(url, bucket);
    }
  }

  private void collectExtensions(URL url, Set<String> bucket) throws URISyntaxException, IOException {
    Path extensionPath;
    if (url.toURI().getScheme().equals("jar")) {
      extensionPath = FileUtils.getPath(url.toURI(), getExtensionsResource());
    } else {
      extensionPath = Paths.get(url.toURI());
    }

    bucket.addAll(readExtensions(extensionPath));
  }

  private Set<String> readExtensions(Path extensionPath) throws IOException {
    final Set<String> result = new HashSet<>();
    Files.walkFileTree(extensionPath, Collections.<FileVisitOption>emptySet(), 1, new SimpleFileVisitor<Path>() {

      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        log.atTrace().log("Read '{}'", file);
        try (Reader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
          //ServiceExtensionsStore.read(reader, result);
          throw new IllegalAccessError("fixme");
        }

        //return FileVisitResult.CONTINUE;
      }

    });

    return result;
  }

}
