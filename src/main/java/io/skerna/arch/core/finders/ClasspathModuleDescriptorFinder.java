package io.skerna.arch.core.finders;

import io.skerna.arch.core.*;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.processor.modules.AbstractModuleProcessor;
import io.skerna.arch.core.processor.modules.PluginAnnotationReader;
import io.github.classgraph.*;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.IteratorUtils;
import io.skerna.arch.core.vo.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;

public class ClasspathModuleDescriptorFinder implements ModuleDescriptorFinder {
  private static final Logger log = LoggerFactory.logger(ClasspathModuleDescriptorFinder.class);
  private Set<AbstractModuleProcessor> processorList = new HashSet<>();
  private static ScanResult localScanResult;

  public ClasspathModuleDescriptorFinder() {
    this.processorList = IteratorUtils.toSet(ServiceLoader.load(AbstractModuleProcessor.class).iterator());
  }

  public ClasspathModuleDescriptorFinder addModuleProcessor(AbstractModuleProcessor abstractModuleProcessor) {
    this.processorList.add(abstractModuleProcessor);
    return this;
  }


  @Override
  public Set<ModuleDescriptor> find(SourceModule sourceModule) throws ArchException {
    Set<ModuleDescriptor> descriptors = new HashSet<>();
    ScanResult scanResult = new ClassGraph()
      .enableExternalClasses()
      .acceptPackages("io.skerna")
      .enableAnnotationInfo()
      .scan(2);

    ClassInfoList plugins = scanResult.getClassesWithAnnotation(ModuleDefinition.class.getCanonicalName());
    ClassInfoList runners = scanResult.getClassesWithAnnotation(RunWith.class.getCanonicalName());

    for (ClassInfo plugin : plugins) {
      //Module activador = plugin.getAnnotation(Module.class);
      AnnotationInfo annotationInfo = plugin.getAnnotationInfo(ModuleDefinition.class.getCanonicalName());
      AnnotationInfo runWithAnnInfo = findAnnotation(plugin, RunWith.class.getCanonicalName());
      AnnotationInfo buidWithAnnInfo = findAnnotation(plugin, BuildWith.class.getCanonicalName());

      Annotation annotationValue = annotationInfo.loadClassAndInstantiate();
      ModuleDefinition activador = (ModuleDefinition) annotationValue;

      DefaultModuleDescriptor.Builder pluginDescriptorBuilder = fromAnnotation(activador, plugin.getName());

      if (runWithAnnInfo != null) {
        RunWith runWith = (RunWith) runWithAnnInfo.loadClassAndInstantiate();
        pluginDescriptorBuilder.setModuleRunnerID(ModuleRunnerID.of(runWith.value()));
      }
      if (buidWithAnnInfo != null) {
        BuildWith buildWith = (BuildWith) buidWithAnnInfo.loadClassAndInstantiate();
        pluginDescriptorBuilder.setModuleFactoryID(ModuleFactoryID.of(buildWith.value()));
      }
      ModuleDescriptor moduleDescriptor = pluginDescriptorBuilder.build();
      descriptors.add(moduleDescriptor);
      log.atWarning().log("Annotated {} not is assignable to {}", plugin, Module.class);
    }
    return descriptors;
  }

  private AnnotationInfo findAnnotation(ClassInfo classInfo, String annotation) {
    AnnotationInfo annotationInfo = classInfo.getAnnotationInfo(annotation);
    if (annotationInfo != null) {
      return annotationInfo;
    }
    ClassInfo lastSuperClass = null;
    for (ClassInfo superclass : classInfo.getSuperclasses()) {
      lastSuperClass = superclass;
      annotationInfo = superclass.getAnnotationInfo(annotation);
      if (annotationInfo != null) {
        break;
      }
    }

    if (annotationInfo == null && classInfo.getSuperclass() != null) {
      return findAnnotation(classInfo.getSuperclass(), annotation);
    } else {
      return annotationInfo;
    }
  }


//  @Override
//  public Set<ModuleDescriptor> find(SourceModule sourceModule) {
//    Set<ModuleDescriptor> descriptors = new HashSet<>();
//
//    for (AbstractModuleProcessor abstractModuleProcessor : processorList) {
//      Class target = abstractModuleProcessor.getTarget();
//      Iterable<Class<?>> plugins = ClassIndex.getAnnotated(target);
//      for (Class<?> plugin : plugins) {
//
//
//        //Module activador = plugin.getAnnotation(Module.class);
//        Annotation annotationValue = plugin.getAnnotation(target);
//        ModuleDefinition activador =  abstractModuleProcessor.adaptAnnotation(annotationValue);
//        RunWith runWith = plugin.getAnnotation(RunWith.class);
//        BuildWith buildWith = plugin.getAnnotation(BuildWith.class);
//
//        DefaultModuleDescriptor.Builder pluginDescriptorBuilder = fromAnnotation(activador, plugin.getCanonicalName());
//
//
//        if(runWith == null){
//          throw new ArchException("ERROR_MODULER_RUNNER")
//            .appendErrorCode("ERROR_MODULER_RUNNER_NOT_DEFINED")
//            .appendMessage("Module ID" +pluginDescriptorBuilder.moduleId)
//            .appendMessage("Module class " + pluginDescriptorBuilder.moduleClass);
//        }
//        if(buildWith == null){
//          throw new ArchException("ERROR_MODULE_BUILDER")
//            .appendErrorCode("ERROR_MODULER_BUILDER_NOT_DEFINED")
//            .appendMessage("Module ID" +pluginDescriptorBuilder.moduleId)
//            .appendMessage("Module class " + pluginDescriptorBuilder.moduleClass);
//        }
//
//        if (Module.class.isAssignableFrom(plugin)) {
//            pluginDescriptorBuilder.setModuleRunnerID(ModuleRunnerID.of(runWith.value()))
//            .setModuleFactoryID(ModuleFactoryID.of(buildWith.value()))
//            .build();
//          ModuleDescriptor moduleDescriptor = pluginDescriptorBuilder.build();
//          descriptors.add(moduleDescriptor);
//        } else {
//          log.atWarning().log("Annotated {} not is assignable to {}", plugin, Module.class);
//        }
//      }
//    }
//
//    return descriptors;
//  }

  private DefaultModuleDescriptor.Builder fromAnnotation(ModuleDefinition pluginActivador, String className) {
    Map<String, Object> mapValues = PluginAnnotationReader.readPluginActivadorInMap(pluginActivador, className);


    PluginAnnotationReader readerValues = PluginAnnotationReader.ofClass(pluginActivador, className);
    ModuleID moduleID = ModuleID.of(readerValues.moduleId());

    DefaultModuleDescriptor.Builder builder = DefaultModuleDescriptor.builder(moduleID, className)
      .setModuleDescription(readerValues.pluginDescription())
      .setVersion(readerValues.version())
      .setRequires(readerValues.requires())
      .setProvider(readerValues.provider())
      .setModuleDependecies(readerValues.dependecies())
      .setLicence(readerValues.license())
      .setModuleType(ModuleType.valueOf(readerValues.moduleType()));


    return builder;
  }


  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    //pluginPath.toString().contains(Constants.VIRTUAL_PATH.toString());
    return ClassUtils.isOfType(sourceModule, SourceClasspathModule.class);
  }

}
