///*
// * Copyright (C) 2012-present the original author or authors.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package io.skerna.arch.core.finders;
//
//import io.skerna.arch.core.ModuleManager;
//import io.skerna.arch.core.ModuleWrapper;
//import io.skerna.arch.core.vo.ModuleID;
//import io.skerna.common.logger.Logger;
//import io.skerna.common.logger.LoggerFactory;
//
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.net.URL;
//import java.nio.charset.StandardCharsets;
//import java.util.*;
//
///**
// * All extensions declared in a plugin are indexed in a file {@code META-INF/extensions.idx}.
// * This class lookup extensions in all extensions index files {@code META-INF/extensions.idx}.
// * <p>
// */
//public class LegacyExtensionFinder extends AbstractExtensionFinder {
//  private static final Logger log = LoggerFactory.logger(LegacyExtensionFinder.class);
//
//  public LegacyExtensionFinder(ModuleManager pluginManager) {
//    super(pluginManager);
//  }
//
// /* private static String getExtensionsResource() {
//    return LegacyServiceStorage.EXTENSIONS_RESOURCE;
//  }*/
//
//  @Override
//  public Map<ModuleID, Set<String>> readClasspathStorages() {
//    log.atDebug().log("Reading extensions storages from classpath");
//    Map<ModuleID, Set<String>> result = new LinkedHashMap<>();
//
//    Set<String> bucket = new HashSet<>();
//    try {
//      Enumeration<URL> urls = getClass().getClassLoader().getResources(getExtensionsResource());
//      if (urls.hasMoreElements()) {
//        collectExtensions(urls, bucket);
//      } else {
//        log.atDebug().log("Cannot find '{}'", getExtensionsResource());
//      }
//
//      debugExtensions(bucket);
//
//      result.put(null, bucket);
//    } catch (IOException e) {
//      log.atError().log(e.getMessage(), e);
//    }
//
//    return result;
//  }
//
//  @Override
//  public Map<ModuleID, Set<String>> readPluginsStorages() {
//    log.atDebug().log("Reading extensions storages from plugins");
//    Map<ModuleID, Set<String>> result = new LinkedHashMap<>();
//
//    List<ModuleWrapper> plugins = pluginManager.getModules();
//    for (ModuleWrapper plugin : plugins) {
//      ModuleID moduleId = plugin.getDescriptor().getModuleID();
//      log.atDebug().log("Reading extensions storage from plugin '{}'", moduleId);
//      Set<String> bucket = new HashSet<>();
//
//      URL urls = plugin.getPluginClassLoader().getResource(getExtensionsResource());
//      collectExtension(urls, bucket);
//      debugExtensions(bucket);
//
//      result.put(moduleId, bucket);
//    }
//
//    return result;
//  }
//
//  private void collectExtensions(Enumeration<URL> urls, Set<String> bucket) throws IOException {
//    while (urls.hasMoreElements()) {
//      collectExtension(urls.nextElement(), bucket);
//    }
//  }
//
//  private void collectExtension(URL url, Set<String> bucket) {
//    log.atDebug().log("Read '{}'", url.getFile());
//    try (Reader reader = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)) {
//      //LegacyServiceStorage.read(reader, bucket);
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }
//
//}
