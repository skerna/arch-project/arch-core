package io.skerna.arch.core.finders;

import io.skerna.arch.core.ExtensionWrapper;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleStateEvent;
import io.skerna.arch.core.ModuleStateObserver;
import io.skerna.arch.core.vo.ModuleID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The default implementation for {@link ExtensionFinder}.
 * It's a compound {@code ExtensionFinder}.
 * <p>
 */
public class DefaultExtensionFinder implements ExtensionFinder, ModuleStateObserver {

  protected ModuleManager pluginManager;
  protected List<ExtensionFinder> finders = new ArrayList<>();

  public DefaultExtensionFinder(ModuleManager pluginManager) {
    this.pluginManager = pluginManager;

    // add(new LegacyExtensionFinder(pluginManager));
//        add(new ServiceProviderExtensionFinder(pluginManager));
  }

  @Override
  public <T> List<ExtensionWrapper<T>> find(Class<T> type) {
    List<ExtensionWrapper<T>> extensions = new ArrayList<>();
    for (ExtensionFinder finder : finders) {
      extensions.addAll(finder.find(type));
    }

    return extensions;
  }

  @Override
  public <T> List<ExtensionWrapper<T>> find(Class<T> type, ModuleID moduleId) {
    List<ExtensionWrapper<T>> extensions = new ArrayList<>();
    for (ExtensionFinder finder : finders) {
      extensions.addAll(finder.find(type, moduleId));
    }

    return extensions;
  }

  @Override
  public List<ExtensionWrapper> find(ModuleID moduleId) {
    List<ExtensionWrapper> extensions = new ArrayList<>();
    for (ExtensionFinder finder : finders) {
      extensions.addAll(finder.find(moduleId));
    }

    return extensions;
  }

  @Override
  public Set<String> findClassNames(ModuleID moduleId) {
    Set<String> classNames = new HashSet<>();
    for (ExtensionFinder finder : finders) {
      classNames.addAll(finder.findClassNames(moduleId));
    }

    return classNames;
  }

  @Override
  public void pluginStateChanged(ModuleStateEvent event) {
    for (ExtensionFinder finder : finders) {
      if (finder instanceof ModuleStateObserver) {
        ((ModuleStateObserver) finder).pluginStateChanged(event);
      }
    }
  }

  public DefaultExtensionFinder addServiceProviderExtensionFinder() {
    return add(new ServiceProviderExtensionFinder(pluginManager));
  }

  public DefaultExtensionFinder add(ExtensionFinder finder) {
    finders.add(finder);

    return this;
  }

}
