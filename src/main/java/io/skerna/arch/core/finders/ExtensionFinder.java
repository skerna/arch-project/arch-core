package io.skerna.arch.core.finders;

import io.skerna.arch.core.ExtensionWrapper;
import io.skerna.arch.core.vo.ModuleID;

import java.util.List;
import java.util.Set;

public interface ExtensionFinder {

  /**
   * Retrieves a list with all extensions found for an extension point.
   */
  <T> List<ExtensionWrapper<T>> find(Class<T> type);

  /**
   * Retrieves a list with all extensions found for an extension point and a plugin.
   */
  <T> List<ExtensionWrapper<T>> find(Class<T> type, ModuleID moduleId);

  /**
   * Retrieves a list with all extensions found for a plugin
   *
   * @param moduleId
   */
  List<ExtensionWrapper> find(ModuleID moduleId);

  /**
   * Retrieves a list with all extension class names found for a plugin.
   *
   * @param moduleId
   */
  Set<String> findClassNames(ModuleID moduleId);

}
