package io.skerna.arch.core.exceptions;

public class I18NInternalException extends RuntimeException {

    public I18NInternalException(){
        super();
    }

    public I18NInternalException(String s){
        super(s);
    }

    public I18NInternalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public I18NInternalException(Throwable throwable) {
        super(throwable);
    }

    protected I18NInternalException(String s,Throwable  throwable, Boolean b, Boolean b1 ){
        super(s,throwable); 
    }
}
