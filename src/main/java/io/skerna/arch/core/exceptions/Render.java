package io.skerna.arch.core.exceptions;


import java.util.Locale;
import java.util.Optional;

public interface Render {
  /**
   * Renderiza una expceion i18n
   *
   * @param i18NExceptionBase
   * @param locale
   * @return
   */
  Optional<RenderResult> render(I18NException i18NExceptionBase, Locale locale);

  /**
   * Renderiza una exception sin especificar el idioma
   *
   * @param i18NExceptionBase
   * @return
   */
  Optional<RenderResult> render(I18NException i18NExceptionBase);


  /**
   * Crea un render
   *
   * @param catalog
   * @return
   */
  static Render create(MessageCatalog catalog) {
    return new ExceptionRender(catalog);
  }

}
