package io.skerna.arch.core.exceptions;

public interface Renderable {
  RenderResult render();
}
