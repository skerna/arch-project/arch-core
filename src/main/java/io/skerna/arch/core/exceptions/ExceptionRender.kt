package io.skerna.arch.core.exceptions

import java.util.Locale
import java.util.Optional

open class ExceptionRender(private val catalog: MessageCatalog) : Render {

    override fun render(i18NExceptionBase: I18NException, locale: Locale): Optional<RenderResult> {
        val code = i18NExceptionBase.getErrorCode()
        val additionalCodes = i18NExceptionBase.getAdditionalErrorCodes()
        val messageTranslated = catalog.getLocalizedMessage(code, locale)


        val messageTranslatedList = additionalCodes.associateWith(catalog::getLocalizedMessage)

        var renderResult = RenderResult(code, messageTranslated,messageTranslatedList)
        return Optional.ofNullable(renderResult)
    }

    override fun render(i18NExceptionBase: I18NException): Optional<RenderResult> {
        return render(i18NExceptionBase, Locale.getDefault())
    }
}
