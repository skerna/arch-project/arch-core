package io.skerna.arch.core.exceptions;

import java.util.Map;
import java.util.Objects;

public class RenderResult implements JsonEncoded {
  private String errorCode;
  private String mensaje;
  private Map<String, String> additionalMessages;

  public RenderResult(String errorCode, String mensaje, Map<String, String> additionalMessages) {
    this.errorCode = errorCode;
    this.mensaje = mensaje;
    this.additionalMessages = additionalMessages;
  }
//
//  override fun toString(): String {
//        return "RenderResult{" +
//                "codigo='" + errorCode + '\''.toString() +
//                ", mensaje='" + mensaje + '\''.toString() +
//                ", additionalMessages='"+(additionalMessages?:"") + '\''.toString() +
//                '}'.toString()
//    }
//
//    override fun encodeAsJson(): String {
//        return """
//            {
//                "codigo":"$errorCode",
//                "message":"$mensaje",
//                "detailes":"$additionalMessages"
//            }
//        """.trimIndent()
//    }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RenderResult that = (RenderResult) o;
    return errorCode.equals(that.errorCode) &&
      mensaje.equals(that.mensaje);
  }

  @Override
  public int hashCode() {
    return Objects.hash(errorCode, mensaje);
  }

  @Override
  public String toString() {
    return "RenderResult{" +
      "errorCode='" + errorCode + '\'' +
      ", mensaje='" + mensaje + '\'' +
      ", additionalMessages=" + additionalMessages +
      '}';
  }

  @Override
  public String encodeAsJson() {
    return String.format("{\"errorCode\":\"%s\", \"message\": \"%s\" , \"additionalMessages\":\"%s\"}", errorCode, mensaje, additionalMessages);
  }

  public String getMensaje() {
    return mensaje;
  }
}
