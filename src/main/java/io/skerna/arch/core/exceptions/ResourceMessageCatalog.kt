// package io.skerna.arch.core.exceptions

// import java.util.*

// open class ResourceMessageCatalog : MessageCatalog {
//     private var classLoader:ClassLoader?=null
//     private var bundleName:String;
//     /**
//      * Defult contructor, espera encontrar en el path un resource bundle con el nombre
//      * messsages
//      */
//     constructor(bundleName: String?) {
//         val _nameBundle = if (bundleName.isNullOrEmpty()) MESSAGE_RESOURCE else bundleName
//         this.bundleName = _nameBundle
//     }

//     /**
//      * Defult contructor, espera encontrar en el path un resource bundle con el nombre
//      * messsages
//      */
//     constructor(bundleName: String, classLoader: ClassLoader):this(bundleName) {
//         this.classLoader = classLoader
//     }

//   /**
//    * El cliente debe retorna el
//    * path de ResourceBundle para el paquete
//    * en el que esta trabajando.
//    * Ejm io.r2b.pack.messages.namebundle
//    * en donde el directorio corresponde a resource files path, del proyecto
//    * @return
//    */
//   override fun bundleName(): String {
//     return this.bundleName;
//   }


//   override fun getLocalizedMessage(code: String, locale: Locale): String {
//         val bundle = loadBundle(locale,classLoader)
//         var message: String? = ""
//         if (bundle.isPresent) {
//             try {
//                 message = bundle.get().getString(code.toString())
//             } catch (e: Exception) {

//             }

//         }
//         if (message == null || message.isEmpty()) {
//             for (key in bundle.get().keys) {
//                 println(key)
//             }
//             throw I18NInternalException(String.format("Code [%s] is not part of catalog $bundleName in Catalog ", code))
//         }

//         return message

//     }


//     override fun getLocalizedMessage(code: String): String {
//         return getLocalizedMessage(code, Locale.getDefault())
//     }

//     /**
//      * @param locale NULLEABLE
//      * @param classLoader
//      * @return
//      */
//     private fun loadBundle(locale: Locale,loader:ClassLoader?=null): Optional<ResourceBundle> {
//         val keyBundle = bundleName() + "_" + locale.country
//         if (!references.containsKey(keyBundle)) {
//             var bundle: ResourceBundle?
//             try {
//                 if(loader == null){
//                     bundle = ResourceBundle.getBundle(bundleName, locale)
//                 }else{
//                     bundle = ResourceBundle.getBundle(bundleName, locale,loader)
//                 }
//                 references[keyBundle] = bundle
//             } catch (ex: Exception) {
//                 throw I18NInternalException(String.format("bundle %s not found", bundleName))
//             }

//         }
//         return Optional.ofNullable(references[keyBundle])
//     }

//     override fun hasCode(code: String): Boolean {
//         return hasCode(code, Locale.getDefault())
//     }

//     override fun hasCode(code: String, locale: Locale): Boolean {
//         val bundle = loadBundle(locale,classLoader)
//         return bundle.map { resourceBundle -> resourceBundle.containsKey(code.toString()) }.orElse(false)
//     }

//     companion object {
//         // Cache resource bundles
//         private val references = WeakHashMap<String, ResourceBundle>()
//         val MESSAGE_RESOURCE = "messages"
//     }

// }
