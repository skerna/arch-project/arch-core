package io.skerna.arch.core.exceptions

open class I18NInternalException : RuntimeException {
    constructor() : super() {}

    constructor(s: String) : super(s) {}

    constructor(s: String, throwable: Throwable) : super(s, throwable) {}

    constructor(throwable: Throwable) : super(throwable) {}

    protected constructor(s: String, throwable: Throwable, b: Boolean, b1: Boolean) : super(s, throwable, b, b1) {}
}
