package io.skerna.arch.core.exceptions;

public interface JsonEncoded {
  String encodeAsJson();
}
