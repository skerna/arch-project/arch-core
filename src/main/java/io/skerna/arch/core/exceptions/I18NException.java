package io.skerna.arch.core.exceptions;

import java.util.List;

public interface I18NException extends Renderable {
    /**
     * Retorna el código a renderizar
     * @return
     */
    String getErrorCode();

    /**
     * Retorna una lista de codigos adicionales
     * @return List<String>
     */
    List<String> getAdditionalErrorCodes();


    /**
     * Check if current exception has addiotional Exceptions error
     * codes
     * @return Boolean
     */
    Boolean hasAdditionalErrorCodes();

    /**
     * Retorna el render
     * para la interfaz cliente
     * @return
     */
    Render getRender();


}
