package io.skerna.arch.core.exceptions;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ronald on 02/02/18.
 */
public abstract class StandardRuntimeException extends RuntimeException implements I18NException {
  private String errorCode;
  private List<String> additionalCodes = new ArrayList<>();
  private List<String> additionalMessages = new ArrayList<>();
  private Render _render;//initialize_render()


  public StandardRuntimeException(String errorCode) {
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String message, String errorCode) {
    super(message);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String message, Throwable cause, String errorCode) {
    super(message, cause);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(Throwable cause, String errorCode) {
    super(cause);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String errorCode) {
    super(message, cause, enableSuppression, writableStackTrace);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String errorCode, String message, Throwable cause) {
    super(message,cause);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String errorCode, Throwable cause) {
    super(cause);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message,cause,enableSuppression,writableStackTrace);
    this.errorCode = errorCode;
  }

  public StandardRuntimeException appendMessage(String message) {
    additionalMessages.add(message);
    return this;
  }
  public StandardRuntimeException appendErrorCode(String errorCode) {
    additionalCodes.add(errorCode);
    return this;
  }

  public String getErrorCode(){
    return errorCode;
  }

  public List<String> getAdditionalErrorCodes(){
    return additionalCodes;
  }

  public Boolean hasAdditionalErrorCodes(){
    return !this.additionalCodes.isEmpty();
  }

  @Override
  public String toString() {
    return "StandardRuntimeException{" +
      "message='" + getMessage() + '\'' +
      "errorCode='" + errorCode + '\'' +
      ", additionalMessages=" + additionalMessages +
      '}';
  }

  /**
   * Returns the detail message string of this throwable.
   *
   * @return the detail message string of this {@code Throwable} instance
   * (which may be {@code null}).
   */
  @Override
  public String getMessage() {
    String message = "";
    Optional<RenderResult> render = getRender().render(this);
    if(render.isPresent()){
      RenderResult _render = render.get();
      message = _render.getMensaje();
    }
    return message;
  }

  @Override
  public RenderResult render() {
    Optional<RenderResult> renderResult = getRender().render(this);
    if(renderResult.isEmpty()){
      this.printStackTrace();
    }
    return renderResult.get();
  }

  private Render initialize_render(){
    return getRender();
  }

}
