package io.skerna.arch.core.exceptions;

import java.util.Locale;

public interface MessageCatalog {

    /**
     * El cliente debe retorna el
     * path de ResourceBundle para el paquete
     * en el que esta trabajando.
     * Ejm io.r2b.pack.messages.namebundle
     * en donde el directorio corresponde a resource files path, del proyecto
     * @return
     */
   String bundleName();

    /**
     * Obtiene un mesage i18n desde resources
     * @param code
     * @return
     */
    String getLocalizedMessage(String code, Locale locale);


    /**
     * Obtiene un mesage i18n desde resources
     * @param code
     * @return
     */
    String getLocalizedMessage(String code);


    /**
     * Verifica si el código esta disponible en la biblioteca de mensajes
     * @param code
     * @return
     */
    Boolean hasCode(String code);

    /**
     * Verifica si el catalogo tiene la clave
     * @param code
     * @param locale
     * @return
     */
    Boolean hasCode(String code, Locale locale);


}
