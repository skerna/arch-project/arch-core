package io.skerna.arch.core.exceptions;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.WeakHashMap;

public class ResourceMessageCatalog implements MessageCatalog {
    private static final String MESSAGE_RESOURCE = "messages";

    private String bundleName ;
    private WeakHashMap<String, ResourceBundle> references = new WeakHashMap<String, ResourceBundle>();
    private ClassLoader classLoader;

    public ResourceMessageCatalog(String bundleName){
        var _nameBundle =  Objects.isNull(bundleName)? MESSAGE_RESOURCE : bundleName;
        this.bundleName = _nameBundle;
    }
    /**
     * Defult contructor, espera encontrar en el path un resource bundle con el nombre
     * messsages
     */
    public ResourceMessageCatalog(String bundleName, ClassLoader classLoader){
        this(bundleName);
        this.classLoader = classLoader;
    }


    @Override
    public String bundleName() {
        return this.bundleName;
    }

    @Override
    public String getLocalizedMessage(String code, Locale locale) {
        var bundle = loadBundle(locale,classLoader);
        var message = "";
        if (bundle.isPresent()) {
            try {
                message = bundle.get().getString(code.toString());
            } catch (Exception e) {

            }
        }
        if (message == null || message.isEmpty()) {
            throw new I18NInternalException(String.format("Code [%s] is not part of catalog $bundleName in Catalog ", code));
        }

        return message;
    }

    @Override
    public String getLocalizedMessage(String code) {
        return getLocalizedMessage(code, Locale.getDefault());
    }

    @Override
    public Boolean hasCode(String code) {
        return hasCode(code, Locale.getDefault());
    }

    @Override
    public Boolean hasCode(String code, Locale locale) {
        var bundle = loadBundle(locale,classLoader);
        return bundle.map(resourceBundle -> resourceBundle.containsKey(code.toString())).orElse(false);
    }

    private Optional<ResourceBundle> loadBundle(Locale locale,ClassLoader loader) {
        var keyBundle = bundleName() + "_" + locale.getCountry();
        if (!references.containsKey(keyBundle)) {
            ResourceBundle bundle = null;
            try {
                if(loader == null){
                    bundle = ResourceBundle.getBundle(bundleName, locale);
                }else{
                    bundle = ResourceBundle.getBundle(bundleName, locale,loader);
                }
                references.put(keyBundle, bundle);
            } catch (Exception ex) {
                throw new I18NInternalException(String.format("bundle %s not found", bundleName));
            }

        }
        return Optional.ofNullable(references.get(keyBundle));
    }
    
}
