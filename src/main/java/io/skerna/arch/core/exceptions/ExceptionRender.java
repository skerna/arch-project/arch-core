package io.skerna.arch.core.exceptions;
import java.util.Locale;
import java.util.Optional;


import io.skerna.arch.core.exceptions.Render;

public class ExceptionRender implements Render{
    private MessageCatalog catalog;

    public ExceptionRender(MessageCatalog catalog){
        this.catalog = catalog;
    }

    @Override
    public Optional<RenderResult> render(I18NException i18nExceptionBase, Locale locale) {
        var code = i18nExceptionBase.getErrorCode();
        var additionalCodes = i18nExceptionBase.getAdditionalErrorCodes();
        var messageTranslated = catalog.getLocalizedMessage(code, locale);
        //var messageTranslatedList = additionalCoes.associateWith(catalog::getLocalizedMessage);

        //var renderResult = RenderResult(code, messageTranslated,messageTranslatedList)
        return Optional.ofNullable(null);
    }

    @Override
    public Optional<RenderResult> render(I18NException i18nExceptionBase) {
        // TODO Auto-generated method stub
        return render(i18nExceptionBase, Locale.getDefault());
    }
    
}
