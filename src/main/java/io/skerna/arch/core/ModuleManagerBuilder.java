package io.skerna.arch.core;

import io.skerna.arch.core.finders.ExtensionFinder;
import io.skerna.arch.core.factories.ExtensionFactoryManager;
import io.skerna.arch.core.factories.ModuleFactoryManager;
import io.skerna.arch.core.finders.ModuleDescriptorFinder;
import io.skerna.arch.core.loaders.PluginLoaderManager;
import io.skerna.arch.core.repositories.PluginRepository;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public abstract class ModuleManagerBuilder<T extends ModuleManagerBuilder<?,?>, R extends AbstractModuleManager> {
  private static final Logger log = LoggerFactory.logger(ModuleManagerBuilder.class);
  protected ExtensionFinder extensionFinder;
  protected Map<ModuleID, ModuleWrapper> plugins;
  protected Executor executorService;
  protected Path pluginsRoot;
  protected ModuleDescriptorFinder pluginDescriptorFinder;
  protected List<ModuleWrapper> unresolvedPlugins;
  protected List<ModuleWrapper> resolvedPlugins;
  protected List<ModuleWrapper> startedPlugins;
  protected List<ModuleStateObserver> pluginStateListeners;
  protected RuntimeMode runtimeMode;
  protected ModuleRunner pluginRunner;
  protected String systemVersion;
  protected PluginRepository pluginRepository;
  protected ModuleFactoryManager pluginFactory;
  protected ExtensionFactoryManager extensionFactory;
  protected ModuleStatusProvider pluginStatusProvider;
  protected DependencyGraph dependencyResolver;
  protected PluginLoaderManager pluginLoaderManager;
  protected boolean exactVersionAllowed;
  protected VersionManager versionManager;


  /**
   * Sets the {@code extensionFinder} and returns a reference to this T so that the methods can be chained together.
   *
   * @param extensionFinder the {@code extensionFinder} to set
   * @return a reference to this T
   */
  public T withExtensionFinder(ExtensionFinder extensionFinder) {
    this.extensionFinder = extensionFinder;
    return (T) this;
  }

  /**
   * Sets the {@code plugins} and returns a reference to this T so that the methods can be chained together.
   *
   * @param plugins the {@code plugins} to set
   * @return a reference to this T
   */
  public T withPlugins(Map<ModuleID, ModuleWrapper> plugins) {
    this.plugins = plugins;
    return (T) this;
  }

  /**
   * Sets the {@code executorService} and returns a reference to this T so that the methods can be chained together.
   *
   * @param executorService the {@code executorService} to set
   * @return a reference to this T
   */
  public T withExecutorService(Executor executorService) {
    this.executorService = executorService;
    return (T) this;
  }

  /**
   * Sets the {@code pluginsRoot} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginsRoot the {@code pluginsRoot} to set
   * @return a reference to this T
   */
  public T withPluginsRoot(Path pluginsRoot) {
    this.pluginsRoot = pluginsRoot;
    return (T) this;
  }

  /**
   * Sets the {@code pluginDescriptorFinder} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginDescriptorFinder the {@code pluginDescriptorFinder} to set
   * @return a reference to this T
   */
  public T withPluginDescriptorFinder(ModuleDescriptorFinder pluginDescriptorFinder) {
    this.pluginDescriptorFinder = pluginDescriptorFinder;
    return (T) this;
  }

  /**
   * Sets the {@code unresolvedPlugins} and returns a reference to this T so that the methods can be chained together.
   *
   * @param unresolvedPlugins the {@code unresolvedPlugins} to set
   * @return a reference to this T
   */
  public T withUnresolvedPlugins(List<ModuleWrapper> unresolvedPlugins) {
    this.unresolvedPlugins = unresolvedPlugins;
    return (T) this;
  }

  /**
   * Sets the {@code resolvedPlugins} and returns a reference to this T so that the methods can be chained together.
   *
   * @param resolvedPlugins the {@code resolvedPlugins} to set
   * @return a reference to this T
   */
  public T withResolvedPlugins(List<ModuleWrapper> resolvedPlugins) {
    this.resolvedPlugins = resolvedPlugins;
    return (T) this;
  }

  /**
   * Sets the {@code startedPlugins} and returns a reference to this T so that the methods can be chained together.
   *
   * @param startedPlugins the {@code startedPlugins} to set
   * @return a reference to this T
   */
  public T withStartedPlugins(List<ModuleWrapper> startedPlugins) {
    this.startedPlugins = startedPlugins;
    return (T) this;
  }

  /**
   * Sets the {@code pluginStateListeners} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginStateListeners the {@code pluginStateListeners} to set
   * @return a reference to this T
   */
  public T withPluginStateListeners(List<ModuleStateObserver> pluginStateListeners) {
    this.pluginStateListeners = pluginStateListeners;
    return (T) this;
  }

  /**
   * Sets the {@code runtimeMode} and returns a reference to this T so that the methods can be chained together.
   *
   * @param runtimeMode the {@code runtimeMode} to set
   * @return a reference to this T
   */
  public T withRuntimeMode(RuntimeMode runtimeMode) {
    this.runtimeMode = runtimeMode;
    return (T) this;
  }

  /**
   * Sets the {@code pluginRunner} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginRunner the {@code pluginRunner} to set
   * @return a reference to this T
   */
  public T withPluginRunner(ModuleRunner pluginRunner) {
    this.pluginRunner = pluginRunner;
    return (T) this;
  }

  /**
   * Sets the {@code systemVersion} and returns a reference to this T so that the methods can be chained together.
   *
   * @param systemVersion the {@code systemVersion} to set
   * @return a reference to this T
   */
  public T withSystemVersion(String systemVersion) {
    this.systemVersion = systemVersion;
    return (T) this;
  }

  /**
   * Sets the {@code pluginRepository} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginRepository the {@code pluginRepository} to set
   * @return a reference to this T
   */
  public T withPluginRepository(PluginRepository pluginRepository) {
    this.pluginRepository = pluginRepository;
    return (T) this;
  }

  /**
   * Sets the {@code pluginFactory} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginFactory the {@code pluginFactory} to set
   * @return a reference to this T
   */
  public T withPluginFactory(ModuleFactoryManager pluginFactory) {
    this.pluginFactory = pluginFactory;
    return (T) this;
  }

  /**
   * Sets the {@code extensionFactory} and returns a reference to this T so that the methods can be chained together.
   *
   * @param extensionFactory the {@code extensionFactory} to set
   * @return a reference to this T
   */
  public T withExtensionFactory(ExtensionFactoryManager extensionFactory) {
    this.extensionFactory = extensionFactory;
    return (T) this;
  }

  /**
   * Sets the {@code pluginStatusProvider} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginStatusProvider the {@code pluginStatusProvider} to set
   * @return a reference to this T
   */
  public T withPluginStatusProvider(ModuleStatusProvider pluginStatusProvider) {
    this.pluginStatusProvider = pluginStatusProvider;
    return (T) this;
  }

  /**
   * Sets the {@code dependencyResolver} and returns a reference to this T so that the methods can be chained together.
   *
   * @param dependencyResolver the {@code dependencyResolver} to set
   * @return a reference to this T
   */
  public T withDependencyResolver(DependencyGraph dependencyResolver) {
    this.dependencyResolver = dependencyResolver;
    return (T) this;
  }

  /**
   * Sets the {@code pluginLoaderManager} and returns a reference to this T so that the methods can be chained together.
   *
   * @param pluginLoaderManager the {@code pluginLoaderManager} to set
   * @return a reference to this T
   */
  public T withPluginLoaderManager(PluginLoaderManager pluginLoaderManager) {
    this.pluginLoaderManager = pluginLoaderManager;
    return (T) this;
  }

  /**
   * Sets the {@code exactVersionAllowed} and returns a reference to this T so that the methods can be chained together.
   *
   * @param exactVersionAllowed the {@code exactVersionAllowed} to set
   * @return a reference to this T
   */
  public T withExactVersionAllowed(boolean exactVersionAllowed) {
    this.exactVersionAllowed = exactVersionAllowed;
    return (T) this;
  }

  /**
   * Sets the {@code versionManager} and returns a reference to this T so that the methods can be chained together.
   *
   * @param versionManager the {@code versionManager} to set
   * @return a reference to this T
   */
  public T withVersionManager(VersionManager versionManager) {
    this.versionManager = versionManager;
    return (T) this;
  }



  public final R build(){
    R moduleManager = prebuild();
    moduleManager.initialize();
    return moduleManager;
  }
  /**
   * build module manager
   * @return
   */
  protected abstract R prebuild();



}
