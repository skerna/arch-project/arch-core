package io.skerna.arch.core;

import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceModule;

public class ModuleAlreadyLoadedException extends ReactorException {

  private final ModuleID moduleId;
  private final SourceModule pluginPath;

  public ModuleAlreadyLoadedException(ModuleID moduleId, SourceModule pluginPath) {
    super("Plugin '{}' already loaded with id '{}'", pluginPath, moduleId);

    this.moduleId = moduleId;
    this.pluginPath = pluginPath;
  }

  public ModuleID getPluginId() {
    return moduleId;
  }

  public SourceModule getPluginPath() {
    return pluginPath;
  }

}
