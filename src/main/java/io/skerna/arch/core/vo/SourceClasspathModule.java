package io.skerna.arch.core.vo;

import io.skerna.arch.core.Constants;
import io.skerna.arch.core.util.FileUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class SourceClasspathModule extends SourceModule {
  public final Path file;
  public final ClassLoader classLoader;

  public SourceClasspathModule(ClassLoader classLoader) {
    super(SourceModuleType.CLASSPATH_SOURCE_MODULE);
    this.classLoader = classLoader;
    this.file = Constants.VIRTUAL_PATH;
  }

  public SourceClasspathModule() {
    super(SourceModuleType.CLASSPATH_SOURCE_MODULE);
    this.file = Constants.VIRTUAL_PATH;
    this.classLoader = getClass().getClassLoader();
  }

  public boolean isJarFile(){
    return FileUtils.isJarFile(file);
  }

  public boolean exists(){
    return Files.exists(file);
  }

  public boolean isDirectory(){
    return Files.isDirectory(file);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SourceClasspathModule that = (SourceClasspathModule) o;
    return file.equals(that.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(file);
  }
}
