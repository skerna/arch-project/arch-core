package io.skerna.arch.core.vo;

public abstract class SourceModule {

  public final SourceModuleType sourceModuleType;

  public SourceModule(SourceModuleType sourceModuleType) {
    this.sourceModuleType = sourceModuleType;
  }

  public boolean isType(SourceModuleType sourceModuleType){
    return sourceModuleType==sourceModuleType;
  }

  /**
   * Answers an integer hash code for the receiver. Any two
   * objects which answer <code>true</code> when passed to
   * <code>.equals</code> must answer the same value for this
   * method.
   *
   * @return the receiver's hash.
   * @see    #equals
   */
  @Override
  abstract public int hashCode();


  /**
   * Compares the argument to the receiver, and answers true
   * if they represent the <em>same</em> object using a class
   * specific comparison. The implementation in Object answers
   * true only if the argument is the exact same object as the
   * receiver (==).
   *
   * @param        o Object
   * the object to compare with this object.
   * @return boolean <code>true</code>
   * if the object is the same as this object
   * <code>false</code>
   * if it is different from this object.
   * @see            #hashCode
   */
  @Override
  public abstract boolean equals(Object o);
}
