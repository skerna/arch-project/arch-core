package io.skerna.arch.core.vo;

import io.skerna.arch.core.util.FileUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class SourceFileModule extends SourceModule {
  public final Path file;

  public SourceFileModule(Path file) {
    super(SourceModuleType.FILE_SOURCE_MODULE);
    this.file = file;
  }

  public boolean isJarFile(){
    return FileUtils.isJarFile(file);
  }

  public boolean exists(){
    return Files.exists(file);
  }

  public boolean isDirectory(){
    return Files.isDirectory(file);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SourceFileModule that = (SourceFileModule) o;
    return file.equals(that.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(file);
  }
}
