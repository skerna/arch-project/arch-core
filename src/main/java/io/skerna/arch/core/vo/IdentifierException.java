package io.skerna.arch.core.vo;

import io.skerna.arch.core.ArchException;
import org.jetbrains.annotations.Nullable;

/**
 * <h1>IdentifierException!</h1>
 * IdentifierException, is trowed when modlule identifier is null or empty
 * one module definition can not be empty or null; if this is the case this
 * exception is triggered
 * <p>
 * <b>Note:</b>
 * </p>
 * @see ModuleFactoryID
 * @see ModuleRunnerID
 *
 */
public class IdentifierException extends ArchException {

  public IdentifierException() {
    super("ERROR_IDENTIFIER");
  }

  public IdentifierException(String errorCode) {
    super(errorCode);
  }

  public IdentifierException(String errorCode, @Nullable String message) {
    super(errorCode, message);
  }

  public IdentifierException(String errorCode, @Nullable String message, @Nullable Throwable cause) {
    super(errorCode, message, cause);
  }

  public IdentifierException(String errorCode, @Nullable Throwable cause) {
    super(errorCode, cause);
  }

  public IdentifierException(String errorCode, @Nullable String message, @Nullable Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(errorCode, message, cause, enableSuppression, writableStackTrace);
  }
}
