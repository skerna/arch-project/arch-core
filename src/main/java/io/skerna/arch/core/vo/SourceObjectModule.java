package io.skerna.arch.core.vo;

import io.skerna.arch.core.Module;

import java.util.Objects;

public class SourceObjectModule extends SourceModule {
  public final Module instance;

  public SourceObjectModule(Module instance) {
    super(SourceModuleType.JVM_OBJECT_REFERENCE);
    Objects.requireNonNull(instance);
    this.instance = instance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SourceObjectModule that = (SourceObjectModule) o;
    return instance.equals(that.instance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(instance);
  }
}
