package io.skerna.arch.core.vo;

import io.skerna.arch.core.ModuleState;

import java.util.Objects;


public class ChangeStateResult {
  private final ModuleID pluginID;
  private final ModuleState pluginState;

  public ChangeStateResult(ModuleID pluginID, ModuleState pluginState) {
    this.pluginID = pluginID;
    this.pluginState = pluginState;
  }

  public ModuleID getPluginID() {
    return pluginID;
  }

  public ModuleState getPluginState() {
    return pluginState;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ChangeStateResult that = (ChangeStateResult) o;
    return Objects.equals(pluginID, that.pluginID) &&
      pluginState == that.pluginState;
  }

  @Override
  public int hashCode() {
    return Objects.hash(pluginID, pluginState);
  }
}
