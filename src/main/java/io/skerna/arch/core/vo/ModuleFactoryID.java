package io.skerna.arch.core.vo;

import io.skerna.arch.core.spi.ModuleFactory;

import java.util.Objects;

public class ModuleFactoryID implements Comparable<String> {
  private String moduleIdValue;

  private ModuleFactoryID(String moduleIdValue) {
    this.moduleIdValue = moduleIdValue;
  }

  public String value() {
    return moduleIdValue;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof String) {
      return value().equals(o.toString());
    }
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ModuleFactoryID pluginID = (ModuleFactoryID) o;

    return Objects.equals(moduleIdValue, pluginID.moduleIdValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(moduleIdValue);
  }

  @Override
  public String toString() {
    return value();
  }

  @Override
  public int compareTo(String o) {
    return value().compareTo(o);
  }

  public static ModuleFactoryID of(ModuleFactory moduleFactory){
    Objects.requireNonNull(moduleFactory,"Module runner can't be null");
    Class<? extends ModuleFactory> classModuleRunner = moduleFactory.getClass();
    return of(classModuleRunner);
  }

  public static ModuleFactoryID of(Class<? extends ModuleFactory> classModuleFactory){
    return of(classModuleFactory.getCanonicalName());
  }

  public static ModuleFactoryID of(String moduleFactoryKey){
    return new ModuleFactoryID(moduleFactoryKey);
  }
}
