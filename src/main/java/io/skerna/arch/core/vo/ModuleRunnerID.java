package io.skerna.arch.core.vo;

import io.skerna.arch.core.spi.ModuleRunner;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * RunnerID, module runner class
 */
public class ModuleRunnerID implements Comparable<String>  {
  private final  String value;

  private ModuleRunnerID(String value) {
    if(value == null || value.isEmpty()){
      throw new IdentifierException();
    }
    this.value = value;
  }

  private ModuleRunnerID(Class<? extends ModuleRunner> classModuleRunner) {
    this.value = classModuleRunner.getCanonicalName();
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof String) {
      return value.equals(o.toString());
    }
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ModuleRunnerID pluginID = (ModuleRunnerID) o;

    return Objects.equals(value, pluginID.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }

  public static ModuleRunnerID of(ModuleRunner moduleRunner){
    Objects.requireNonNull(moduleRunner,"Module runner can't be null");
    Class<? extends ModuleRunner> classModuleRunner = moduleRunner.getClass();
    return of(classModuleRunner);
  }

  public static ModuleRunnerID of(Class<? extends ModuleRunner> classModuleRunner){
    return of(classModuleRunner.getCanonicalName());
  }

  public static ModuleRunnerID of(String moduleRunnerKey){
    return new ModuleRunnerID(moduleRunnerKey);
  }

  @Override
  public int compareTo(@NotNull String s) {
    return  value.compareTo(s);
  }
}
