package io.skerna.arch.core.vo;

public enum SourceModuleType{
    FILE_SOURCE_MODULE,
    JVM_OBJECT_REFERENCE,
    CLASSPATH_SOURCE_MODULE
}
