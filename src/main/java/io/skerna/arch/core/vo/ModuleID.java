package io.skerna.arch.core.vo;

import io.skerna.arch.core.Module;

import java.util.Objects;

public class ModuleID implements Comparable<String> {
  private String moduleIdValue;
  private ModuleID(String moduleIdValue) {
    this.moduleIdValue = moduleIdValue;
  }


  public String value() {
    return moduleIdValue;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof String) {
      return value().equals(o.toString());
    }
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ModuleID pluginID = (ModuleID) o;

    return Objects.equals(moduleIdValue, pluginID.moduleIdValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(moduleIdValue);
  }

  @Override
  public String toString() {
    return value();
  }

  @Override
  public int compareTo(String o) {
    return value().compareTo(o);
  }

  public static ModuleID of(String rawPluginId) {
    if(rawPluginId == null || rawPluginId.isEmpty()){
      throw new IdentifierException();
    }
    return new ModuleID(rawPluginId);
  }

  public static ModuleID of(Class<? extends Module> classModuleRunner){
    return of(classModuleRunner.getCanonicalName());
  }

}
