package io.skerna.arch.core;


public class ReactorRuntimeException extends ArchException {

  public ReactorRuntimeException() {
    super("RUNTIME_ERROR");
  }
}
