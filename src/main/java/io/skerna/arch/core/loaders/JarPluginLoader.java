package io.skerna.arch.core.loaders;

import io.skerna.arch.core.ModuleClassLoader;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.arch.core.vo.SourceModule;

public class JarPluginLoader implements PluginLoader {

  protected ModuleManager moduleManager;
  /**
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.moduleManager = moduleManager;
  }


  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    if(!ClassUtils.isOfType(sourceModule, SourceFileModule.class)){
      return false;
    }
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    //return Files.exists(pluginPath) && FileUtils.isJarFile(pluginPath);
    return sourceFileModule.exists() && sourceFileModule.isJarFile();
  }

  @Override
  public ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor) {
  SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    ModuleClassLoader moduleClassLoader = new ModuleClassLoader(moduleManager, pluginDescriptor, getClass().getClassLoader(),true);
    moduleClassLoader.addFile(sourceFileModule.file.toFile());
//    List<File> files = FileUtils.getJars(Path.of("/tmp/skerna/lib"));
//    for (File file : files) {
//      moduleClassLoader.addFile(file);
//    }
    return moduleClassLoader;
  }


}
