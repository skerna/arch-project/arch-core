package io.skerna.arch.core.loaders;

import io.skerna.arch.core.processor.Processor;
import io.skerna.arch.core.processor.Reporter;

import javax.annotation.processing.Filer;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AbstractStore {
  protected Reporter reporter;
  protected Processor processor;
  protected String outputDirectory;
  protected Path pathOutDirectory;
  public AbstractStore(Processor processor,String outputDirectory ) {
    this.reporter = processor.getReporter();
    this.processor = processor;
    this.outputDirectory = outputDirectory;
    this.pathOutDirectory = Paths.get(outputDirectory);
  }

  /**
   * Helper method.
   */
  protected Filer getFiler() {
    return processor.getFiler();
  }


}
