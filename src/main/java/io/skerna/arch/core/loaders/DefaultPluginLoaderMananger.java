package io.skerna.arch.core.loaders;

import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 * DefaultPluginLoaderMananger, default implementation of {@link PluginLoaderManager}
 * @implSpec this implementation allow create {@link PluginLoader} composed by several
 * loaders and them using all strategies avaible to create or find existent classLoader
 */
public class DefaultPluginLoaderMananger implements PluginLoaderManager {
  private static final Logger log = LoggerFactory.logger(DefaultPluginLoaderMananger.class);

  private Map<SourceModule, Set<ModuleID>> pluginsInPath = new HashMap<>();
  private Map<SourceModule, EntryLoaders> classLoadersMap = new HashMap<>();
  private ModuleManager manager;
  private List<PluginLoader> loaders = new ArrayList<>();

  /**
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.manager = moduleManager;
  }

  /**
   * add, this method allow add new {@link PluginLoader}
   * @param loader null is not allowed
   * @return
   */
  public DefaultPluginLoaderMananger add(PluginLoader loader) {
    if (loader == null) {
      throw new IllegalArgumentException("null not allowed");
    }
    loader.bindManager(manager);
    loaders.add(loader);

    return this;
  }

  /**
   * size, returns number of Loader currently registered in {@link PluginLoaderManager}
   * @return
   */
  public int size() {
    return loaders.size();
  }

  /**
   * isApplicable, inspect all loaders for one that returns true
   * @param sourceModule
   * @return
   */
  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    for (PluginLoader loader : loaders) {
      if (loader.isApplicable(sourceModule)) {
        return true;
      }
    }

    return false;
  }

  /**
   * getPluginClassLoader, returns ClassLoader for one module
   * @param moduleId
   * @return
   */
  @Override
  public Optional<ClassLoader> getPluginClassLoader(ModuleID moduleId) {
    return pluginsInPath.entrySet()
      .stream()
      .filter((k) -> k.getValue().contains(moduleId))
      .map(keyLoader -> classLoadersMap.get(keyLoader.getKey()))
      .map((e) -> e.classLoader)
      .findFirst();
  }

  @Override
  public synchronized ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor) {
    EntryLoaders classLoaderEntry = classLoadersMap.get(sourceModule);

    ModuleID pluginID = pluginDescriptor.getModuleID();
    Set<ModuleID> ids = pluginsInPath.computeIfAbsent(sourceModule, k -> new HashSet<>());
    ids.add(pluginID);


    if (classLoaderEntry != null) {
      classLoaderEntry.increment();
      return classLoaderEntry.classLoader;
    }


    for (PluginLoader loader : loaders) {
      if (loader.isApplicable(sourceModule)) {
        log.atDebug().log("'{}' is applicable for plugin '{}'", loader, sourceModule);
        try {

          ClassLoader classLoader = loader.loadPlugin(sourceModule, pluginDescriptor);
          if (classLoader != null) {
            EntryLoaders entryLoaders = new EntryLoaders(classLoader);
            this.classLoadersMap.put(sourceModule, entryLoaders);
            return classLoader;
          }
        } catch (Exception e) {
          // log the exception and continue with the next loader
          log.atError().log(e.getMessage()); // ?!
        }
      } else {
        log.atDebug().log("'{}' is not applicable for plugin '{}'", loader, sourceModule);
      }
    }

    throw new RuntimeException("No PluginLoader for plugin '" + sourceModule + "' and descriptor '" + pluginDescriptor + "'");
  }

  /**
   * Check if mananger has classloader for path
   *
   * @param sourceModule
   * @return
   */
  @Override
  public boolean hasClassloaderForpath(SourceModule sourceModule) {
    return false;
  }

  /**
   * Remove existent classloader from cache loaders
   *
   * @param sourceModule
   */
  @Override
  public synchronized void removeClassLoader(SourceModule sourceModule) {
    EntryLoaders entry = classLoadersMap.get(sourceModule);
    if (!entry.hasReferences()) {
      classLoadersMap.remove(sourceModule);
    }
  }

  /**
   * Remove existent classloader from cache loaders
   *
   * @param pluginID
   */
  @Override
  public void removeClassLoader(ModuleID pluginID) {
    Optional<Map.Entry<SourceModule, Set<ModuleID>>> entryKey = pluginsInPath.entrySet()
      .stream()
      .filter((k) -> k.getValue().contains(pluginID))
      .findFirst();

    if (entryKey.isPresent() == false) {
      log.atWarning().log("Attempt remove plugin from map using Plugin ID {} ", pluginID);
      return;
    }
    SourceModule key = entryKey.get().getKey();
    EntryLoaders entryLoaders = classLoadersMap.get(key);
    // decrement references and check if has references
    entryLoaders.decrement();
    if (entryLoaders.hasReferences()) {
      log.atDebug().log("current class loader has references, imposible close classloeader");
      return;
    }
    ClassLoader classLoader = entryLoaders.classLoader;
    if (classLoader instanceof Closeable) {
      try {
        ((Closeable) classLoader).close();
        log.atInfo().log("task:[UNLOAD_PLUGIN] ClassLoader closed");
      } catch (IOException e) {
        log.atError().log("task:[UNLOAD_PLUGIN] Cannot close classloader", e);
      }
    }
    classLoadersMap.remove(key);
    pluginsInPath.remove(key);

  }


  private class EntryLoaders {
    private final ClassLoader classLoader;
    private int referenceCounter;

    public EntryLoaders(ClassLoader classLoader) {
      this.classLoader = classLoader;
      this.referenceCounter = 1;
    }

    synchronized void increment() {
      referenceCounter = referenceCounter + 1;
    }

    synchronized void decrement() {
      referenceCounter = referenceCounter - 1;
    }

    boolean hasReferences() {
      return referenceCounter > 0;
    }

  }
}
