package io.skerna.arch.core.loaders;

import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.vo.SourceObjectModule;
import io.skerna.arch.core.ModuleClassLoader;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.SourceModule;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

/**
 * Load all information needed by a plugin.
 * This means add to classpath all jar files from {@code lib} directory
 * and all class files from {@code classes}.
 * <p>
 * *
 */
public class InstancePluginLoader implements PluginLoader {

  protected ModuleManager moduleManager;

  public InstancePluginLoader() {
  }

  /**
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.moduleManager = moduleManager;
  }

  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    return ClassUtils.isOfType(sourceModule, SourceObjectModule.class);
  }

  @Override
  public ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor) {
    SourceObjectModule sourceObjectModule = (SourceObjectModule) sourceModule;
    ModuleClassLoader moduleClassLoader = createPluginClassLoader(sourceObjectModule, pluginDescriptor);
    List<File> files = FileUtils.getJars(Path.of("arch_lib/"));
    for (File file : files) {
      moduleClassLoader.addFile(file);
    }
    return moduleClassLoader;
  }

  protected ModuleClassLoader createPluginClassLoader(SourceObjectModule sourceObjectModule, ModuleDescriptor pluginDescriptor) {
    return new ModuleClassLoader(moduleManager, pluginDescriptor, sourceObjectModule.instance.getClass().getClassLoader(),false);
  }

}
