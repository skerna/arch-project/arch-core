package io.skerna.arch.core.loaders;

import io.skerna.arch.core.Initializable;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.vo.SourceModule;

import java.nio.file.Path;

public interface PluginLoader extends Initializable {

  /**
   * Returns true if this loader is applicable to the given {@link Path}.
   *
   * @param sourceModule
   * @return
   */
  boolean isApplicable(SourceModule sourceModule);

  ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor);

}
