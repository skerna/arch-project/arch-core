package io.skerna.arch.core.loaders;

import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.classpath.PluginClasspath;
import io.skerna.arch.core.ModuleClassLoader;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.arch.core.vo.SourceModule;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

/**
 * Load all information needed by a plugin.
 * This means add to classpath all jar files from {@code lib} directory
 * and all class files from {@code classes}.
 * <p>
 * *
 */
public class DefaultPluginLoader implements PluginLoader {

  protected ModuleManager moduleManager;
  protected PluginClasspath moduleClasspath;

  public DefaultPluginLoader(PluginClasspath moduleClasspath) {
    this.moduleClasspath = moduleClasspath;
  }

  /**
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.moduleManager = moduleManager;
  }

  @Override
  public boolean isApplicable(SourceModule sourceModule) {
    if(!ClassUtils.isOfType(sourceModule, SourceFileModule.class)){
      return false;
    }
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    return sourceFileModule.exists() && sourceFileModule.isDirectory();
    //return Files.exists(pluginPath) && Files.isDirectory(pluginPath);
  }

  @Override
  public ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor) {
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    ModuleClassLoader moduleClassLoader = createPluginClassLoader(sourceFileModule.file, pluginDescriptor);
    loadClasses(sourceFileModule.file, moduleClassLoader);
    loadJars(sourceFileModule.file, moduleClassLoader);

    return moduleClassLoader;
  }

  protected ModuleClassLoader createPluginClassLoader(Path pluginPath, ModuleDescriptor pluginDescriptor) {
    return new ModuleClassLoader(moduleManager, pluginDescriptor, getClass().getClassLoader());
  }

  /**
   * Add all {@code *.class} files from {@code classes} directories to plugin class loader.
   */
  protected void loadClasses(Path pluginPath, ModuleClassLoader moduleClassLoader) {
    for (String directory : moduleClasspath.getClassesDirectories()) {
      File file = pluginPath.resolve(directory).toFile();
      if (file.exists() && file.isDirectory()) {
        moduleClassLoader.addFile(file);
      }
    }
  }

  /**
   * Add all {@code *.jar} files from {@code lib} directories to plugin class loader.
   */
  protected void loadJars(Path pluginPath, ModuleClassLoader moduleClassLoader) {
    for (String libDirectory : moduleClasspath.getLibDirectories()) {
      Path file = pluginPath.resolve(libDirectory);
      List<File> jars = FileUtils.getJars(file);
      for (File jar : jars) {
        moduleClassLoader.addFile(jar);
      }
    }
  }

}
