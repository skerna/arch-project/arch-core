package io.skerna.arch.core.loaders;

import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceModule;

import java.util.Optional;

public interface PluginLoaderManager extends PluginLoader {
  /**
   * @param moduleId
   * @return
   */
  Optional<ClassLoader> getPluginClassLoader(ModuleID moduleId);

  /**
   * Return classloader, if it not exist use pluginloaders and save loader in cache
   *
   * @param sourceModule
   * @param pluginDescriptor
   * @return
   */
  ClassLoader loadPlugin(SourceModule sourceModule, ModuleDescriptor pluginDescriptor);


  /**
   * Check if mananger has classloader for path
   *
   * @param sourceModule
   * @return
   */
  boolean hasClassloaderForpath(SourceModule sourceModule);

  /**
   * Remove existent classloader from cache loaders
   *
   * @param sourceModule
   */
  void removeClassLoader(SourceModule sourceModule );

  /**
   * Remove existent classloader from cache loaders
   *
   * @param pluginID
   */
  void removeClassLoader(ModuleID pluginID);


}
