package io.skerna.arch.core.loaders;

import io.skerna.arch.core.Constants;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.vo.SourceClasspathModule;
import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.vo.SourceModule;

import java.util.Optional;

/**
 * ClasspathPluginLoader, this class allow create classloader using same classloader app level
 * @implSpec this class implement {@link PluginLoader} using local classloader
 * @implNote default implementation use {@link Constants VIRTUAL_PATH_NAME}
 * to  differentiate the type of classloader to create, for example when Plugin Path
 * contains regex VIRTUAL_PATH_NAME, them this loader is created when isApplicable method is invoqued by {@link PluginLoaderManager}
 */
public class  ClasspathPluginLoader implements PluginLoader {
  protected ModuleManager moduleManager;

  public ClasspathPluginLoader() {
  }

  /**
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.moduleManager = moduleManager;
  }


  /**
   * @implSpec this implementation check if Path of plugins contains {@link Constants } VIRTUAL_PATH_NAME
   * @param sourceModule
   * @return
   */
  @Override
  public boolean isApplicable(SourceModule sourceModule) {

    //return sourceModule.toString().contains(Constants.VIRTUAL_PATH_NAME);
    return ClassUtils.isOfType(sourceModule, SourceClasspathModule.class);
  }

  /**
   * @implSpec this implementation return classloader using local classloader
   * @param sourceModule
   * @param pluginDescriptor
   * @return
   */
  @Override
  public ClassLoader loadPlugin(SourceModule sourceModule ,ModuleDescriptor pluginDescriptor) {
    Optional<ClassLoader> classLoader  = ClassUtils.whenOfType(SourceClasspathModule.class,sourceModule,(action)-> action.classLoader);
    return classLoader.orElseGet(() -> getClass().getClassLoader());
  }

}
