package io.skerna.arch.core.asm;

import io.skerna.arch.core.ExtensionDefinition;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;
import org.objectweb.asm.ClassReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This class holds the parameters of an {@link ExtensionDefinition}
 * annotation defined for a certain class.
 * <p>
 * *
 * *
 */
public final class ExtensionInfo {
  private static final Logger log = LoggerFactory.logger(ExtensionInfo.class);

  private final String className;

  int ordinal = 0;
  List<ModuleID> plugins = new ArrayList<ModuleID>();
  List<String> points = new ArrayList<>();

  private ExtensionInfo(String className) {
    this.className = className;
  }

  /**
   * Load an {@link ExtensionInfo} for a certain class.
   *
   * @param className   absolute class name
   * @param classLoader class loader to access the class
   * @return the {@link ExtensionInfo}, if the class was annotated with an {@link ExtensionDefinition}, otherwise null
   */
  public static ExtensionInfo load(String className, ClassLoader classLoader) {
    try (InputStream input = classLoader.getResourceAsStream(className.replace('.', '/') + ".class")) {
      ExtensionInfo info = new ExtensionInfo(className);
      new ClassReader(input).accept(new ExtensionVisitor(info), ClassReader.SKIP_DEBUG);

      return info;
    } catch (IOException e) {
      log.atError().log(e.getMessage(), e);
      return null;
    }
  }

  /**
   * Get the name of the class, for which extension info was created.
   *
   * @return absolute class name
   */
  public String getClassName() {
    return className;
  }

  /**
   * Get the {@link ExtensionDefinition#ordinal()} value, that was assigned to the extension.
   *
   * @return ordinal value
   */
  public int getOrdinal() {
    return ordinal;
  }

  /**
   * Get the {@link ExtensionDefinition#plugins()} value, that was assigned to the extension.
   *
   * @return ordinal value
   */
  public List<ModuleID> getPlugins() {
    return Collections.unmodifiableList(plugins);
  }

  /**
   * Get the {@link ExtensionDefinition#points()} value, that was assigned to the extension.
   *
   * @return ordinal value
   */
  public List<String> getPoints() {
    return Collections.unmodifiableList(points);
  }

}
