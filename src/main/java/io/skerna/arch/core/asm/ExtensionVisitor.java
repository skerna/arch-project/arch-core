package io.skerna.arch.core.asm;

import io.skerna.arch.core.ExtensionDefinition;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.Arrays;

/**
 * This visitor extracts an {@link ExtensionInfo} from any class,
 * that holds an {@link ExtensionDefinition} annotation.
 * <p>
 * The annotation parameters are extracted from byte code by using the
 * <a href="https://asm.ow2.io/">ASM library</a>. This makes it possible to
 * access the {@link ExtensionDefinition} parameters without loading the class into
 * the class loader. This avoids possible {@link NoClassDefFoundError}'s
 * for extensions, that can't be loaded due to missing dependencies.
 * <p>
 */
class ExtensionVisitor extends ClassVisitor {
  private static final Logger log = LoggerFactory.logger(ExtensionVisitor.class);

  private static final int ASM_VERSION = Opcodes.ASM7;

  private final ExtensionInfo extensionInfo;

  ExtensionVisitor(ExtensionInfo extensionInfo) {
    super(ASM_VERSION);
    this.extensionInfo = extensionInfo;
  }

  @Override
  public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
    if (!Type.getType(descriptor).getClassName().equals(ExtensionDefinition.class.getName())) {
      return super.visitAnnotation(descriptor, visible);
    }

    return new AnnotationVisitor(ASM_VERSION) {

      @Override
      public AnnotationVisitor visitArray(final String name) {
        if ("ordinal".equals(name) || "plugins".equals(name) || "points".equals(name)) {
          return new AnnotationVisitor(ASM_VERSION, super.visitArray(name)) {

            @Override
            public void visit(String key, Object value) {
              log.atDebug().log("Load annotation attribute {} = {} ({})", name, value, value.getClass().getName());
              if ("ordinal".equals(name)) {
                extensionInfo.ordinal = Integer.parseInt(value.toString());
              } else if ("plugins".equals(name)) {
                if (value instanceof String) {
                  log.atDebug().log("Found plugin {}", value);
                  extensionInfo.plugins.add(ModuleID.of((String) value));
                } else if (value instanceof String[]) {
                  log.atDebug().log("Found plugins {}", Arrays.toString((String[]) value));
                  for (int i = 0; i < ((String[]) value).length; i++) {
                    extensionInfo.plugins.add(ModuleID.of(value.toString()));
                  }
                } else {
                  log.atDebug().log("Found plugin {}", value.toString());
                  extensionInfo.plugins.add(ModuleID.of(value.toString()));
                }
              } else {
                String pointClassName = ((Type) value).getClassName();
                log.atDebug().log("Found point " + pointClassName);
                extensionInfo.points.add(pointClassName);
              }

              super.visit(key, value);
            }
          };
        }

        return super.visitArray(name);
      }

    };
  }

}
