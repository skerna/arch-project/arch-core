package io.skerna.arch.core;

import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

/**
 * It's an implementation of {@link ModuleStateObserver} that writes all events to logger (DEBUG level).
 * This listener is added automatically by {@link DefaultModuleManager} for {@code dev} mode.
 * <p>
 */
public class LoggingPluginStateListener implements ModuleStateObserver {

  protected final Logger log = LoggerFactory.logger(LoggingPluginStateListener.class);


  @Override
  public void pluginStateChanged(ModuleStateEvent event) {
    log.atDebug().log("The state of moduleInstance '{}' has changed from '{}' to '{}'", event.getPlugin().getPluginId(),
      event.getOldState(), event.getPluginState());
  }

}
