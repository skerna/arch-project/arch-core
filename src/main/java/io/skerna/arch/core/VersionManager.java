package io.skerna.arch.core;

import java.util.Comparator;


public interface VersionManager {

  /**
   * Check if a {@code constraint} and a {@code version} match.
   * A possible constrain can be {@code >=1.0.0 & <2.0.0}.
   *
   * @param version
   * @param constraint
   * @return
   */
  boolean checkVersionConstraint(String version, String constraint);

  /**
   * Compare two versions. It's similar with {@link Comparator#compare(Object, Object)}.
   *
   * @param v1
   * @param v2
   */
  int compareVersions(String v1, String v2);

}
