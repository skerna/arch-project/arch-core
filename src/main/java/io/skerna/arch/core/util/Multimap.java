package io.skerna.arch.core.util;

import java.util.*;

public class Multimap<K,V> {
    private final Map<K, Set<V>> multimap = new HashMap<>();

  public void put(K key, V value) {
    this.multimap.computeIfAbsent(key, k -> new HashSet<>()).add(value);
  }
  public void remove(K key) {
    this.multimap.remove(key);
  }
  public Set<V> get(K key) {
    return this.multimap.getOrDefault(key,Collections.emptySet());
  }
}
