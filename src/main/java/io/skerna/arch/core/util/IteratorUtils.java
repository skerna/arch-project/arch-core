package io.skerna.arch.core.util;

import java.util.*;

public class IteratorUtils {
  // Function to get the List
  public static <T> List<T> toList(Iterator<T> iterator)
  {

    // Create an empty list
    List<T> list = new ArrayList<>();

    // Add each element of iterator to the List
    iterator.forEachRemaining(list::add);

    // Return the List
    return list;
  }

  // Function to get the set
  public static <T> Set<T> toSet(Iterator<T> iterator)
  {

    // Create an empty list
    Set<T> list = new HashSet<>();

    // Add each element of iterator to the List
    iterator.forEachRemaining(list::add);

    // Return the List
    return list;
  }
}
