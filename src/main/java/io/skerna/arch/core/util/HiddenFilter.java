package io.skerna.arch.core.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Filter that only accepts hidden files.
 * <p>
 * *
 */
public class HiddenFilter implements FileFilter {

  @Override
  public boolean accept(File file) {
    return file.isHidden();
  }

}
