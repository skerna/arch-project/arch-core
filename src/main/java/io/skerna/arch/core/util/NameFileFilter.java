package io.skerna.arch.core.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Filter accepts any file with this name. The case of the filename is ignored.
 * <p>
 * *
 */
public class NameFileFilter implements FileFilter {

  private String name;

  public NameFileFilter(String name) {
    this.name = name;
  }

  @Override
  public boolean accept(File file) {
    // perform a case insensitive check.
    return file.getName().equalsIgnoreCase(name);
  }

}
