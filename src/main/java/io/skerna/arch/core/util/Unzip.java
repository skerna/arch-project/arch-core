package io.skerna.arch.core.util;

import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * This class extracts the content of the plugin zip into a directory.
 * It's a class for only the internal use.
 * <p>
 * *
 */
public class Unzip {

  private static final Logger log = LoggerFactory.logger(Unzip.class);

  /**
   * Holds the destination directory.
   * File will be unzipped into the destination directory.
   */
  private File destination;

  /**
   * Holds path to zip file.
   */
  private File source;

  public Unzip() {
  }

  public Unzip(File source, File destination) {
    this.source = source;
    this.destination = destination;
  }

  public void setSource(File source) {
    this.source = source;
  }

  public void setDestination(File destination) {
    this.destination = destination;
  }

  public void extract() throws IOException {
    log.atDebug().log("Extract content of '{}' to '{}'", source, destination);

    // delete destination file if exists
    if (destination.exists() && destination.isDirectory()) {
      FileUtils.delete(destination.toPath());
    }

    try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(source))) {
      ZipEntry zipEntry;
      while ((zipEntry = zipInputStream.getNextEntry()) != null) {
        try {
          File file = new File(destination, zipEntry.getName());

          // create intermediary directories - sometimes zip don't add them
          File dir = new File(file.getParent());
          dir.mkdirs();

          if (zipEntry.isDirectory()) {
            file.mkdirs();
          } else {
            byte[] buffer = new byte[1024];
            int length;
            try (FileOutputStream fos = new FileOutputStream(file)) {
              while ((length = zipInputStream.read(buffer)) >= 0) {
                fos.write(buffer, 0, length);
              }
            }
          }
        } catch (FileNotFoundException e) {
          log.atError().log("File '{}' not found", zipEntry.getName());
        }
      }
    }
  }

}
