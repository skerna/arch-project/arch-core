package io.skerna.arch.core.util;

public interface Transform<T,R> {
  R transform(T element);
}
