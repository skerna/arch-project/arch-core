package io.skerna.arch.core.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Filter accepts files that are directories.
 * <p>
 * *
 */
public class DirectoryFileFilter implements FileFilter {

  @Override
  public boolean accept(File file) {
    return file.isDirectory();
  }

}
