package io.skerna.arch.core.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Filter accepts any file ending in extension. The case of the filename is ignored.
 * <p>
 * *
 */
public class ExtensionFileFilter implements FileFilter {

  private String extension;

  public ExtensionFileFilter(String extension) {
    this.extension = extension;
  }

  @Override
  public boolean accept(File file) {
    // perform a case insensitive check.
    return file.getName().toUpperCase().endsWith(extension.toUpperCase());
  }

}
