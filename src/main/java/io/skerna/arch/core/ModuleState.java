package io.skerna.arch.core;


public enum ModuleState {
  UNKNOWN,
  /**
   * Event when moduleInstance is installed from path directory
   */
  INSTALLED,
  /**
   * Event when plugins is removed from path (Uninstalled)
   */
  UNINSTALLED,
  /**
   * The runtime knows the moduleInstance is there. It knows about the moduleInstance path, the moduleInstance descriptor.
   */
  CREATED,

  /**
   * The moduleInstance cannot be used.
   */
  DISABLED,

  /**
   * The moduleInstance is created. All the dependencies are created and resolved.
   * The moduleInstance is ready to be started.
   */
  RESOLVED,

  /**
   * The {@link AbstractPlugin#startPlugin()} has executed. A started moduleInstance may contribute extensions.
   */
  STARTED,

  /**
   * The {@link AbstractPlugin#stopPlugin()} has executed.
   */
  STOPPED

}
