package io.skerna.arch.core;

import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.ModuleRunnerID;

import java.nio.file.Path;
import java.util.*;

public class DefaultModuleDescriptor implements ModuleDescriptor {

  private ModuleID moduleId;
  private String description;
  private String moduleClass;
  private String version;
  private String requires = "*"; // SemVer format
  private String provider;
  private List<ModuleDependecy> dependencies = new ArrayList<>();
  private String license;
  private Path source;
  private ModuleType moduleType;
  private ModuleFactoryID moduleFactoryID = null;
  private ModuleRunnerID moduleRunnerID = null;

  private DefaultModuleDescriptor() {
    dependencies = new ArrayList<>();
  }

  private DefaultModuleDescriptor(Builder builder) {
    Objects.requireNonNull(builder);
    Objects.requireNonNull(builder.moduleId);
    Objects.requireNonNull(builder.moduleClass);
    this.moduleId = builder.moduleId;
    this.description = builder.description;
    this.moduleClass = builder.moduleClass;
    this.version = builder.version;
    this.requires = builder.requires;
    this.provider = builder.provider;
    this.dependencies.addAll(builder.dependencies);
    this.license = builder.license;
    this.source = builder.source;
    this.moduleType = builder.moduleType;
    this.moduleFactoryID = builder.moduleFactoryID;
    this.moduleRunnerID = builder.moduleRunnerID;
  }

  public void addDependency(ModuleDependecy dependency) {
    this.dependencies.add(dependency);
  }

  /**
   * Returns the unique identifier of this plugin.
   */
  @Override
  public ModuleID getModuleID() {
    return moduleId;
  }

  /**
   * Returns the description of this plugin.
   */
  @Override
  public String getDescription() {
    return description;
  }

  /**
   * Returns the name of the class that implements Plugin interface.
   */
  @Override
  public String getModuleClass() {
    return moduleClass;
  }

  /**
   * Returns the version of this plugin.
   */
  @Override
  public String getVersion() {
    return version;
  }

  /**
   * Returns string version of requires
   *
   * @return String with requires expression on SemVer format
   */
  @Override
  public String getRequires() {
    return requires;
  }

  /**
   * Returns the provider name of this plugin.
   */
  @Override
  public String getProvider() {
    return provider;
  }

  /**
   * Returns the legal license of this plugin, e.g. "Apache-2.0", "MIT" etc
   */
  @Override
  public String getLicense() {
    return license;
  }

  /**
   * Returns all dependencies declared by this plugin.
   * Returns an empty array if this plugin does not declare any require.
   */
  @Override
  public List<ModuleDependecy> getDependencies() {
    return dependencies;
  }

  @Override
  public Path source() {
    return source;
  }

  @Override
  public ModuleFactoryID getModuleFactoryID() {
    return moduleFactoryID;
  }

  @Override
  public ModuleRunnerID getModuleRunnerID() {
    return this.moduleRunnerID;
  }

  @Override
  public ModuleType getModuleType() {
    return moduleType;
  }

  @Override
  public String toString() {
    return "PluginDescriptor [moduleId=" + moduleId + ", moduleClass="
      + moduleClass + ", version=" + version + ", provider="
      + provider + ", dependencies=" + dependencies + ", description="
      + description + ", requires=" + requires + ", license="
      + license + "]";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DefaultModuleDescriptor that = (DefaultModuleDescriptor) o;
    return moduleId.equals(that.moduleId) &&
      moduleClass.equals(that.moduleClass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(moduleId, moduleClass);
  }

  protected ModuleDescriptor setDependencies(String dependencies) {
    if (dependencies != null) {
      dependencies = dependencies.trim();
      if (dependencies.isEmpty()) {
        this.dependencies = Collections.emptyList();
      } else {
        this.dependencies = new ArrayList<>();
        String[] tokens = dependencies.split(",");
        for (String dependency : tokens) {
          dependency = dependency.trim();
          if (!dependency.isEmpty()) {
            this.dependencies.add(new ModuleDependecy(ModuleID.of(dependency)));
          }
        }
        if (this.dependencies.isEmpty()) {
          this.dependencies = Collections.emptyList();
        }
      }
    } else {
      this.dependencies = Collections.emptyList();
    }

    return this;
  }

  protected ModuleDescriptor setModuleType(ModuleType moduleType) {
    this.moduleType = moduleType;
    return this;
  }

  protected ModuleDescriptor setModuleRunnerID(ModuleRunnerID moduleRunnerID) {
    this.moduleRunnerID = moduleRunnerID;
    return this;
  }

  protected ModuleDescriptor setModuleFactoryID(ModuleFactoryID moduleFactoryID) {
    this.moduleFactoryID = moduleFactoryID;
    return this;
  }

  public ModuleDescriptor setLicense(String license) {
    this.license = license;

    return this;
  }

  public static Builder builder(ModuleID moduleID,String moduleClass) {
    return new Builder(moduleID,moduleClass);
  }

  public static Builder builder(Class<? extends Module> moduleClass){
    ModuleID moduleID = ModuleID.of(moduleClass);
    String moduleClassname = moduleClass.getCanonicalName();
    return builder(moduleID,moduleClassname);
  }


  public static class Builder {
    public ModuleID moduleId;
    public String description;
    public String moduleClass;
    public String version;
    public String requires = "*"; // SemVer format
    public String provider;
    public Set<ModuleDependecy> dependencies = new HashSet<>();
    public String license;
    public Path source;
    public ModuleType moduleType;
    public ModuleFactoryID moduleFactoryID = null;
    public ModuleRunnerID moduleRunnerID = null;

    public Builder(ModuleID moduleId, String moduleClass) {
      this.moduleId = moduleId;
      this.moduleClass = moduleClass;
    }

    public Builder setModuleDescription(String description) {
      this.description = description;
      return this;
    }

    public Builder setModuleClass(String moduleClass) {
      this.moduleClass = moduleClass;
      return this;
    }
    public Builder setModuleClass(Class<? extends Module> moduleClass) {
      this.moduleClass = moduleClass.getCanonicalName();
      return this;
    }

    public Builder setVersion(String version) {
      this.version = version;
      return this;
    }

    /**
     * setRequires, define minimal system version for plugin deploy
     * @implNote requires is used for determinate if plugin will
     * @param requires
     * @return
     */
    public Builder setRequires(String requires) {
      this.requires = requires;
      return this;
    }

    public Builder setProvider(String provider) {
      this.provider = provider;
      return this;
    }

    public Builder setModuleDependecies(List<ModuleDependecy> dependencies) {
      this.dependencies.addAll(dependencies);
      return this;
    }

    public Builder setModuleDependecies(String moduleDependeciesDelimited) {
      Objects.requireNonNull(moduleDependeciesDelimited,"Module delimited dependencies can not be null");
      moduleDependeciesDelimited = moduleDependeciesDelimited.trim();
      if (!moduleDependeciesDelimited.isEmpty()) {
        String[] tokens = moduleDependeciesDelimited.split(Constants.DEPENDECY_LIST_DELIMITED);
        for (String dependency : tokens) {
          dependency = dependency.trim();
          if (!dependency.isEmpty()) {
            addDependency(new ModuleDependecy(ModuleID.of(dependency)));
          }
        }
      }
      return this;
    }

    public Builder setLicence(String licence) {
      this.license = licence;
      return this;
    }

    public Builder setPath(Path source) {
      this.source = source;
      return this;
    }

    public Builder setModuleType(ModuleType moduleType) {
      this.moduleType = moduleType;
      return this;
    }

    public Builder setModuleFactoryID(ModuleFactoryID moduleFactoryId) {
      this.moduleFactoryID = moduleFactoryId;
      return this;
    }

    public Builder setModuleRunnerID(ModuleRunnerID moduleRunnerID) {
      this.moduleRunnerID = moduleRunnerID;
      return this;
    }

    public Builder addDependency(ModuleDependecy moduleDependecy) {
      dependencies.add(moduleDependecy);
      return this;
    }

    public DefaultModuleDescriptor build() {
      return new DefaultModuleDescriptor(this);
    }
  }
}
