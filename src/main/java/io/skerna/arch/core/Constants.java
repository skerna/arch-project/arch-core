package io.skerna.arch.core;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Constants, represents System module definitions
 */
public class Constants {
  /**
   * MODULE_PACKAGE_PREFIX, is root package arch project, is used by classloader loggic
   * @see ModuleClassLoader
   */
  public static final String MODULE_PACKAGE_PREFIX="io.skerna.arch";

  /**
   * VIRTUAL_PATH_NAME, is virtual path used for build modules using same app classloader instead of create
   * new classpaths for modules in same Loader
   */
  public static final String VIRTUAL_PATH_NAME = "virtual-path";
  /**
   * VIRTUAL_PATH, is virtual path used for build modules using same app classloader instead of create
   * new classpaths for modules in same Loader
   */
  public static final Path VIRTUAL_PATH = Paths.get(VIRTUAL_PATH_NAME);
  /**
   * Module dependecy delimiter ','
   */
  public static final String DEPENDECY_LIST_DELIMITED = ",";

  public static final String DEFAULT_OUTPUT_DIRECTORY = "META-INF/plugins";


}
