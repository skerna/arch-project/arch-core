package io.skerna.arch.core;

import io.skerna.arch.core.classpath.DevelopmentPluginClasspath;
import io.skerna.arch.core.classpath.PluginClasspath;
import io.skerna.arch.core.factories.DefaultExtensionFactoryManager;
import io.skerna.arch.core.repositories.CompoundPluginRepository;
import io.skerna.arch.core.repositories.DefaultPluginRepository;
import io.skerna.arch.core.classpath.DefaultPluginClasspath;
import io.skerna.arch.core.factories.DefaultModuleFactoryManager;
import io.skerna.arch.core.factories.ExtensionFactoryManager;
import io.skerna.arch.core.factories.ModuleFactoryManager;
import io.skerna.arch.core.repositories.JarPluginRepository;
import io.skerna.arch.core.repositories.PluginRepository;
import io.skerna.arch.core.runners.RunnerManager;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.arch.core.finders.*;
import io.skerna.arch.core.loaders.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Default implementation of the {@link ModuleManager} interface.
 *
 * <p>This class is not thread-safe.
 * <p>
 */
public class DefaultModuleManager extends AbstractModuleManager {
  private static final Logger log = LoggerFactory.logger(DefaultModuleManager.class);

  protected PluginClasspath moduleClasspath;

  public DefaultModuleManager(ModuleManagerBuilder<?, ?> builder) {
    super(builder);
  }


  @Override
  protected ModuleDescriptorFinder createPluginDescriptorFinder() {
    return new CompoundModuleDescriptorFinder()
      .add(new ClasspathModuleDescriptorFinder());
  }

  @Override
  protected ExtensionFinder createExtensionFinder() {
    DefaultExtensionFinder extensionFinder = new DefaultExtensionFinder(this);
    registerObserver(extensionFinder);

    return extensionFinder;
  }

  @Override
  protected ModuleFactoryManager createPluginFactory() {
    return new DefaultModuleFactoryManager(true);
  }

  @Override
  protected ExtensionFactoryManager createExtensionFactory() {
    return new DefaultExtensionFactoryManager(this);
  }

  @Override
  protected ModuleStatusProvider createPluginStatusProvider() {
    String configDir = System.getProperty("reactor.pluginsConfigDir");
    Path configPath = configDir != null ? Paths.get(configDir) : getPluginsRoot();
    return new DefaultPluginStatusProvider(configPath);
  }

  @Override
  protected PluginRepository createPluginRepository() {
    return new CompoundPluginRepository()
      .add(new DefaultPluginRepository(getPluginsRoot(), isDevelopment()))
      .add(new JarPluginRepository(getPluginsRoot()));
  }

  @Override
  protected PluginLoaderManager createPluginLoaderManager() {
    return new DefaultPluginLoaderMananger()
      .add(new ClasspathPluginLoader())
      .add(new DefaultPluginLoader(moduleClasspath))
      .add(new JarPluginLoader());
  }

  @Override
  protected VersionManager createVersionManager() {
    return new DefaultVersionManager();
  }

  @Override
  protected ModuleRunner createPluginRuner() {
    return new RunnerManager( true);
  }

  @Override
  protected ExecutorService createExecutorService() {
    return Executors.newFixedThreadPool(1);
  }

  /**
   * By default if {@link DefaultModuleManager#isDevelopment()} returns true
   * than a {@link DevelopmentPluginClasspath} is returned
   * else this method returns {@link DefaultPluginClasspath}.
   */
  protected PluginClasspath createPluginClasspath() {
    return isDevelopment() ? new DevelopmentPluginClasspath() : new DefaultPluginClasspath();
  }

  @Override
  protected void initialize() {
    moduleClasspath = createPluginClasspath();

    super.initialize();

    if (isDevelopment()) {
      registerObserver(new LoggingPluginStateListener());
    }

    log.atInfo().log("ModuleManage version {} in '{}' mode", getVersion(), getRuntimeMode());
  }

  /**
   * Load a moduleInstance from disk. If the path is a zip file, first unpack
   *
   * @param sourceModule moduleInstance location on disk
   * @return PluginWrapper for the loaded moduleInstance or null if not loaded
   * @throws ReactorException if problems during load
   */
  @Override
  protected Set<ModuleWrapper> loadPluginsFromPath(SourceModule sourceModule) throws ReactorException {
    // First unzip any ZIP files
    try {
      if(ClassUtils.isOfType(sourceModule, SourceFileModule.class)){
        SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
        FileUtils.expandIfZip(sourceFileModule.file);
      }
    } catch (Exception e) {
      log.atWarning().log("Failed to unzip " + sourceModule, e);
      return null;
    }

    return super.loadPluginsFromPath(sourceModule);
  }

  public static DefaultModuleManagerBuilder builder(){
    return new DefaultModuleManagerBuilder();
  }
}
