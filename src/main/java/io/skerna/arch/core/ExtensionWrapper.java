package io.skerna.arch.core;

import io.skerna.arch.core.spi.ExtensionFactory;

public class ExtensionWrapper<T> implements Comparable<ExtensionWrapper<T>> {

  private final ExtensionDescriptor descriptor;
  private final ExtensionFactory extensionFactory;
  private T extension; // cache

  public ExtensionWrapper(ExtensionDescriptor descriptor, ExtensionFactory extensionFactory) {
    this.descriptor = descriptor;
    this.extensionFactory = extensionFactory;
  }

  @SuppressWarnings("unchecked")
  public T getExtension() {
    if (extension == null) {
      try {
        extension = (T) extensionFactory.create(descriptor.extensionClass);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return extension;
  }

  public ExtensionDescriptor getDescriptor() {
    return descriptor;
  }

  public int getOrdinal() {
    return descriptor.ordinal;
  }

  @Override
  public int compareTo(ExtensionWrapper<T> o) {
    return (getOrdinal() - o.getOrdinal());
  }

}
