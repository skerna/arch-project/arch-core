package io.skerna.arch.core.cdi;

public interface AdapterCDI<T> {

  void accept(T object);

}
