package io.skerna.arch.core;

import io.skerna.arch.core.vo.ChangeStateResult;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public abstract class ModuleManagerReactive extends AbstractModuleManager implements Executable {
  private static final Logger log = LoggerFactory.logger(ModuleManagerReactive.class);
  private static final List<Boolean> RESULT_LIST_SUCCESS = new ArrayList<>();
  private Map<Path, Instant> currentlyInstalling = new ConcurrentHashMap<>();
  private ModulePathWatcher pluginMonitor;

  public ModuleManagerReactive(ModuleManagerReactiveBuilder<?,?> builder) {
    super(builder);
    this.pluginMonitor = builder.getPluginMonitor();
  }

  @Override
  protected void initialize() {
    super.initialize();
    if(pluginMonitor == null){
      log.atTrace().log("Using deault pluginMonitor");
      this.pluginMonitor = createPluginMonitor();
    }
  }


  /**
   * createExecutorService, provide executor service
   *
   * @return
   */
  protected ModulePathWatcher createPluginMonitor() {
    return ModulePathWatcher.builder()
      .addDirectory(getPluginsRoot())
      .setExecutorService(Executors.newSingleThreadExecutor())
      .build(new PluginpathChangeListener());

  }

  @Override
  public void start() {
    log.atDebug().log("Start AbstractPluginManagerReactive");
    pluginMonitor.start();
  }

  @Override
  public void stop() {
    log.atDebug().log("Stop AbstractPluginManagerReactive");
    pluginMonitor.stop();
  }

  /**
   * Internal Event listener from moduleInstance directory path, each event as new jar or zip
   * on moduleInstance path is fired to onChange
   */
  private class PluginpathChangeListener implements ModulePathWatcher.Listener {

    @Override
    public void onChange(ModulePathWatcher.Event event, Path dir, Path path) {
      Path modulePath = dir.resolve(path);
      log.atDebug().log("task:[INSTALLER], New event from plugins path {} on path {} ", event.name(), modulePath);
      log.atDebug().log("task:[INSTALLER], module exists? {}", modulePath.toFile().exists());
      switch (event) {
        case ENTRY_CREATE: {
          if (isIntalling(modulePath)) {
            return;
          }
          currentlyInstalling.put(modulePath, Instant.now());
          createPluginFromFsEvent(event, dir, path).whenComplete((e, t) -> {
            if (t != null) {
              log.atWarning().log("task:[INSTALLER], imposible install {}", modulePath);
              log.atError().log("",e);
            }
            log.atDebug().log("task:[INSTALLER] success install {}", modulePath);
            currentlyInstalling.remove(modulePath);
          });
          break;
        }
        case ENTRY_DELETE: {
          if (isIntalling(modulePath)) {
            return;
          }
          handleUnhandleCompletable(deletePluginFromFsEvent(event, dir, path), "PLUGIN_UNINSTALLED");
          break;
        }
        case ENTRY_MODIFY:
          if (isIntalling(modulePath)) {
            return;
          }
          currentlyInstalling.put(modulePath, Instant.now());
          deletePluginFromFsEvent(event, dir, path)
            .thenCompose(aBoolean -> whenFileEditIsCompleted(modulePath))
            .thenCompose((p) -> createPluginFromFsEvent(event, dir, path))
            .whenComplete((pluginState, throwable) -> {
              if (throwable != null) {
                currentlyInstalling.remove(modulePath);
                return;
              }
              currentlyInstalling.remove(modulePath);
            });
          break;

      }
    }

    private boolean isIntalling(Path modulePath) {
      if (currentlyInstalling.containsKey(modulePath)) {
        log.atInfo().log("task:[INSTALLER],{} installer is running", modulePath);
        return true;
      }
      log.atDebug().log("task:[INSTALLER],{} installer avaible", modulePath);
      return false;
    }

    private CompletableFuture<Path> whenFileEditIsCompleted(Path fileTest) {
      File currentFile = fileTest.toFile();
      if (!currentFile.exists()) {
        CompletableFuture<Path> fstR = new CompletableFuture<>();
        fstR.completeExceptionally(new IllegalStateException("ERROR file not exist"));
        return fstR;
        //return CompletableFuture.failedFuture();
      }
      return CompletableFuture.supplyAsync(() -> {
        log.atDebug().log("Waiting for next check Interval");

        long lastSizeFile = currentFile.length();
        try {
          Thread.sleep(1500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        long lastSizeFile2 = currentFile.length();

        while (currentFile.length() > lastSizeFile) {
          log.atDebug().log("Waiting for next check Interval");
          lastSizeFile = currentFile.length();
          try {
            Thread.sleep(3000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        }
        log.atDebug().log("WASABY OK FILE COMPLETED");
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        return fileTest;
      });
    }

    private CompletableFuture<List<ChangeStateResult>> createPluginFromFsEvent(ModulePathWatcher.Event event, Path dir, Path path) {
      return CompletableFuture.supplyAsync(() -> {
        Path directtory = Paths.get(dir.toUri());
        Path module = directtory.resolve(path.toString());
        log.atDebug().log("New moduleInstance auto finded on path");
        Set<ModuleID> moduleIds = idForPath(new SourceFileModule(module));
        if (moduleIds.size() > 0) {
          log.atDebug().log("Plugin is already load, skipe loading");
        } else {
          moduleIds = loadModules(new SourceFileModule(module));
        }
        log.atDebug().log("Reactive total plugins {}",moduleIds );
        List<ChangeStateResult> changeStateResults = new ArrayList<>();
        for (ModuleID moduleId : moduleIds) {
          ChangeStateResult state = startModule(moduleId).join();
          changeStateResults.add(state);
        }
        return changeStateResults;
      });

    }

    private CompletableFuture<List<Boolean>> deletePluginFromFsEvent(ModulePathWatcher.Event event, Path dir, Path path) {
      Path fullPaht = dir.resolve(path);
      log.atDebug().log("A moduleInstance was uninstalled from path plugins");
      List<ModuleID> resultFinder = plugins.entrySet().stream()
        .filter(entry -> entry.getValue().getSourceModule().equals(fullPaht))
        .map(Map.Entry::getKey)
        .collect(Collectors.toList());

      if (resultFinder.isEmpty()) {
        log.atWarning().log("Plugin with path {} not found in loadedPlugins", fullPaht);

        return CompletableFuture.completedFuture(new ArrayList<>());
      }
      log.atDebug().log("Ok Plugin found with path {}", fullPaht);
      return CompletableFuture.supplyAsync(() -> {
        List<Boolean> changeStateResults = new ArrayList<>();
        for (ModuleID pluginID : resultFinder) {
          Boolean result = unloadModule(pluginID).join();
          changeStateResults.add(result);
        }
        return changeStateResults;
      });

      // this method stopModule and unload moduleInstance
    }

    private void handleUnhandleCompletable(CompletableFuture<?> completableFuture, String taskName) {
      completableFuture.whenComplete(new BiConsumer<Object, Throwable>() {
        /**
         * Performs this operation on the given arguments.
         *
         * @param o         the first input argument
         * @param throwable the second input argument
         */
        @Override
        public void accept(Object o, Throwable throwable) {
          if (throwable != null) {
            log.atError().log("task:[{}] Completable ended with atError().log", taskName, throwable);
            return;
          }
          log.atDebug().log("task:[{}] ended susccessfull", taskName);
        }
      });
    }
  }





}
