package io.skerna.arch.core;


import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * archConfig, this class keep the settings for Arch System as
 * - Debug flag
 * - Plugin scan directory
 * - Arch runtimeMode {@link RuntimeMode}
 */
public class ReactorConfig {
  private static final Logger logger = LoggerFactory.logger(ReactorConfig.class);
  public static final String SYS_PROPERTY_DEBUG = "arch.debug";
  public static final String SYS_PROPERTY_MODULES_DIR = "arch.module_path";
  public static final String SYS_PROPERTY_DEPLOY_MODE = "arch.mode";
  public static final String SYS_PROPERTY_CLASSPATH_DIR = "arch.module_classpath";

  /**
   * Return boolean = true flag if debug is enable
   * this property is system property {@literal archConfig.SYS_PROPERTY_DEBUG}
   *
   * @return boolean
   */
  public static boolean isDebugEnabled() {
    String strValue = System.getProperty(SYS_PROPERTY_DEBUG, "false");
    return Boolean.valueOf(strValue);
  }

  /**
   * Return flag boolean, true if current mode is development
   * this property is system property {@literal archConfig.SYS_PROPERTY_DEPLOY_MODE}
   *
   * @return
   */
  public static boolean isDevelopment() {
    return currentRuntimeMode().equals(RuntimeMode.DEVELOPMENT);
  }


  public static RuntimeMode currentRuntimeMode() {
    String modeAsString = System.getProperty(SYS_PROPERTY_DEPLOY_MODE, RuntimeMode.DEPLOYMENT.toString());
    RuntimeMode runtimeMode = RuntimeMode.byName(modeAsString);
    return runtimeMode;
  }

  /**
   * Return modules path configuration
   * this property is system property {@literal archConfig.SYS_PROPERTY_DEPLOY_MODE}
   *
   * @return
   */
  public static Path getModulePath() {
    String pluginsDir = System.getProperty(SYS_PROPERTY_MODULES_DIR);
    if (pluginsDir == null) {
      logger.atWarning().log("Module directory not defined using default");
      if (isDevelopment()) {
        pluginsDir = "../modules";
      } else {
        pluginsDir = "./modules";
      }
    }

    Path path= Paths.get(pluginsDir);
    return path;
  }

  /**
   * Return modules path configuration
   * this property is system property {@literal archConfig.SYS_PROPERTY_DEPLOY_MODE}
   *
   * @return
   */
  public static Path getModuleClassPath() {
    String moduleClasspath = System.getProperty(SYS_PROPERTY_CLASSPATH_DIR);
    if (moduleClasspath == null) {
      moduleClasspath = "module_classpath";
    }

    return Paths.get(moduleClasspath);
  }
}
