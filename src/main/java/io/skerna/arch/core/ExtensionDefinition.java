package io.skerna.arch.core;

import io.skerna.arch.core.finders.AbstractExtensionFinder;
import org.atteo.classindex.IndexAnnotated;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * *
 */
@Retention(RUNTIME)
@Target(TYPE)
@Inherited
@Documented
@IndexAnnotated
public @interface ExtensionDefinition {

  int ordinal() default 0;

  Class<? extends ExtensionPoint>[] points() default {};

  /**
   * An array of moduleInstance IDs, that have to be available in order to load this extension.
   * The {@link AbstractExtensionFinder} won't load this extension, if these plugins are not
   * available / started at runtime.
   * <p>
   * Notice: This feature requires the optional <a href="https://asm.ow2.io/">ASM library</a>
   * to be available on the applications classpath and has to be explicitly enabled via
   * {@link AbstractExtensionFinder#setCheckForExtensionDependencies(boolean)}.
   *
   * @return moduleInstance IDs, that have to be available in order to load this extension
   */
  String[] plugins() default {};

}
