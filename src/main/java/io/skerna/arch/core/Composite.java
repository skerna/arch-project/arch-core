package io.skerna.arch.core;

import io.skerna.arch.core.spi.BaseService;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.*;

/**
 * @author Ronald Cárdenas
 * project: skerna-arch created at 21/03/19
 * {@link Composite} this abstract class share standard base code to create
 * manager of services, implementing composite pattern of different strategies (Algoritmos)
 * for resolve problems.
 * <p>
 * * ELEMENTS_MANAGE - EXPANSIVE_TYPE is type of elements acceptables in composite pattern
 * * RETURN_TYPE return TYPE then INVOKE MOETHOD flow api
 **/
public abstract class Composite<ELEMENTS_MANAGE extends BaseService, FLOW_TYPE>
  implements Flow<ELEMENTS_MANAGE, FLOW_TYPE> {
  private static final Logger log = LoggerFactory.logger(Composite.class);


  protected ModuleManager moduleManager;
  protected boolean allowAutoScan;
  protected Set<ELEMENTS_MANAGE> components;
  private Class<ELEMENTS_MANAGE> targetClass;


  /**
   * Constructor, requiere next args, when targetclass is contract of META-INF-service
   *
   * @param moduleManager
   * @param allowAutoScan
   * @param targetClass
   */
  public Composite(ModuleManager moduleManager, boolean allowAutoScan, Class<ELEMENTS_MANAGE> targetClass) {
    this.moduleManager = moduleManager;
    this.allowAutoScan = allowAutoScan;
    this.targetClass = targetClass;
    this.components = new HashSet<>();
    initialize();
  }

  void initialize() {
    if (allowAutoScan) {
      log.atTrace().log("Enabled autoscan!!! all service in META-INF will be registered");
      Iterator<ELEMENTS_MANAGE> services = ServiceLoader.load(targetClass).iterator();
      for (Iterator<ELEMENTS_MANAGE> iter = services; iter.hasNext(); ) {
        ELEMENTS_MANAGE valueInstance = iter.next();
        install(valueInstance);
      }
    }
  }


  void install(ELEMENTS_MANAGE compound) {
    Objects.requireNonNull(compound, "Compound is required, this method not allowed null values");
    compound.bind(moduleManager);
    components.add(compound);
  }

  @Override
  public FLOW_TYPE register(ELEMENTS_MANAGE additive) {
    install(additive);
    return flow();
  }

  @Override
  public FLOW_TYPE unregister(ELEMENTS_MANAGE additive) {
    components.remove(additive);
    return flow();
  }


  @Override
  public int countComponents() {
    return components.size();
  }

  public Set<ELEMENTS_MANAGE> getComponents() {
    return components;
  }


}
