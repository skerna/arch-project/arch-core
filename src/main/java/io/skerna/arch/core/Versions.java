package io.skerna.arch.core;

import com.vdurmont.semver4j.Semver;

public class Versions {
  public enum VERSION_TYPE {
    LESS,
    HIGHER,
    SAME,
    UNKNOW
  }
  /**
   * versionIsLessThan, verifica usando versionamiento semantico dadas dos versiones si
   * el primer argumento tiene una version inferior a otra
   * @param versionOrigin
   * @param versionRemote
   * @return
   */
  public static final VERSION_TYPE compareVersions(String versionOrigin, String versionRemote){

    try{


      Semver sem1 = new Semver(versionOrigin, Semver.SemverType.NPM); // Defaults to STRICT mode
      Semver sem2 = new Semver(versionRemote, Semver.SemverType.NPM); // Specify the mode

      Semver.VersionDiff compare = sem1.diff(sem2);

      if(compare == Semver.VersionDiff.MINOR){
        return VERSION_TYPE.LESS;
      }else if(compare == Semver.VersionDiff.MAJOR){
        return VERSION_TYPE.HIGHER;
      }else if(compare == Semver.VersionDiff.NONE){
        return VERSION_TYPE.SAME;
      }else if(compare == Semver.VersionDiff.PATCH){
        return VERSION_TYPE.HIGHER;
      }else{
        return VERSION_TYPE.SAME;
      }

//      Version v1 = Version.valueOf(versionOrigin);
//      Version v2 = Version.valueOf(versionRemote);
//      int resultAware = Version.BUILD_AWARE_ORDER.compare(v1, v2);  // < 0
//      if(resultAware < 1){
//        return VERSION_TYPE.LESS;
//      }else if(resultAware>=1){
//        return VERSION_TYPE.HIGHER;
//      }else {
//        return VERSION_TYPE.SAME;
//      }
    }catch (Exception ex){
      // no hagas nada, en cualquer caso retorna no
    }

    return VERSION_TYPE.UNKNOW;
  }
}
