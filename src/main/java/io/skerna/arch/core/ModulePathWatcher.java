package io.skerna.arch.core;


import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * This class runs a background task that checks directories for new, modified or removed files.
 * It's used for reloading mechanism.
 * *
 */
public class ModulePathWatcher implements Runnable {
  private static final Map<WatchEvent.Kind<Path>, Event> EVENT_MAP = new HashMap<WatchEvent.Kind<Path>, Event>() {{
    put(ENTRY_CREATE, Event.ENTRY_CREATE);
    put(ENTRY_MODIFY, Event.ENTRY_MODIFY);
    put(ENTRY_DELETE, Event.ENTRY_DELETE);
  }};
  protected final Logger log = LoggerFactory.logger(ModulePathWatcher.class);
  private final Set<Path> dirPaths;
  private Listener listener;
  private ExecutorService executorService;
  private Future<?> watcherTask;
  private WatchService watchService;
  private Map<WatchKey, Path> watchKeyToDirectory;
  private boolean running;

  private ModulePathWatcher(Set<Path> dirPaths, Listener listener) {
    this.dirPaths = dirPaths;
    this.listener = listener;
  }

  public static final Builder builder() {
    return new Builder();
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  public void start() {
    if (!running) {
      log.atDebug().log("Start moduleInstance listener path");
      running = true;
      watcherTask = executorService.submit(this);
    }
  }

  public void stop() {
    log.atDebug().log("Stop moduleInstance listener path");
    if (running) {
      running = false;
      executorService.shutdownNow();
      try {
        watchService.close();
      } catch (IOException e) {
        log.atError().log("Cannot close the watch service", e);
      }

      watcherTask.cancel(true);
      watcherTask = null;
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public void run() {
    log.atDebug().log("Total dirsPaths " + dirPaths.size());
    try {
      watchService = FileSystems.getDefault().newWatchService();
    } catch (IOException e) {
      throw new RuntimeException("Exception while creating watch service", e);
    }

    watchKeyToDirectory = new HashMap<>();

    for (Path dir : dirPaths) {
      try {
        log.atDebug().log("Watching dirs " + dir);
        // register the given directory, and all its sub-directories
        registerDirectory(dir);
      } catch (IOException e) {
        log.atError().log("Not watching, check path '{}'", dir, e);
      }
    }

    while (running) {
      if (Thread.interrupted()) {
        log.atInfo().log("Directory watcher thread interrupted");
        break;
      }

      WatchKey key;
      try {
        key = watchService.take();
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        continue;
      }

      Path dir = watchKeyToDirectory.get(key);
      if (dir == null) {
        log.atWarning().log("Watch key not recognized");
        continue;
      }

      // http://stackoverflow.com/a/25221600
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        // ignore ?!
      }

      for (WatchEvent<?> event : key.pollEvents()) {
        if (event.kind().equals(OVERFLOW)) {
          break;
        }

        WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
        WatchEvent.Kind<Path> kind = pathEvent.kind();

        Path path = dir.resolve(pathEvent.context());

        // if directory is created, and watching recursively, then register it and its sub-directories
        if (kind == ENTRY_CREATE) {
          try {
            if (Files.isDirectory(path, NOFOLLOW_LINKS)) {
              registerDirectory(dir);
            }
          } catch (IOException e) {
            // ignore
          }
        }

        if (running && EVENT_MAP.containsKey(kind)) {
          try {
            listener.onChange(EVENT_MAP.get(kind), dir, pathEvent.context());
          } catch (Exception ex) {
            log.atError().log("Event moduleInstance monitor atError().log", ex);
            ex.printStackTrace();
          }
        }
      }

      // reset key and remove from set if directory no longer accessible
      boolean valid = key.reset();
      if (!valid) {
        watchKeyToDirectory.remove(key);
//                log.atWarning().log("'{}' is inaccessible, stopping watch", dir);
        if (watchKeyToDirectory.isEmpty()) {
          break;
        }
      }
    }
  }

  /**
   * Register the given directory, and all its sub-directories.
   */
  private void registerDirectory(Path dir) throws IOException {
    Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {

      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        WatchKey key = dir.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        watchKeyToDirectory.put(key, dir);

        return FileVisitResult.CONTINUE;
      }

    });
  }

  public enum Event {
    ENTRY_CREATE,
    ENTRY_MODIFY,
    ENTRY_DELETE
  }

  public interface Listener {

    void onChange(Event event, Path dir, Path path);

  }

  public static class Builder {

    private Set<Path> dirPaths = new HashSet<>();
    private ExecutorService executorService;

    public Builder addDirectory(String dirPath) {
      return addDirectory(Paths.get(dirPath));
    }

    public Builder addDirectory(Path dirPath) {
      dirPaths.add(dirPath);

      return this;
    }

    public Builder setExecutorService(ExecutorService executorService) {
      this.executorService = executorService;

      return this;
    }

    public ModulePathWatcher build(Listener listener) {
      ModulePathWatcher watcher = new ModulePathWatcher(dirPaths, listener);
      if (executorService == null) {
        executorService = Executors.newSingleThreadExecutor();
      }
      watcher.executorService = executorService;

      return watcher;
    }

  }

}
