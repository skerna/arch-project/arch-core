package io.skerna.arch.core;

import io.skerna.arch.core.finders.ExtensionFinder;
import io.skerna.arch.core.finders.ModuleDescriptorFinder;
import io.skerna.arch.core.loaders.PluginLoaderManager;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.factories.ExtensionFactoryManager;
import io.skerna.arch.core.factories.ModuleFactoryManager;
import io.skerna.arch.core.repositories.PluginRepository;
import io.skerna.arch.core.vo.ModuleID;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public abstract class ModuleManagerReactiveBuilder<T extends ModuleManagerReactiveBuilder<?,?>,R extends ModuleManagerReactive> extends ModuleManagerBuilder<T,R> {
  protected ModulePathWatcher pluginMonitor;

  public ModuleManagerReactiveBuilder() {
  }

  public ModulePathWatcher getPluginMonitor() {
    return pluginMonitor;
  }


  @Override
  public T withExtensionFinder(ExtensionFinder extensionFinder) {
    return super.withExtensionFinder(extensionFinder);
  }

  @Override
  public T withPlugins(Map<ModuleID, ModuleWrapper> plugins) {
    return super.withPlugins(plugins);
  }

  @Override
  public T withExecutorService(Executor executorService) {
    return super.withExecutorService(executorService);
  }

  @Override
  public T withPluginsRoot(Path pluginsRoot) {
    return super.withPluginsRoot(pluginsRoot);
  }

  @Override
  public T withPluginDescriptorFinder(ModuleDescriptorFinder pluginDescriptorFinder) {
    return super.withPluginDescriptorFinder(pluginDescriptorFinder);
  }

  @Override
  public T withUnresolvedPlugins(List<ModuleWrapper> unresolvedPlugins) {
    return super.withUnresolvedPlugins(unresolvedPlugins);
  }

  @Override
  public T withResolvedPlugins(List<ModuleWrapper> resolvedPlugins) {
    return super.withResolvedPlugins(resolvedPlugins);
  }

  @Override
  public T withStartedPlugins(List<ModuleWrapper> startedPlugins) {
    return super.withStartedPlugins(startedPlugins);
  }

  @Override
  public T withPluginStateListeners(List<ModuleStateObserver> pluginStateListeners) {
    return super.withPluginStateListeners(pluginStateListeners);
  }

  @Override
  public T withRuntimeMode(RuntimeMode runtimeMode) {
    return super.withRuntimeMode(runtimeMode);
  }

  @Override
  public T withPluginRunner(ModuleRunner pluginRunner) {
    return super.withPluginRunner(pluginRunner);
  }

  @Override
  public T withSystemVersion(String systemVersion) {
    return super.withSystemVersion(systemVersion);
  }

  @Override
  public T withPluginRepository(PluginRepository pluginRepository) {
    return super.withPluginRepository(pluginRepository);
  }

  @Override
  public T withPluginFactory(ModuleFactoryManager pluginFactory) {
    return super.withPluginFactory(pluginFactory);
  }

  @Override
  public T withExtensionFactory(ExtensionFactoryManager extensionFactory) {
    return super.withExtensionFactory(extensionFactory);
  }

  @Override
  public T withPluginStatusProvider(ModuleStatusProvider pluginStatusProvider) {
    return super.withPluginStatusProvider(pluginStatusProvider);
  }

  @Override
  public T withDependencyResolver(DependencyGraph dependencyResolver) {
    return super.withDependencyResolver(dependencyResolver);
  }

  @Override
  public T withPluginLoaderManager(PluginLoaderManager pluginLoaderManager) {
    return super.withPluginLoaderManager(pluginLoaderManager);
  }

  @Override
  public T withExactVersionAllowed(boolean exactVersionAllowed) {
    return super.withExactVersionAllowed(exactVersionAllowed);
  }

  @Override
  public T withVersionManager(VersionManager versionManager) {
    return super.withVersionManager(versionManager);
  }

  /**
   * Sets the {@code pluginMonitor} and returns a reference to this Builder so that the methods can be chained together.
   *
   * @param pluginMonitor the {@code pluginMonitor} to set
   * @return a reference to this Builder
   */
  public T withPluginMonitor(ModulePathWatcher pluginMonitor) {
    this.pluginMonitor = pluginMonitor;
    return (T) this;
  }


}
