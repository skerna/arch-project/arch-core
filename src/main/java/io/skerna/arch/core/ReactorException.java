package io.skerna.arch.core;

import io.skerna.arch.core.util.StringUtils;

/**
 * An exception used to indicate that a moduleInstance problem occurred.
 * It's a generic moduleInstance exception class to be thrown when no more specific class is applicable.
 * <p>
 * *
 */
public class ReactorException extends Exception {

  public ReactorException() {
    super();
  }

  public ReactorException(String message) {
    super(message);
  }

  public ReactorException(Throwable cause) {
    super(cause);
  }

  public ReactorException(String message, Throwable cause) {
    super(message, cause);
  }

  public ReactorException(Throwable cause, String message, Object... args) {
    super(StringUtils.format(message, args), cause);
  }

  public ReactorException(String message, Object... args) {
    super(StringUtils.format(message, args));
  }

}
