package io.skerna.arch.core;

import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.ModuleRunnerID;

import java.nio.file.Path;
import java.util.List;

/**
 * A plugin descriptor contains information about a plug-in.
 * <p>
 */
public interface ModuleDescriptor {

  ModuleID getModuleID();

  String getDescription();

  String getModuleClass();

  String getVersion();

  String getRequires();

  String getProvider();

  String getLicense();

  List<ModuleDependecy> getDependencies();

  Path source();

  ModuleFactoryID getModuleFactoryID();

  ModuleRunnerID getModuleRunnerID();

  ModuleType getModuleType();

}
