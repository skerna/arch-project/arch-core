package io.skerna.arch.core;

import io.skerna.arch.core.vo.ModuleID;


public interface ModuleStatusProvider {

  /**
   * Checks if the moduleInstance is disabled or not
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return if the moduleInstance is disabled or not
   */
  boolean isPluginDisabled(ModuleID moduleId);

  /**
   * Disables a moduleInstance from being loaded.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if moduleInstance is disabled
   */
  boolean disablePlugin(ModuleID moduleId);

  /**
   * Enables a moduleInstance that has previously been disabled.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if moduleInstance is enabled
   */
  boolean enablePlugin(ModuleID moduleId);

}
