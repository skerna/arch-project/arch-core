package io.skerna.arch.core;

import io.skerna.arch.core.factories.FactoryException;
import io.skerna.arch.core.spi.ModuleFactory;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.arch.core.vo.SourceModuleType;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.Optional;

/**
 * A wrapper over moduleInstance instance.
 * <p>
 * *
 */
public class ModuleWrapper {
  protected final Logger log = LoggerFactory.logger(ModuleWrapper.class);
  Module moduleInstance; // cache
  private ModuleManager moduleManager;
  private ModuleDescriptor descriptor;
  private SourceModule sourceModule;
  private ClassLoader moduleClassLoader;
  private ModuleFactory moduleFactory;
  private ModuleState moduleState;
  private RuntimeMode runtimeMode;
  private Class<?> moduleClass;
  private SourceModuleType moduleOrigen;

  public ModuleWrapper(ModuleManager moduleManager,
                       ModuleDescriptor descriptor,
                       SourceModule sourceModule,
                       ClassLoader moduleClassLoader,
                       ModuleFactory moduleFactory,
                       RuntimeMode runtimeMode,
                       SourceModuleType moduleOrigen) {
    this.moduleManager = moduleManager;
    this.descriptor = descriptor;
    this.sourceModule = sourceModule;
    this.moduleClassLoader = moduleClassLoader;
    this.moduleState = ModuleState.CREATED;
    this.moduleFactory = moduleFactory;
    this.runtimeMode = runtimeMode;
    this.moduleOrigen = moduleOrigen;

  }



  /**
   * Returns the moduleInstance manager.
   */
  public ModuleManager getModuleManager() {
    return moduleManager;
  }

  /**
   * Returns the moduleInstance descriptor.
   */
  public ModuleDescriptor getDescriptor() {
    return descriptor;
  }

  /**
   * Returns the path of this moduleInstance.
   */
  public SourceModule getSourceModule() {
    return sourceModule;
  }

  /**
   * Returns the moduleInstance class loader used to load classes and resources
   * for this plug-in. The class loader can be used to directly access
   * plug-in resources and classes.
   */
  public ClassLoader getPluginClassLoader() {
    return moduleClassLoader;
  }

  /**
   * Check if moduleInstance use external classloader
   *
   * @return
   */
  private boolean isExternalClassloader() {
    return getPluginClassLoader().getClass() != ModuleClassLoader.class;
  }

  public synchronized Module getModuleInstance() {
    if (moduleInstance == null) {
      log.atDebug().log("Creating moduleInstance using factory {}", moduleFactory);
      try {
        moduleInstance = moduleFactory.createModule(this);
      } catch (Exception e) {
        log.atError().log("imposible build moduleInstance using factory ", e);
        e.printStackTrace();
      }
    }
    if (moduleInstance == null) {
      throw new FactoryException();
    }
    return moduleInstance;
  }

  public synchronized Module getNewModuleInstance() {
    log.atDebug().log("Creating moduleInstance using factory {}", moduleFactory);
    try {
      moduleInstance = moduleFactory.createModule(this);
    } catch (Exception e) {
      log.atError().log("imposible build moduleInstance using factory ", e);
      e.printStackTrace();
    }
    return moduleInstance;
  }

  public synchronized Optional<Class<?>> getModuleClass() {
    String className = descriptor.getModuleClass();

    if (moduleClass == null) {
      log.atDebug().log("Load class {} from classloader and save in cache wrapper", className);
      try {
        moduleClass = moduleClassLoader.loadClass(className);
      } catch (ClassNotFoundException e) {
        log.atError().log(e.getMessage(), e);
        throw new IllegalStateException("Imposible load classname " + className, e);
      }
    } else {
      log.atDebug().log("Return cache class {} loaded from ClassLoader", className);
    }
    return Optional.ofNullable(moduleClass);
  }

  public ModuleState getModuleState() {
    return moduleState;
  }

  void setModuleState(ModuleState moduleState) {
    this.moduleState = moduleState;
  }

  public RuntimeMode getRuntimeMode() {
    return runtimeMode;
  }

  void setRuntimeMode(RuntimeMode runtimeMode) {
    this.runtimeMode = runtimeMode;
  }

  /**
   * Shortcut
   */
  public ModuleID getPluginId() {
    return getDescriptor().getModuleID();
  }

  public SourceModuleType getModuleOrigen() {
    return moduleOrigen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + descriptor.getModuleID().hashCode();

    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    ModuleWrapper other = (ModuleWrapper) obj;
    if (!descriptor.getModuleID().equals(other.descriptor.getModuleID())) {
      return false;
    }

    return true;
  }

  @Override
  public String toString() {
    return "PluginWrapper [descriptor=" + descriptor + ", modulePath=" + sourceModule + "]";
  }

  void setModuleFactory(ModuleFactory moduleFactory) {
    this.moduleFactory = moduleFactory;
  }

  public ModuleFactory getModuleFactory() {
    return moduleFactory;
  }
  }
