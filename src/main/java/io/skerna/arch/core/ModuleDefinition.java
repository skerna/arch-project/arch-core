package io.skerna.arch.core;

import org.atteo.classindex.IndexAnnotated;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(TYPE)
@Inherited
@Documented
@IndexAnnotated
public @interface ModuleDefinition {

  String moduleId() default "";

  String pluginDescription() default "";

  String moduleClass() default "";

  String version() default "latest";

  String requires() default "";

  String provider() default "";

  String license() default "";

  Dependency[] dependecies() default {};

  Class prefferredFactory() default void.class;

  ModuleType moduleType() default ModuleType.STANDARD;

}
