package io.skerna.arch.core;

/**
 * @author Ronald Cárdenas
 * {@link Flow} define common method agregate pattern or composite pattern
 * EXPANSIVE_TYPE is type of elements acceptables
 * RETURN_TYPE return TYPE then INVOKE MOETHOD flow api
 **/
public interface Flow<EXPANSIVE_TYPE, FLOW_TYPE> {
  /**
   * Register new addive
   *
   * @param additive
   */
  FLOW_TYPE register(EXPANSIVE_TYPE additive);

  /**
   * Remove existent adive
   *
   * @param additive
   */
  FLOW_TYPE unregister(EXPANSIVE_TYPE additive);

  /**
   * Return context flow TYPE
   *
   * @return
   */
  FLOW_TYPE flow();

  /**
   * return size factories registered
   *
   * @return int
   */
  int countComponents();

}
