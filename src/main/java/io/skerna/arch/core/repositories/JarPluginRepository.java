package io.skerna.arch.core.repositories;

import io.skerna.arch.core.util.JarFileFilter;

import java.nio.file.Path;

public class JarPluginRepository extends BasePluginRepository {

  public JarPluginRepository(Path pluginsRoot) {
    super(pluginsRoot, new JarFileFilter());
  }

}
