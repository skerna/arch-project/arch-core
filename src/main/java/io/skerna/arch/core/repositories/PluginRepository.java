package io.skerna.arch.core.repositories;

import io.skerna.arch.core.vo.SourceModule;

import java.util.List;

public interface PluginRepository {

  /**
   * List all plugin paths.
   *
   * @return a list of files
   */
  List<SourceModule> getPluginPaths();

  /**
   * Removes a plugin from the repository.
   *
   * @param sourceModule the plugin path
   * @return true if deleted
   */
  boolean deletePluginPath(SourceModule sourceModule);

}
