package io.skerna.arch.core.repositories;

import io.skerna.arch.core.vo.SourceFileModule;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.arch.core.util.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class DefaultPluginRepository extends BasePluginRepository {
  private static final Logger log = LoggerFactory.logger(DefaultPluginRepository.class);

  public DefaultPluginRepository(Path pluginsRoot, boolean development) {
    super(pluginsRoot);

    AndFileFilter pluginsFilter = new AndFileFilter(new DirectoryFileFilter());
    pluginsFilter.addFileFilter(new NotFileFilter(createHiddenPluginFilter(development)));
    setFilter(pluginsFilter);
  }

  @Override
  public List<SourceModule> getPluginPaths() {
    // expand plugins zip files
    File[] pluginZips = pluginsRoot.toFile().listFiles(new ZipFileFilter());
    if ((pluginZips != null) && pluginZips.length > 0) {
      for (File pluginZip : pluginZips) {
        try {
          FileUtils.expandIfZip(pluginZip.toPath());
        } catch (IOException e) {
          log.atError().log("Cannot expand plugin zip '{}'", pluginZip);
          log.atError().log(e.getMessage(), e);
        }
      }
    }

    return super.getPluginPaths();
  }

  @Override
  public boolean deletePluginPath(SourceModule sourceModule) {
    if(!ClassUtils.isOfType(sourceModule, SourceFileModule.class)){
      return false;
    }
    SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
    FileUtils.optimisticDelete(FileUtils.findWithEnding(sourceFileModule.file, ".zip", ".ZIP", ".Zip"));
    return super.deletePluginPath(sourceFileModule);
  }

  protected FileFilter createHiddenPluginFilter(boolean development) {
    OrFileFilter hiddenPluginFilter = new OrFileFilter(new HiddenFilter());

    if (development) {
      // skip default build output folders since these will cause errors in the logs
      hiddenPluginFilter
        .addFileFilter(new NameFileFilter("target")) // MAVEN
        .addFileFilter(new NameFileFilter("build")); // GRADLE
    }

    return hiddenPluginFilter;
  }
}
