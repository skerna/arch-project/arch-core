package io.skerna.arch.core.repositories;

import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.SourceModule;
import io.skerna.arch.core.vo.SourceFileModule;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.*;

public class BasePluginRepository implements PluginRepository {

  protected final Path pluginsRoot;

  protected FileFilter filter;
  protected Comparator<File> comparator;

  public BasePluginRepository(Path pluginsRoot) {
    this(pluginsRoot, null);
  }

  public BasePluginRepository(Path pluginsRoot, FileFilter filter) {
    this.pluginsRoot = pluginsRoot;
    this.filter = filter;

    // last modified file is first
    this.comparator = new Comparator<File>() {

      @Override
      public int compare(File o1, File o2) {
        return (int) (o2.lastModified() - o1.lastModified());
      }

    };
  }

  public void setFilter(FileFilter filter) {
    this.filter = filter;
  }

  /**
   * Set a {@link File} {@link Comparator} used to sort the listed files from {@code pluginsRoot}.
   * This comparator is used in {@link #getPluginPaths()} method.
   * By default is used a file comparator that returns the last modified files first.
   * If you don't want a file comparator, then call this method with {@code null}.
   */
  public void setComparator(Comparator<File> comparator) {
    this.comparator = comparator;
  }

  @Override
  public List<SourceModule> getPluginPaths() {
    File[] files = pluginsRoot.toFile().listFiles(filter);

    if ((files == null) || files.length == 0) {
      return Collections.emptyList();
    }

    if (comparator != null) {
      Arrays.sort(files, comparator);
    }

    List<SourceModule> paths = new ArrayList<>(files.length);
    for (File file : files) {
      paths.add(new SourceFileModule(file.toPath()));
    }

    return paths;
  }

  @Override
  public boolean deletePluginPath(SourceModule sourceModule) {
    if(!ClassUtils.isOfType(sourceModule,SourceFileModule.class)){
      return false;
    }
    try {
      SourceFileModule sourceFileModule = (SourceFileModule) sourceModule;
      FileUtils.delete(sourceFileModule.file);
      return true;
    } catch (NoSuchFileException nsf) {
      return false; // Return false on not found to be compatible with previous API
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
