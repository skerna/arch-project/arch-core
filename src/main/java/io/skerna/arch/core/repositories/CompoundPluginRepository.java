package io.skerna.arch.core.repositories;

import io.skerna.arch.core.vo.SourceModule;

import java.util.ArrayList;
import java.util.List;

public class CompoundPluginRepository implements PluginRepository {

  private List<PluginRepository> repositories = new ArrayList<>();

  public CompoundPluginRepository add(PluginRepository repository) {
    if (repository == null) {
      throw new IllegalArgumentException("null not allowed");
    }

    repositories.add(repository);

    return this;
  }

  @Override
  public List<SourceModule> getPluginPaths() {
    List<SourceModule> sourceModuleList = new ArrayList<>();
    for (PluginRepository repository : repositories) {
      sourceModuleList.addAll(repository.getPluginPaths());
    }

    return sourceModuleList;
  }

  @Override
  public boolean deletePluginPath(SourceModule sourceModule) {
    for (PluginRepository repository : repositories) {
      if (repository.deletePluginPath(sourceModule)) {
        return true;
      }
    }

    return false;
  }

}
