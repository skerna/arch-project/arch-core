package io.skerna.arch.core.repositories;

public interface RepoEventListener<T extends RepoEventSource> {

  /**
   * onNewEvent, onNewEvent
   * @param moduleSource
   */
  void onNewEvent(T moduleSource);

  boolean isApplicable(T moduleSource);
}
