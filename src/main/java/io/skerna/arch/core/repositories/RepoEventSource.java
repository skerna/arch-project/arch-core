package io.skerna.arch.core.repositories;

public class RepoEventSource {
  public final TYPE type;

  public RepoEventSource(TYPE type) {
    this.type = type;
  }


  enum TYPE{
    MODULE_PATH_EVENT,
    CLASSPATH_EVENT,
    INSTANCE_REFERECE_EVENT
  }

}
