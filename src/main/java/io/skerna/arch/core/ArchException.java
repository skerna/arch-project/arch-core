package io.skerna.arch.core;

import io.skerna.arch.core.exceptions.ResourceMessageCatalog;
import io.skerna.arch.core.exceptions.StandardRuntimeException;
import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.ModuleRunnerID;
import io.skerna.arch.core.exceptions.Render;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;

/**
 * <h1>ArchException!</h1>
 * ArchException, is base class exceptions
 * <p>
 * <b>Note:</b>
 * </p>
 * @see ModuleFactoryID
 * @see ModuleRunnerID
 *
 */
public class ArchException extends StandardRuntimeException {


  public ArchException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode) {
    super(errorCode);
  }

  public ArchException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode, @Nullable String message) {
    super(errorCode, message);
  }

  public ArchException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode, @Nullable String message, @Nullable Throwable cause) {
    super(errorCode, message, cause);
  }

  public ArchException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode, @Nullable Throwable cause) {
    super(errorCode, cause);
  }

  public ArchException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode, @Nullable String message, @Nullable Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(errorCode, message, cause, enableSuppression, writableStackTrace);
  }


  @NotNull
  @Override
  public Render getRender() {
    return Render.create(new ResourceMessageCatalog("io.skerna.arch.core.messages.i18nexceptions"));
  }
}
