package io.skerna.arch.core;

import io.skerna.arch.core.vo.ModuleID;

public class ModuleDependecy {

  private ModuleID moduleId;
  private String pluginVersionSupport = "*";
  private boolean optional;

  public ModuleDependecy(ModuleID dependency) {
    int index = dependency.value().indexOf('@');
    if (index == -1) {
      this.moduleId = dependency;
    } else {
      this.moduleId = ModuleID.of(dependency.value().substring(0, index));
      if (dependency.value().length() > index + 1) {
        this.pluginVersionSupport = dependency.value().substring(index + 1);
      }
    }

    // A dependency is considered as optional, if the moduleInstance id ends with a question mark.
    this.optional = this.moduleId.value().endsWith("?");
    if (this.optional) {
      this.moduleId = ModuleID.of(this.moduleId.value().substring(0, this.moduleId.value().length() - 1));
    }
  }

  public ModuleID getPluginId() {
    return moduleId;
  }

  public String getPluginVersionSupport() {
    return pluginVersionSupport;
  }

  public boolean isOptional() {
    return optional;
  }

  @Override
  public String toString() {
    return "PluginDependency [moduleId=" + moduleId + ", pluginVersionSupport="
      + pluginVersionSupport + ", optional="
      + optional + "]";
  }

}
