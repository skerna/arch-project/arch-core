package io.skerna.arch.core;

import io.skerna.arch.core.spi.ModuleRunner;
import org.atteo.classindex.IndexAnnotated;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.TYPE;

@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
@Inherited
@Documented
@IndexAnnotated
public @interface RunWith {
  Class<? extends ModuleRunner> value();
}
