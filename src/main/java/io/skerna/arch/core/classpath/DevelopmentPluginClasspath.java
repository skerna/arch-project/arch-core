package io.skerna.arch.core.classpath;

import io.skerna.arch.core.RuntimeMode;

/**
 * It's a compound {@link PluginClasspath} ({@link #MAVEN} + {@link #GRADLE} + {@link #KOTLIN})
 * used in development mode ({@link RuntimeMode#DEVELOPMENT}).
 * <p>
 */
public class DevelopmentPluginClasspath extends PluginClasspath {

  /**
   * The development plugin classpath for <a href="https://maven.apache.org">Maven</a>.
   * The classes directory is {@code target/classes} and the lib directory is {@code target/lib}.
   */
  public static final PluginClasspath MAVEN = new PluginClasspath().addClassesDirectories("target/classes").addLibDirectories("target/lib");

  /**
   * The development plugin classpath for <a href="https://gradle.org">Gradle</a>.
   * The classes directories are {@code build/classes/java/main, build/resources/main}.
   */
  public static final PluginClasspath GRADLE = new PluginClasspath().addClassesDirectories("build/classes/java/main", "build/resources/main");

  /**
   * The development plugin classpath for <a href="https://kotlinlang.org">Kotlin</a>.
   * The classes directories are {@code build/classes/kotlin/main", build/resources/main, build/tmp/kapt3/classes/main}.
   */
  public static final PluginClasspath KOTLIN = new PluginClasspath().addClassesDirectories("build/classes/kotlin/main", "build/resources/main", "build/tmp/kapt3/classes/main");

  public DevelopmentPluginClasspath() {
    addClassesDirectories(MAVEN.getClassesDirectories());
    addClassesDirectories(GRADLE.getClassesDirectories());
    addClassesDirectories(KOTLIN.getClassesDirectories());

    addLibDirectories(MAVEN.getLibDirectories());
    addLibDirectories(GRADLE.getLibDirectories());
    addLibDirectories(KOTLIN.getLibDirectories());
  }

}
