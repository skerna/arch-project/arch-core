package io.skerna.arch.core.classpath;

/**
 * The default values are {@code classes} and {@code lib}.
 * <p>
 */
public class DefaultPluginClasspath extends PluginClasspath {

  public DefaultPluginClasspath() {
    super();

    addClassesDirectories("classes");
    addLibDirectories("lib");
  }

}
