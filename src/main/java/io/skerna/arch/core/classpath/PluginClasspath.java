package io.skerna.arch.core.classpath;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The classpath of the plugin.
 * It contains {@code classes} directories and {@code lib} directories (directories that contains jars).
 * <p>
 * *
 */
public class PluginClasspath {

  private Set<String> classesDirectories = new HashSet<>();
  private Set<String> libDirectories = new HashSet<>();

  public Set<String> getClassesDirectories() {
    return classesDirectories;
  }

  public PluginClasspath addClassesDirectories(String... classesDirectories) {
    return addClassesDirectories(Arrays.asList(classesDirectories));
  }

  public PluginClasspath addClassesDirectories(Collection<String> classesDirectories) {
    this.classesDirectories.addAll(classesDirectories);

    return this;
  }

  public Set<String> getLibDirectories() {
    return libDirectories;
  }

  public PluginClasspath addLibDirectories(String... libDirectories) {
    return addLibDirectories(Arrays.asList(libDirectories));
  }

  public PluginClasspath addLibDirectories(Collection<String> libDirectories) {
    this.libDirectories.addAll(libDirectories);

    return this;
  }

}
