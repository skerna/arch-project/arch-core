package io.skerna.arch.core;

import io.skerna.arch.core.finders.ExtensionFinder;
import io.skerna.arch.core.factories.ExtensionFactoryManager;
import io.skerna.arch.core.factories.ModuleFactoryManager;
import io.skerna.arch.core.finders.ModuleDescriptorFinder;
import io.skerna.arch.core.loaders.PluginLoaderManager;
import io.skerna.arch.core.repositories.PluginRepository;
import io.skerna.arch.core.spi.ModuleFactory;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.util.StringUtils;
import io.skerna.arch.core.vo.*;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;
import org.atteo.classindex.ClassIndex;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;


/**
 * This class implements the boilerplate moduleInstance code that any {@link ModuleManager}
 * implementation would have to support.
 * It helps cut the noise out of the subclass that handles moduleInstance management.
 *
 * <p>This class is not thread-safe.
 * <p>
 */
public abstract class AbstractModuleManager implements ModuleManager {
  private static final long MAX_AWAIT_COMPLETABLES = 1000;

  private static final Logger log = LoggerFactory.logger(AbstractModuleManager.class);
  protected ExtensionFinder extensionFinder;
  /**
   * A map of plugins this manager is responsible for (the key is the 'moduleId').
   */
  protected Map<ModuleID, ModuleWrapper> plugins;
  Executor executorService;
  private Path pluginsRoot;
  private ModuleDescriptorFinder pluginDescriptorFinder;
  /**
   * A list with unresolved plugins (unresolved dependency).
   */
  private List<ModuleWrapper> unresolvedPlugins;
  /**
   * A list with all resolved plugins (resolved dependency).
   */
  private List<ModuleWrapper> resolvedPlugins;
  /**
   * A list with started plugins.
   */
  private List<ModuleWrapper> startedPlugins;
  /**
   * The registered {@link ModuleStateObserver}s.
   */
  private List<ModuleStateObserver> pluginStateListeners;
  /**
   * Cache value for the runtime mode.
   * No need to re-read it because it wont change at runtime.
   */
  private RuntimeMode runtimeMode;
  private ModuleRunner pluginRunner;
  /**
   * The system version used for comparisons to the moduleInstance requires attribute.
   */
  private String systemVersion = "0.0.0";
  private PluginRepository pluginRepository;
  private ModuleFactoryManager pluginFactory;
  private ExtensionFactoryManager extensionFactory;
  private ModuleStatusProvider pluginStatusProvider;
  private DependencyGraph dependencyResolver;
  private PluginLoaderManager pluginLoaderManager;
  private boolean exactVersionAllowed = false;
  private VersionManager versionManager;


  protected AbstractModuleManager(ModuleManagerBuilder<?,?> builder) {
    this.extensionFinder = builder.extensionFinder;
    this.plugins = builder.plugins;
    this.executorService = builder.executorService;
    this.pluginsRoot = builder.pluginsRoot;
    this.pluginDescriptorFinder = builder.pluginDescriptorFinder;
    this.unresolvedPlugins = builder.unresolvedPlugins;
    this.resolvedPlugins = builder.resolvedPlugins;
    this.startedPlugins = builder.startedPlugins;
    this.pluginStateListeners = builder.pluginStateListeners;
    this.runtimeMode = builder.runtimeMode;
    this.pluginRunner = builder.pluginRunner;
    this.systemVersion = builder.systemVersion;
    this.pluginRepository = builder.pluginRepository;
    this.pluginFactory = builder.pluginFactory;
    this.extensionFactory = builder.extensionFactory;
    this.pluginStatusProvider = builder.pluginStatusProvider;
    this.dependencyResolver = builder.dependencyResolver;
    this.pluginLoaderManager = builder.pluginLoaderManager;
    this.exactVersionAllowed = builder.exactVersionAllowed;
    this.versionManager = builder.versionManager;
  }

  protected void initialize() {
    plugins = new HashMap<>();
    unresolvedPlugins = new ArrayList<>();
    resolvedPlugins = new ArrayList<>();
    startedPlugins = new ArrayList<>();
    pluginStateListeners = new ArrayList<>();

    if (pluginsRoot == null) {
      pluginsRoot = createPluginsRoot();
      if (!pluginsRoot.toFile().exists()) {
        log.atDebug().log("Folder to log Not exist , create new");
        pluginsRoot.toFile().mkdirs();
      }
    }
    if(pluginRepository == null){
      log.atTrace().log("Using deault pluginRepository");
      pluginRepository = createPluginRepository();
    }
    if(pluginFactory == null){
      log.atTrace().log("Using deault pluginFactory");
      pluginFactory = createPluginFactory();
    }
    if(extensionFactory == null){
      log.atTrace().log("Using deault extensionFactory");
      extensionFactory = createExtensionFactory();
    }
    if(pluginDescriptorFinder == null){
      log.atTrace().log("Using deault pluginDescriptorFinder");
      pluginDescriptorFinder = createPluginDescriptorFinder();
    }
    if(extensionFinder == null){
      extensionFinder = createExtensionFinder();
    }
    if(pluginStatusProvider == null){
      log.atTrace().log("Using deault pluginStatusProvider");
      pluginStatusProvider = createPluginStatusProvider();
    }
    if(pluginLoaderManager== null){
      log.atTrace().log("Using deault pluginLoaderManager");
      pluginLoaderManager = createPluginLoaderManager();
    }
    if(versionManager == null){
      log.atTrace().log("Using deault versionManager");
      versionManager = createVersionManager();
    }
    if(pluginRunner==null){
      log.atTrace().log("Using deault pluginRunner");
      pluginRunner = createPluginRuner();
    }
    if(dependencyResolver== null){
      log.atTrace().log("Using deault dependencyResolver");
      dependencyResolver = new DependencyGraph(versionManager);
    }
    if(executorService == null){
      log.atTrace().log("Using deault executorService");
      executorService = createExecutorService();
    }
    if(systemVersion == null){
      this.systemVersion= "0.0.0";
    }
    this.pluginFactory.bindManager(this);
    this.pluginLoaderManager.bindManager(this);
    this.pluginRunner.bindManager(this);
  }
//  protected void initialize() {
//    plugins = new HashMap<>();
//    unresolvedPlugins = new ArrayList<>();
//    resolvedPlugins = new ArrayList<>();
//    startedPlugins = new ArrayList<>();
//
//    pluginStateListeners = new ArrayList<>();
//
//    if (pluginsRoot == null) {
//      pluginsRoot = createPluginsRoot();
//      if (!pluginsRoot.toFile().exists()) {
//        log.atDebug().log("Folder to log Not exist , create new");
//        pluginsRoot.toFile().mkdirs();
//      }
//    }
//
//    pluginRepository = createPluginRepository();
//    pluginFactory = createPluginFactory();
//    extensionFactory = createExtensionFactory();
//    pluginDescriptorFinder = createPluginDescriptorFinder();
//    extensionFinder = createExtensionFinder();
//    pluginStatusProvider = createPluginStatusProvider();
//    pluginLoaderManager = createPluginLoaderManager();
//    versionManager = createVersionManager();
//    pluginRunner = createPluginRuner();
//    dependencyResolver = new DependencyGraph(versionManager);
//    this.executorService = createExecutorService();
//  }

  @Override
  public String getSystemVersion() {
    return systemVersion;
  }

  @Override
  public void setSystemVersion(String version) {
    systemVersion = version;
  }

  /**
   * Returns a copy of plugins.
   */
  @Override
  public List<ModuleWrapper> getModules() {
    return new ArrayList<>(plugins.values());
  }

  /**
   * Returns a copy of plugins with that state.
   */
  @Override
  public List<ModuleWrapper> getModules(ModuleState pluginState) {
    List<ModuleWrapper> plugins = new ArrayList<>();
    for (ModuleWrapper plugin : getModules()) {
      if (pluginState.equals(plugin.getModuleState())) {
        plugins.add(plugin);
      }
    }

    return plugins;
  }

  @Override
  public List<ModuleWrapper> getResolvedModules() {
    return resolvedPlugins;
  }

  @Override
  public List<ModuleWrapper> getUnresolvedModules() {
    return unresolvedPlugins;
  }

  @Override
  public List<ModuleWrapper> getStartedModules() {
    return startedPlugins;
  }

  @Override
  public ModuleWrapper getModule(ModuleID moduleId) {
    return plugins.get(moduleId);
  }

  @Override
  public Set<ModuleID> loadModules(SourceModule sourceModule) {

    if(sourceModule == null){
      throw new IllegalArgumentException(String.format("task:[LOAD_PLUGIN] Specified moduleInstance %s does not exist!", sourceModule));
    }
//    if ((pluginPath == null) || Files.notExists(pluginPath)) {
  //  }

    log.atDebug().log("task:[LOAD_PLUGIN] Loading moduleInstance from '{}'", sourceModule);

    try {
      Set<ModuleWrapper> pluginWrapper = loadPluginsFromPath(sourceModule);

      // try to resolve  the loaded moduleInstance together with other possible plugins that depend on this moduleInstance
      resolvePlugins();

      return pluginWrapper.stream()
        .map(ModuleWrapper::getPluginId)
        .collect(Collectors.toSet());
    } catch (ReactorException e) {
      log.atError().log(e.getMessage(), e);
    }

    return null;
  }

  /**
   * Load plugins.
   */
  @Override
  public void loadModules() {
    log.atDebug().log("task:[LOAD_PLUGINS], Lookup plugins in '{}'", pluginsRoot);
    // check for plugins root
    if (Files.notExists(pluginsRoot) || !Files.isDirectory(pluginsRoot)) {
      log.atWarning().log("task:[LOAD_PLUGINS] '{}' root", pluginsRoot);
      return;
    }

    // get all moduleInstance paths from repository
    List<SourceModule> sourceModuleList = pluginRepository.getPluginPaths();

    // check for no plugins
    if (sourceModuleList.isEmpty()) {
      log.atWarning().log("task:[LOAD_PLUGINS] No plugins find on Directory {}", sourceModuleList);
      // return;
    }

    log.atDebug().log("task:[LOAD_PLUGINS] Found {} possible plugins: {}", sourceModuleList.size(), sourceModuleList);
    // TODO(FIXME RONALD este es un error grave que presenta conflicto al cargar los modlos con classgraph)
    try {
      loadPluginsFromClasspath();
    } catch (ReactorException | URISyntaxException e) {
      e.printStackTrace();
    }

    // load plugins from moduleInstance paths
    for (SourceModule sourceModule : sourceModuleList) {
      try {
        loadPluginsFromPath(sourceModule);
      } catch (ReactorException e) {
        log.atError().log(e.getMessage(), e);
      }
    }

    // resolve plugins
    try {
      resolvePlugins();
    } catch (ReactorException e) {
      log.atError().log(e.getMessage(), e);
    }
  }

  /**
   * Unload the specified moduleInstance and it's dependents.
   *
   * @param moduleId
   */
  @Override
  public CompletableFuture<Boolean> unloadModule(ModuleID moduleId) {
    return CompletableFuture.supplyAsync(() -> unloadPlugin(moduleId, true));
  }

  private boolean unloadPlugin(ModuleID moduleId, boolean unloadDependents) {
    try {
      if (unloadDependents) {
        List<ModuleID> dependents = dependencyResolver.getDependents(moduleId);
        while (!dependents.isEmpty()) {
          ModuleID dependent = dependents.remove(0);
          unloadPlugin(dependent, false);
          dependents.addAll(0, dependencyResolver.getDependents(dependent));
        }
      }

      ModuleState pluginState = stopPlugin(moduleId, false);
      if (ModuleState.STARTED == pluginState) {
        return false;
      }

      ModuleWrapper pluginWrapper = getModule(moduleId);
      log.atDebug().log("task:[UNLOAD_PLUGIN] moduleInstance '{}'", getPluginLabel(pluginWrapper.getDescriptor()));

      // remove the moduleInstance
      plugins.remove(moduleId);
      getResolvedModules().remove(pluginWrapper);
      firePluginStateEvent(new ModuleStateEvent(this, pluginWrapper, pluginState));
      pluginLoaderManager.removeClassLoader(moduleId);
      return true;
    } catch (IllegalArgumentException e) {
      // ignore not found exceptions because this  method is recursive
      log.atError().log(e.getMessage(), e);
    }

    return false;
  }

  @Override
  public CompletableFuture<Boolean> deleteModule(ModuleID moduleId) {
    return CompletableFuture.supplyAsync(() -> {
      checkPluginId(moduleId);
      ModuleWrapper pluginWrapper = getModule(moduleId);
      ChangeStateResult changeStateResult = stopModule(moduleId).join();
      if (ModuleState.STARTED == changeStateResult.getPluginState()) {
        log.atError().log("task:[DELETE_PLUGIN] {} Failed to stop moduleInstance '{}' on delete", moduleId);
        return false;
      }

      CompletableFuture<Boolean> unloadPluginResult = unloadModule(moduleId);
      /**
       * Blocking Get unload result
       */
      try {
        Boolean unloadPlugin = unloadPluginResult.get();
        if (!unloadPlugin) {
          log.atError().log("task:[DELETE_PLUGIN] {} ,Failed to unload moduleInstance '{}' on delete", moduleId);
          return false;
        }
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }

      try {
        pluginWrapper.getModuleInstance().delete();
      } catch (ReactorException e) {
        log.atError().log(e.getMessage(), e);
        return false;
      }

      SourceModule sourceModule = pluginWrapper.getSourceModule();
      getPluginFactory().umountFactory(pluginWrapper);
      return pluginRepository.deletePluginPath(sourceModule);
    }, executorService);
  }

  @Override
  public CompletableFuture<Boolean> deleteModule(SourceModule sourceModule) {
    Set<ModuleID> moduleID = idForPath(sourceModule);
    for (ModuleID id : moduleID) {
      deleteModule(id);
    }
    return CompletableFuture.completedFuture(true);
  }

  /**
   * Start all active plugins.
   */
  @Override
  public CompletableFuture<Void> startModules() {
    log.atDebug().log("task:[START_PLUGINS] start all plugins ");

    List<ModuleWrapper> configurationModules = resolvedPlugins.stream()
      .filter((e) -> e.getDescriptor().getModuleType() == ModuleType.CONFIGURATION)
      .collect(Collectors.toList());

    List<ModuleWrapper> standardModules = resolvedPlugins.stream()
      .filter((e) -> e.getDescriptor().getModuleType() == ModuleType.STANDARD)
      .collect(Collectors.toList());

    return CompletableFuture
      .supplyAsync(() -> startModules(configurationModules), executorService)
      .thenRunAsync(() -> startModules(standardModules), executorService);
  }

  public CompletableFuture<Void> startModules(List<ModuleWrapper> modules) {
    log.atDebug().log("task:[START_PLUGINS] start block {}", modules.size());
    List<CompletableFuture<ChangeStateResult>> futures = modules.stream()
      .map((moduleWrapper -> startModule(moduleWrapper.getPluginId())))
      .collect(Collectors.toList());

    return CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
/**
 return CompletableFuture.supplyAsync(() -> {
 for (ModuleWrapper pluginWrapper : modules) {
 ModuleState pluginState = pluginWrapper.getModuleState();
 if ((ModuleState.DISABLED != pluginState) && (ModuleState.STARTED != pluginState)) {
 try {
 log.atDebug().log("task:[START_PLUGINS] Start moduleInstance '{}'", getPluginLabel(pluginWrapper.getDescriptor()));
 CompletableFuture<Boolean>result = pluginRunner.startModule(pluginWrapper);
 if(result.isCompletedExceptionally()){
 log.atError().log("task:[START_PLUGINS] Failed start moduleInstance {}", getPluginLabel(pluginWrapper.getDescriptor()));
 }else{
 log.atDebug().log("task:[START_PLUGINS] Succeed start moduleInstance {}", getPluginLabel((pluginWrapper.getDescriptor())));
 pluginWrapper.setModuleState(ModuleState.STARTED);
 startedPlugins.add(pluginWrapper);
 firePluginStateEvent(new ModuleStateEvent(getSelfReference(), pluginWrapper, pluginState));
 }

 } catch (Exception e) {
 log.atError().log(e.getMessage());
 }
 }
 }
 return null;
 }, executorService);**/
  }

  /**public CompletableFuture<ChangeStateResult> startModules(List<ModuleWrapper> modules){
   log.atDebug().log("task:[START_PLUGINS] start block {}",modules.size());
   return CompletableFuture.supplyAsync(() -> {
   for (ModuleWrapper pluginWrapper : modules) {
   ModuleState pluginState = pluginWrapper.getModuleState();
   if ((ModuleState.DISABLED != pluginState) && (ModuleState.STARTED != pluginState)) {
   try {
   log.atDebug().log("task:[START_PLUGINS] Start moduleInstance '{}'", getPluginLabel(pluginWrapper.getDescriptor()));
   CompletableFuture<Boolean>result = pluginRunner.startModule(pluginWrapper);
   if(result.isCompletedExceptionally()){
   log.atError().log("task:[START_PLUGINS] Failed start moduleInstance {}", getPluginLabel(pluginWrapper.getDescriptor()));
   }else{
   log.atDebug().log("task:[START_PLUGINS] Succeed start moduleInstance {}", getPluginLabel((pluginWrapper.getDescriptor())));
   pluginWrapper.setModuleState(ModuleState.STARTED);
   startedPlugins.add(pluginWrapper);
   firePluginStateEvent(new ModuleStateEvent(getSelfReference(), pluginWrapper, pluginState));
   }

   } catch (Exception e) {
   log.atError().log(e.getMessage());
   }
   }
   }
   return null;
   }, executorService);
   }**/
  /**
   * Start the specified moduleInstance and its dependencies.
   *
   * @param moduleId
   */
  @Override
  public CompletableFuture<ChangeStateResult> startModule(ModuleID moduleId) {
    return CompletableFuture.supplyAsync(() -> {
      checkPluginId(moduleId);

      ModuleWrapper pluginWrapper = getModule(moduleId);
      ModuleDescriptor pluginDescriptor = pluginWrapper.getDescriptor();
      ModuleState pluginState = pluginWrapper.getModuleState();
      if (ModuleState.STARTED == pluginState) {
        log.atDebug().log("task:[START_PLUGIN] Already started moduleInstance '{}'", getPluginLabel(pluginDescriptor));
        ChangeStateResult changeStateResult = new ChangeStateResult(moduleId, ModuleState.STARTED);
        return changeStateResult;
      }

      if (!resolvedPlugins.contains(pluginWrapper)) {
        log.atWarning().log("task:[START_PLUGIN] Cannot start an unresolved moduleInstance '{}'", getPluginLabel(pluginDescriptor));
        ChangeStateResult changeStateResult = new ChangeStateResult(moduleId, pluginState);
        return changeStateResult;
      }

      if (ModuleState.DISABLED == pluginState) {
        // automatically enable moduleInstance on manual moduleInstance start
        if (!enableModule(moduleId)) {
          ChangeStateResult changeStateResult = new ChangeStateResult(moduleId, pluginState);
          return changeStateResult;
        }
      }

      for (ModuleDependecy dependency : pluginDescriptor.getDependencies()) {
        startModule(dependency.getPluginId()).join();
      }

      try {
        log.atDebug().log("task:[START_PLUGIN] Start moduleInstance '{}'", getPluginLabel(pluginDescriptor));
        pluginRunner.startModule(pluginWrapper).whenComplete((aBoolean, throwable) -> {
          if (throwable != null) {
            log.atError().log("Imposible run moduleInstance ", throwable);
            return;
          }
          log.atDebug().log("task:[START_PLUGIN] Succeed start moduleInstance {}", getPluginLabel(pluginDescriptor));
          pluginWrapper.setModuleState(ModuleState.STARTED);
          startedPlugins.add(pluginWrapper);
          firePluginStateEvent(new ModuleStateEvent(getSelfReference(), pluginWrapper, pluginState));
        });
      } catch (CompletionException e) {
        log.atError().log(e.getMessage(), e);
      }

      ModuleState currentState = pluginWrapper.getModuleState();
      ChangeStateResult changeStateResult = new ChangeStateResult(moduleId, currentState);
      return changeStateResult;
    }, executorService);
  }

  /**
   * Stop all active plugins.
   */
  @Override
  public void stopPlugins() {
    // stop started plugins in reverse order
    Collections.reverse(startedPlugins);
    Iterator<ModuleWrapper> itr = startedPlugins.iterator();
    while (itr.hasNext()) {
      ModuleWrapper pluginWrapper = itr.next();
      ModuleState pluginState = pluginWrapper.getModuleState();
      if (ModuleState.STARTED == pluginState) {

        try {
          log.atDebug().log("task:[STOP_PLUGIN] Stop moduleInstance '{}'", getPluginLabel(pluginWrapper.getDescriptor()));

          pluginRunner.stopModule(pluginWrapper).whenComplete((aBoolean, throwable) -> {
            if (throwable != null) {
              log.atError().log(" task:[STOP_PLUGIN] Imposible stop moduleInstance ", throwable);
              return;
            }
            log.atDebug().log("task:[STOP_PLUGIN] Succeed stop moduleInstance {}", getPluginLabel(pluginWrapper.getDescriptor()));
            pluginWrapper.setModuleState(ModuleState.STOPPED);
            startedPlugins.remove(pluginWrapper);
            firePluginStateEvent(new ModuleStateEvent(getSelfReference(), pluginWrapper, pluginState));

          });
        } catch (Exception e) {
          log.atError().log(e.getMessage(), e);
        }
      }
    }
  }

  /**
   * Stop the specified moduleInstance and it's dependents.
   *
   * @param moduleId
   */
  @Override
  public CompletableFuture<ChangeStateResult> stopModule(ModuleID moduleId) {
    return CompletableFuture.supplyAsync(() -> {
      ModuleState state = stopPlugin(moduleId, true);
      return new ChangeStateResult(moduleId, state);
    }, executorService);
  }

  private ModuleState stopPlugin(ModuleID moduleId, boolean stopDependents) {
    checkPluginId(moduleId);
    ModuleWrapper pluginWrapper = getModule(moduleId);
    ModuleDescriptor pluginDescriptor = pluginWrapper.getDescriptor();
    ModuleState pluginState = pluginWrapper.getModuleState();
    if (ModuleState.STOPPED == pluginState) {
      log.atDebug().log("task:[STOP_PLUGIN] Already stopped moduleInstance '{}'", getPluginLabel(pluginDescriptor));
      return ModuleState.STOPPED;
    }

    // test for disabled moduleInstance
    if (ModuleState.DISABLED == pluginState) {
      // do nothing
      return pluginState;
    }

    if (stopDependents) {
      List<ModuleID> dependents = dependencyResolver.getDependents(moduleId);
      log.atDebug().log("task:[STOP_PLUGIN] stop all dependencies, total {}", dependents.size());
      while (!dependents.isEmpty()) {
        ModuleID dependent = dependents.remove(0);
        stopPlugin(dependent, false);
        dependents.addAll(0, dependencyResolver.getDependents(dependent));
      }
    }

    try {

      log.atDebug().log("task:[STOP_PLUGIN] Stop moduleInstance '{}'", getPluginLabel(pluginDescriptor));

      pluginRunner.stopModule(pluginWrapper).whenComplete((aBoolean, throwable) -> {
        if (throwable != null) {
          log.atError().log("task:[STOP_PLUGIN] Imposible stop moduleInstance ", throwable);
          return;
        }
        log.atDebug().log("task:[STOP_PLUGIN] Succeed stop moduleInstance {}", getPluginLabel(pluginDescriptor));
        pluginWrapper.setModuleState(ModuleState.STOPPED);
        startedPlugins.remove(pluginWrapper);
        firePluginStateEvent(new ModuleStateEvent(getSelfReference(), pluginWrapper, pluginState));

      }).join();

    } catch (CompletionException e) {
      log.atError().log(e.getMessage(), e);
    }

    return pluginWrapper.getModuleState();
  }

  private void checkPluginId(ModuleID moduleId) {
    if (!plugins.containsKey(moduleId)) {
      throw new IllegalArgumentException(String.format("Unknown moduleId %s", moduleId));
    }
  }

  @Override
  public CompletableFuture<Boolean> disableModule(ModuleID moduleId) {
    return CompletableFuture.supplyAsync(() -> {
      checkPluginId(moduleId);
      ModuleWrapper pluginWrapper = getModule(moduleId);
      ModuleDescriptor pluginDescriptor = pluginWrapper.getDescriptor();
      ModuleState pluginState = pluginWrapper.getModuleState();
      if (ModuleState.DISABLED == pluginState) {
        log.atDebug().log("task:[DISABLE_PLUGIN] Already disabled moduleInstance '{}'", getPluginLabel(pluginDescriptor));
        return true;
      }

      ChangeStateResult stopPlugin = stopModule(moduleId).join();
      if (ModuleState.STOPPED == stopPlugin.getPluginState()) {
        pluginWrapper.setModuleState(ModuleState.DISABLED);

        firePluginStateEvent(new ModuleStateEvent(this, pluginWrapper, ModuleState.STOPPED));

        if (!pluginStatusProvider.disablePlugin(moduleId)) {
          return false;
        }

        log.atDebug().log("task:[DISABLE_PLUGIN] moduleInstance '{}'", getPluginLabel(pluginDescriptor));

        return true;
      }

      return false;
    }, executorService);
  }

  @Override
  public boolean enableModule(ModuleID moduleId) {
    checkPluginId(moduleId);

    ModuleWrapper pluginWrapper = getModule(moduleId);
    if (!isPluginValid(pluginWrapper)) {
      log.atWarning().log("task:[ENABLE_PLUGIN] Plugin '{}' can not be enabled", getPluginLabel(pluginWrapper.getDescriptor()));
      return false;
    }

    ModuleDescriptor pluginDescriptor = pluginWrapper.getDescriptor();
    ModuleState pluginState = pluginWrapper.getModuleState();
    if (ModuleState.DISABLED != pluginState) {
      log.atDebug().log("task:[ENABLE_PLUGIN] Plugin '{}' is not disabled", getPluginLabel(pluginDescriptor));
      return true;
    }

    if (!pluginStatusProvider.enablePlugin(moduleId)) {
      return false;
    }

    pluginWrapper.setModuleState(ModuleState.CREATED);

    firePluginStateEvent(new ModuleStateEvent(this, pluginWrapper, pluginState));

    log.atDebug().log("task:[ENABLE_PLUGIN] Enabled moduleInstance '{}'", getPluginLabel(pluginDescriptor));

    return true;
  }


  @SuppressWarnings("rawtypes")
  @Override
  public List<Class<?>> getExtensionClasses(ModuleID moduleId) {
    List<ExtensionWrapper> extensionsWrapper = extensionFinder.find(moduleId);
    List<Class<?>> extensionClasses = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper extensionWrapper : extensionsWrapper) {
      Class<?> c = extensionWrapper.getDescriptor().extensionClass;
      extensionClasses.add(c);
    }
    return extensionClasses;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> List<Class<T>> getExtensionClasses(Class<T> type) {
    List<ExtensionWrapper<T>> extensionsWrapper = extensionFinder.find(type);
    List<Class<T>> extensionClasses = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper<T> extensionWrapper : extensionsWrapper) {
      Class<T> c = (Class<T>) extensionWrapper.getDescriptor().extensionClass;
      extensionClasses.add(c);
    }

    return extensionClasses;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> List<Class<T>> getExtensionClasses(Class<T> type, ModuleID moduleId) {
    List<ExtensionWrapper<T>> extensionsWrapper = extensionFinder.find(type, moduleId);
    List<Class<T>> extensionClasses = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper<T> extensionWrapper : extensionsWrapper) {
      Class<T> c = (Class<T>) extensionWrapper.getDescriptor().extensionClass;
      extensionClasses.add(c);
    }

    return extensionClasses;
  }

  @Override
  public <T> List<T> getExtensions(Class<T> type) {
    List<ExtensionWrapper<T>> extensionsWrapper = extensionFinder.find(type);
    List<T> extensions = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper<T> extensionWrapper : extensionsWrapper) {
      extensions.add(extensionWrapper.getExtension());
    }

    return extensions;
  }

  @Override
  public <T> List<T> getExtensions(Class<T> type, ModuleID moduleId) {
    List<ExtensionWrapper<T>> extensionsWrapper = extensionFinder.find(type, moduleId);
    List<T> extensions = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper<T> extensionWrapper : extensionsWrapper) {
      extensions.add(extensionWrapper.getExtension());
    }

    return extensions;
  }

  @Override
  @SuppressWarnings("unchecked")
  public List getExtensions(ModuleID moduleId) {
    List<ExtensionWrapper> extensionsWrapper = extensionFinder.find(moduleId);
    List extensions = new ArrayList<>(extensionsWrapper.size());
    for (ExtensionWrapper extensionWrapper : extensionsWrapper) {
      extensions.add(extensionWrapper.getExtension());
    }

    return extensions;
  }

  @Override
  public Set<String> getExtensionClassNames(ModuleID moduleId) {
    return extensionFinder.findClassNames(moduleId);
  }

  @Override
  public ExtensionFactoryManager getExtensionFactory() {
    return extensionFactory;
  }

  @Override
  public PluginLoaderManager getPluginLoaderManager() {
    return pluginLoaderManager;
  }

  public Path getPluginsRoot() {
    return pluginsRoot;
  }

  @Override
  public RuntimeMode getRuntimeMode() {
    return ReactorConfig.currentRuntimeMode();
  }

  @Override
  public Optional<ModuleWrapper> whichModule(Class<?> clazz) {
    ClassLoader classLoader = clazz.getClassLoader();
    for (ModuleWrapper plugin : resolvedPlugins) {
      if (plugin.getPluginClassLoader() == classLoader) {
        return Optional.of(plugin);
      }
    }

    return Optional.empty();
  }

  @Override
  public Optional<ModuleWrapper> whichModule(String clazzName) {
    for (ModuleWrapper plugin : resolvedPlugins) {
      ModuleDescriptor descriptor = plugin.getDescriptor();
      String moduleClassname = descriptor.getModuleClass();
      if (moduleClassname.equals(clazzName)) {
        return Optional.of(plugin);
      }
    }
    return Optional.empty();
  }

  @Override
  public synchronized void registerObserver(ModuleStateObserver observer) {
    pluginStateListeners.add(observer);
  }

  @Override
  public synchronized void removeObserve(ModuleStateObserver observer) {
    pluginStateListeners.remove(observer);
  }

  public String getVersion() {
    String version = null;

    Package pack = ModuleManager.class.getPackage();
    if (pack != null) {
      version = pack.getImplementationVersion();
      if (version == null) {
        version = pack.getSpecificationVersion();
      }
    }

    return (version != null) ? version : "0.0.0";
  }


  protected ModuleDescriptorFinder getPluginDescriptorFinder() {
    return pluginDescriptorFinder;
  }

  public ModuleFactoryManager getPluginFactory() {
    return pluginFactory;
  }

  @Override
  public ModuleRunner getPluginRunner() {
    return pluginRunner;
  }

  protected Path createPluginsRoot() {
    return ReactorConfig.getModulePath();
  }

  /**
   * Check if this moduleInstance is valid (satisfies "requires" param) for a given system version.
   *
   * @param pluginWrapper the moduleInstance to check
   * @return true if moduleInstance satisfies the "requires" or if requires was left blank
   */
  protected boolean isPluginValid(ModuleWrapper pluginWrapper) {
    String requires = pluginWrapper.getDescriptor().getRequires().trim();
    if (!isExactVersionAllowed() && requires.matches("^\\d+\\.\\d+\\.\\d+$")) {
      // If exact versions are not allowed in requires, rewrite to >= expression
      requires = ">=" + requires;
    }
    if (systemVersion.equals("0.0.0") || versionManager.checkVersionConstraint(systemVersion, requires)) {
      return true;
    }

    ModuleDescriptor pluginDescriptor = pluginWrapper.getDescriptor();
    log.atWarning().log("Plugin '{}' requires a minimum system version of {}, and you have {}",
      getPluginLabel(pluginDescriptor),
      pluginWrapper.getDescriptor().getRequires(),
      getSystemVersion());

    return false;
  }

  protected boolean isPluginDisabled(ModuleID moduleId) {
    return pluginStatusProvider.isPluginDisabled(moduleId);
  }

  protected void resolvePlugins() throws ReactorException {
    // retrieves the plugins descriptors
    List<ModuleDescriptor> descriptors = new ArrayList<>();
    for (ModuleWrapper plugin : plugins.values()) {
      descriptors.add(plugin.getDescriptor());
    }

    DependencyGraph.Result result = dependencyResolver.resolve(descriptors);

    if (result.hasCyclicDependency()) {
      throw new DependencyGraph.CyclicDependencyException();
    }

    List<ModuleID> notFoundDependencies = result.getNotFoundDependencies();
    if (!notFoundDependencies.isEmpty()) {
      throw new DependencyGraph.DependenciesNotFoundException(notFoundDependencies);
    }

    List<DependencyGraph.WrongDependencyVersion> wrongVersionDependencies = result.getWrongVersionDependencies();
    if (!wrongVersionDependencies.isEmpty()) {
      throw new DependencyGraph.DependenciesWrongVersionException(wrongVersionDependencies);
    }

    List<ModuleID> sortedPlugins = result.getSortedPlugins();

    // move plugins from "unresolved" to "resolved"
    for (ModuleID moduleId : sortedPlugins) {
      ModuleWrapper pluginWrapper = plugins.get(moduleId);
      if (unresolvedPlugins.remove(pluginWrapper)) {
        ModuleState pluginState = pluginWrapper.getModuleState();
        if (pluginState != ModuleState.DISABLED) {
          //if(pluginWrapper.getModuleOrigen()== ModuleOrigen.EXTERNAL_CLASSPATH){
          if(pluginWrapper.getModuleOrigen()== SourceModuleType.CLASSPATH_SOURCE_MODULE){
           getPluginFactory().mountFactory(pluginWrapper);
          }
          pluginWrapper.setModuleState(ModuleState.RESOLVED);
        }

        resolvedPlugins.add(pluginWrapper);
        log.atDebug().log("Plugin '{}' resolved", getPluginLabel(pluginWrapper.getDescriptor()));

        firePluginStateEvent(new ModuleStateEvent(this, pluginWrapper, pluginState));
      }
    }
  }

  protected synchronized void firePluginStateEvent(ModuleStateEvent event) {
    Set<ModuleWrapper> pluginWrapperList = new HashSet<>();

    for (ModuleStateObserver listener : pluginStateListeners) {
      log.atTrace().log("Fire '{}' to '{}'", event, listener);
      listener.pluginStateChanged(event);
    }
  }

  protected Set<ModuleWrapper> loadPluginsFromClasspath() throws ReactorException, URISyntaxException {
    Iterable<Class<?>> allPlugins = ClassIndex.getAnnotated(ModuleDefinition.class);
    SourceClasspathModule sourceClasspathModule = new SourceClasspathModule(getClass().getClassLoader());
    //SourceFileModule sourceFileModule = new SourceFileModule()
    return loadPluginsFromPath(sourceClasspathModule);
//    for (Class<?> klass : allPlugins) {
//
//      CodeSource codeSource = klass.getProtectionDomain().getCodeSource();
//
//      if (codeSource != null) {
//        return loadPluginsFromPath(Constants.VIRTUAL_PATH);
//      }
//    }
//    return new HashSet<>();
  }

  protected Set<ModuleWrapper> loadPluginsFromPath(SourceModule sourceModule) throws ReactorException {
    log.atDebug().log("LOAD PLUGINS FROM PATH {}", sourceModule);
    Set<ModuleWrapper> pluginWrapperList = new HashSet<>();
    // Test for moduleInstance path duplication
    Set<ModuleID> moduleIds = idForPath(sourceModule);
    if (moduleIds.size() > 0) {
      String currentLoaders = moduleIds.stream()
        .map(ModuleID::value)
        .collect(Collectors.joining(", "));
      throw new ModuleAlreadyLoadedException(ModuleID.of(currentLoaders), sourceModule);
    }

    // Retrieve and validate the moduleInstance descriptor
    ModuleDescriptorFinder pluginDescriptorFinder = getPluginDescriptorFinder();
    log.atDebug().log("Use '{}' to find plugins descriptors", pluginDescriptorFinder);
    log.atDebug().log("Finding moduleInstance descriptor for moduleInstance '{}'", sourceModule);
    Set<ModuleDescriptor> pluginDescriptorsList = pluginDescriptorFinder.find(sourceModule);

    for (ModuleDescriptor pluginDescriptor : pluginDescriptorsList) {

      validatePluginDescriptor(pluginDescriptor);

      // Check there are no loaded plugins with the retrieved id
      ModuleID moduleId = pluginDescriptor.getModuleID();
      if (plugins.containsKey(moduleId)) {
        ModuleWrapper loadedPlugin = getModule(moduleId);
        throw new ReactorException("There is an already loaded moduleInstance ({}) "
          + "with the same id ({}) as the moduleInstance at path '{}'. Simultaneous loading "
          + "of plugins with the same PluginId is not currently supported.\n"
          + "As a workaround you may include PluginVersion and PluginProvider "
          + "in PluginId.",
          loadedPlugin, moduleId, sourceModule);
      }

      log.atDebug().log("Found descriptor {}", pluginDescriptor);
      String moduleClassName = pluginDescriptor.getModuleClass();
      log.atDebug().log("Class '{}' for moduleInstance '{}'", moduleClassName, sourceModule);

      // load moduleInstance
      log.atDebug().log("Loading moduleInstance '{}'", sourceModule);
      ClassLoader moduleClassLoader = getPluginLoaderManager().loadPlugin(sourceModule, pluginDescriptor);
      log.atDebug().log("Loaded moduleInstance '{}' with class loader '{}'", sourceModule, moduleClassLoader);

      // create the moduleInstance wrapper
      log.atDebug().log("Creating wrapper for moduleInstance '{}'", sourceModule);


      RuntimeMode runtimeMode = getRuntimeMode();
      ModuleFactory moduleFactory = getPluginFactory();
      SourceModuleType origen =  sourceModule.sourceModuleType;

      ModuleWrapper pluginWrapper = new ModuleWrapper(this, pluginDescriptor, sourceModule, moduleClassLoader, moduleFactory, runtimeMode,origen);
      //pluginWrapper.setModuleFactory(getPluginFactory());
      //pluginWrapper.setRuntimeMode(getRuntimeMode());

      // test for disabled moduleInstance
      if (isPluginDisabled(pluginDescriptor.getModuleID())) {
        log.atDebug().log("Plugin '{}' is disabled", sourceModule);
        pluginWrapper.setModuleState(ModuleState.DISABLED);
      }

      // validate the moduleInstance
      if (!isPluginValid(pluginWrapper)) {
        log.atWarning().log("Plugin '{}' is invalid and it will be disabled", sourceModule);
        pluginWrapper.setModuleState(ModuleState.DISABLED);
      }

      log.atDebug().log("Created wrapper '{}' for moduleInstance '{}'", pluginWrapper, sourceModule);

      moduleId = pluginDescriptor.getModuleID();

      // add moduleInstance to the list with plugins
      plugins.put(moduleId, pluginWrapper);
      getUnresolvedModules().add(pluginWrapper);


      pluginWrapperList.add(pluginWrapper);
    }
    return pluginWrapperList;

  }


  /**
   * Tests for already loaded plugins on given path.
   *
   * @param sourceModule the path to investigate
   * @return id of moduleInstance or null if not loaded
   */
  protected Set<ModuleID> idForPath(SourceModule sourceModule) {
    return plugins.values().stream()
      .filter((pluginW) -> pluginW.getSourceModule().equals(sourceModule))
      .map(ModuleWrapper::getPluginId)
      .collect(Collectors.toSet());
  }


  protected Set<ModuleID> idForClass(Class<?> moduleClass){
    return plugins.values().stream()
      .filter((pluginW)->pluginW.getModuleClass().isPresent())
      .filter((pluginW) -> pluginW.getModuleClass().get().equals(moduleClass))
      .map(ModuleWrapper::getPluginId)
      .collect(Collectors.toSet());
  }

  /**
   * Override this to change the validation criteria.
   *
   * @param descriptor the moduleInstance descriptor to validate
   * @throws ReactorException if validation fails
   */
  protected void validatePluginDescriptor(ModuleDescriptor descriptor) throws ReactorException {
    log.atDebug().log("validating descriptor {} ", descriptor.toString());

    if (StringUtils.isNullOrEmpty(descriptor.getModuleID().value())) {
      throw new ReactorException("Field 'id' cannot be empty");
    }

    if (descriptor.getVersion() == null) {
      throw new ReactorException("Field 'version' cannot be empty");
    }
  }

  // TODO add this method in PluginManager as default method for Java 8.
  protected boolean isDevelopment() {
    return RuntimeMode.DEVELOPMENT.equals(getRuntimeMode());
  }

  /**
   * @return true if exact versions in requires is allowed
   */
  public boolean isExactVersionAllowed() {
    return exactVersionAllowed;
  }

  /**
   * Set to true to allow requires expression to be exactly x.y.z.
   * The default is false, meaning that using an exact version x.y.z will
   * implicitly mean the same as >=x.y.z
   *
   * @param exactVersionAllowed set to true or false
   */
  public void setExactVersionAllowed(boolean exactVersionAllowed) {
    this.exactVersionAllowed = exactVersionAllowed;
  }

  @Override
  public VersionManager getVersionManager() {
    return versionManager;
  }

  /**
   * The moduleInstance label is used in logging and it's a string in format {@code moduleId@pluginVersion}.
   */
  protected String getPluginLabel(ModuleDescriptor pluginDescriptor) {
    return pluginDescriptor.getModuleID() + "@" + pluginDescriptor.getVersion();
  }

  protected ModuleManager getSelfReference() {
    return this;
  }
  /************************* Factory Method **********************/
  /**
   * Full specification factoy methods, this methods are used by init block
   * @return
   */

  /**
   * createPluginRepository, create moduleInstance repository
   *
   * @return
   */
  protected abstract PluginRepository createPluginRepository();

  /**
   * createPluginFactory, create new moduleInstance factory
   *
   * @return PluginFactory
   */
  protected abstract ModuleFactoryManager createPluginFactory();

  /**
   * createExtensionFactory, create extensions factory
   *
   * @return ExtensionFactory
   */
  protected abstract ExtensionFactoryManager createExtensionFactory();

  /**
   * createPluginDescriptorFinder, create descriptor finder
   *
   * @return PluginDescriptorFinder
   */
  protected abstract ModuleDescriptorFinder createPluginDescriptorFinder();

  /**
   * createExtensionFinder, provides extension finder
   *
   * @return ExtensionFinder
   */
  protected abstract ExtensionFinder createExtensionFinder();

  /**
   * createPluginStatusProvider, provide moduleInstance status
   *
   * @return PluginStatusProvider
   */
  protected abstract ModuleStatusProvider createPluginStatusProvider();

  /**
   * createPluginLoaderManager, provides moduleInstance Loader
   *
   * @return PluginLoader
   */
  protected abstract PluginLoaderManager createPluginLoaderManager();

  /**
   * createVersionManager, provide version mananger
   *
   * @return VersionManager
   */
  protected abstract VersionManager createVersionManager();

  /**
   * createPluginRuners, Provide avaible Runners
   *
   * @return List<PluginRunner>
   */
  protected abstract ModuleRunner createPluginRuner();

  /**
   * createExecutorService, provide executor service
   *
   * @return
   */
  abstract protected Executor createExecutorService();


}
