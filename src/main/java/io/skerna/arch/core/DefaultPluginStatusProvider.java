package io.skerna.arch.core;

import io.skerna.arch.core.util.FileUtils;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The default implementation for {@link ModuleStatusProvider}.
 * The enabled plugins are read from {@code enabled.txt} file and
 * the disabled plugins are read from {@code disabled.txt} file.
 * <p>
 */
public class DefaultPluginStatusProvider implements ModuleStatusProvider {
  private static final Logger log = LoggerFactory.logger(DefaultPluginStatusProvider.class);

  private final Path pluginsRoot;

  private List<ModuleID> enabledPlugins = new ArrayList<>();
  private List<ModuleID> disabledPlugins = new ArrayList<>();

  public DefaultPluginStatusProvider(Path pluginsRoot) {
    this.pluginsRoot = pluginsRoot;

    try {
      // create a list with moduleInstance identifiers that should be only accepted by this manager (whitelist from plugins/enabled.txt file)
      List<String> linesPluginsID = FileUtils.readLines(pluginsRoot.resolve("enabled.txt"), true);
      for (String linePluginId : linesPluginsID) {
        enabledPlugins.add(ModuleID.of(linePluginId));
      }
      log.atInfo().log("Enabled plugins: {}", enabledPlugins);

      // create a list with moduleInstance identifiers that should not be accepted by this manager (blacklist from plugins/disabled.txt file)
      List<String> linesPluginsIDDisabled = FileUtils.readLines(pluginsRoot.resolve("disabled.txt"), true);
      for (String linePluginId : linesPluginsIDDisabled) {
        disabledPlugins.add(ModuleID.of(linePluginId));
      }
      log.atInfo().log("Disabled plugins: {}", disabledPlugins);
    } catch (IOException e) {
      log.atError().log(e.getMessage(), e);
    }
  }

  @Override
  public boolean isPluginDisabled(ModuleID moduleId) {
    if (disabledPlugins.contains(moduleId)) {
      return true;
    }

    return !enabledPlugins.isEmpty() && !enabledPlugins.contains(moduleId);
  }

  @Override
  public boolean disablePlugin(ModuleID moduleId) {
    if (disabledPlugins.add(moduleId)) {
      try {
        List<String> disablePluginsStrings = disabledPlugins.
          stream().
          map(ModuleID::value).collect(Collectors.toList());

        FileUtils.writeLines(disablePluginsStrings, pluginsRoot.resolve("disabled.txt").toFile());
      } catch (IOException e) {
        log.atError().log("Failed to disable moduleInstance {}", moduleId, e);
        return false;
      }
    }

    return true;
  }

  @Override
  public boolean enablePlugin(ModuleID moduleId) {
    if (disabledPlugins.remove(moduleId)) {
      List<String> disablePluginsStrings = disabledPlugins.
        stream().
        map(ModuleID::value).collect(Collectors.toList());

      try {
        FileUtils.writeLines(disablePluginsStrings, pluginsRoot.resolve("disabled.txt").toFile());
      } catch (IOException e) {
        log.atError().log("Failed to enable moduleInstance {}", moduleId, e);
        return false;
      }
    }

    return true;
  }

}
