package io.skerna.arch.core.factories;

import io.skerna.arch.core.AbstractModule;

public class DefaultModuleFactory extends  AbstractModuleFactory{
  @Override
  public boolean isApplicable(Class<?> target) {
    return AbstractModule.class.isAssignableFrom(target);
  }

}
