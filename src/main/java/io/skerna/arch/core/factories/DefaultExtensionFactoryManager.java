package io.skerna.arch.core.factories;

import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.spi.ExtensionFactory;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class DefaultExtensionFactoryManager implements ExtensionFactoryManager {
  private static final Logger log = LoggerFactory.logger(DefaultExtensionFactoryManager.class);

  private ModuleManager manager;
  private boolean allowScanClasspath;
  private Map<String, ExtensionFactory> factoryMap = new HashMap<>();


  public DefaultExtensionFactoryManager(ModuleManager manager) {
    this(manager, true);
  }

  public DefaultExtensionFactoryManager(ModuleManager manager, boolean allowScanClasspath) {
    this.manager = manager;
    this.allowScanClasspath = allowScanClasspath;
    initialize();
  }

  void initialize() {
    if (allowScanClasspath) {
      log.atDebug().log("Load plugin factories from meta services ");
      ServiceLoader<ExtensionFactory> providers = ServiceLoader.load(ExtensionFactory.class);
      for (ExtensionFactory provider : providers) {
        install(provider);
      }
    }
  }

  @Override
  public ExtensionFactoryManager register(ExtensionFactory additive) {
    install(additive);
    return this;
  }

  @Override
  public ExtensionFactoryManager unregister(ExtensionFactory additive) {
    String additiveName = additive.getClass().getCanonicalName();
    factoryMap.remove(additiveName);
    return this;
  }

  private void install(ExtensionFactory extensionFactory) {
    if (extensionFactory.equals(this)) {
      log.atWarning().log("recursive factories are not allowed");
      return;
    }
    log.atTrace().log("New factory installed {}", extensionFactory);
    String factory = extensionFactory.getClass().getCanonicalName();
    factoryMap.put(factory, extensionFactory);
  }

  @Override
  public ExtensionFactoryManager flow() {
    return this;
  }

  @Override
  public int countComponents() {
    return factoryMap.size();
  }

  @Override
  public Object create(Class<?> extensionClass) {
    String extensionFactoryname = extensionClass.getCanonicalName();
    log.atDebug().log("Create instance for extension '{}'", extensionFactoryname);

    //When factory is not found in cache factories, search and intitialize cache when instance
    // be successfully built
    log.atDebug().log("Attempt create plugin with total factories strategies {}", factoryMap.size());
    Object newInstancePlugin = null;
    for (ExtensionFactory factory : factoryMap.values()) {
      String factoryName = factory.getClass().getCanonicalName();
      log.atDebug().log("Attemp Using factory " + factory);
      try {
        newInstancePlugin = factory.create(extensionClass);
        if (newInstancePlugin == null) {
          log.atWarning().log("Factory {} cannot build plugin {} ", factoryName, extensionClass);
          continue;
        }
        log.atDebug().log("A instance of {} was succed build by {}", extensionClass, factoryName);
        return newInstancePlugin;
      } catch (Exception ex) {
        log.atError().log("could not have created pugin {} using factory {}", ex, factoryName, ex.getMessage());
        ex.printStackTrace();

      }

    }
    return null;
  }
}
