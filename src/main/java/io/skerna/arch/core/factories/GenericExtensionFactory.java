package io.skerna.arch.core.factories;

import io.skerna.arch.core.spi.ExtensionFactory;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

/**
 * The default implementation for {@link ExtensionFactory}.
 * It uses {@link Class#newInstance} method.
 * <p>
 */
public class GenericExtensionFactory implements ExtensionFactory {
  private static final Logger log = LoggerFactory.logger(GenericExtensionFactory.class);

  /**
   * Creates an extension instance. If an error occurs than that error is logged and the method returns {@code null}.
   *
   * @param extensionClass
   * @return
   */
  @Override
  public Object create(Class<?> extensionClass) throws Exception {
    log.atDebug().log("Create instance for extension '{}'", extensionClass.getName());
    return extensionClass.newInstance();
  }

  @Override
  public boolean isApplicable(Class<?> target) {
    return true;
  }

}
