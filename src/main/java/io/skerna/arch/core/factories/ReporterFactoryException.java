package io.skerna.arch.core.factories;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

public class ReporterFactoryException extends RuntimeException {
  final List<Throwable> causes;

  public ReporterFactoryException(String message, List<Throwable> causes) {
    super(message);
    this.causes = causes;
  }

  void append(Throwable throwable) {
    causes.add(throwable);
  }

  @Override
  public String getMessage() {
    if (causes.isEmpty()) {
      return super.getMessage();
    }
    StringBuilder stringBuilder = new StringBuilder();
    for (Throwable cause : causes) {
      stringBuilder.append(cause.getMessage());
    }
    return stringBuilder.toString();
  }

  @Override
  public void printStackTrace() {
    if (causes.isEmpty()) {
      super.printStackTrace();
      return;
    }
    for (Throwable cause : causes) {
      cause.printStackTrace();
    }
  }

  @Override
  public void printStackTrace(PrintStream s) {
    if (causes.isEmpty()) {
      super.printStackTrace(s);
      return;
    }
    for (Throwable cause : causes) {
      cause.printStackTrace(s);
    }
  }

  @Override
  public void printStackTrace(PrintWriter s) {
    if (causes.isEmpty()) {
      super.printStackTrace(s);
      return;
    }
    for (Throwable cause : causes) {
      cause.printStackTrace(s);
    }
  }
}
