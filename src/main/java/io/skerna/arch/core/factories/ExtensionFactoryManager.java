package io.skerna.arch.core.factories;

import io.skerna.arch.core.Flow;
import io.skerna.arch.core.spi.ExtensionFactory;

public interface ExtensionFactoryManager extends Flow<ExtensionFactory, ExtensionFactoryManager>, ExtensionFactory {


  @Override
  default boolean isApplicable(Class<?> target) {
    return true;
  }
}
