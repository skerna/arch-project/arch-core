package io.skerna.arch.core.factories;

import io.skerna.arch.core.*;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.util.Multimap;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.spi.ModuleFactory;
import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.SourceModuleType;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.*;


public class DefaultModuleFactoryManager implements ModuleFactoryManager {
  protected ModuleManager manager;
  protected Map<ModuleFactoryID, ModuleFactory> globalClassPathFactories = new HashMap<>();
  protected Multimap<ModuleID, ModuleFactory> moduleClassPathFactories = new Multimap<>();
  private Logger log = getLogger();
  private boolean allowScanClasspath;

  public DefaultModuleFactoryManager(boolean allowScanClasspath) {
    this.allowScanClasspath = allowScanClasspath;
  }

  /**
   * Inject {@link ModuleManager } instance
   *
   * @param moduleManager
   */
  @Override
  public final void bindManager(ModuleManager moduleManager) {
    Objects.requireNonNull(moduleManager, "Module manager cant not be null");
    this.manager = moduleManager;
    initialize();
    log.atDebug().log("All factories registered manuall and auto = " + globalClassPathFactories.size());
  }

  /**
   * monta todos los factories proporcionado por un modulo wrapper
   *
   * @param moduleWrapper
   */
  @Override
  public void mountFactory(ModuleWrapper moduleWrapper) {
    ModuleID moduleID = moduleWrapper.getPluginId();
    log.atDebug().log("Mount factories....{}", moduleID);
    if (moduleWrapper.getModuleOrigen() != SourceModuleType.FILE_SOURCE_MODULE) {
      log.atWarning().log("No se instalaron las fabricas el modulo no es de tipo externo");
      return;
    }
    ClassLoader moduleClassLoader = moduleWrapper.getPluginClassLoader();
    ServiceLoader<ModuleFactory> loader = ServiceLoader.load(ModuleFactory.class, moduleClassLoader);
    for (ModuleFactory factory : loader) {
      log.atDebug().log("Mount factory {} for plugin {}", factory.getFactoryIdentifier(), moduleID);
      installExternalFactory(moduleID, factory);
    }
  }

  /**
   * desomonta todos los factories proporcionado por un modulo wrapper
   *
   * @param moduleWrapper
   */
  @Override
  public void umountFactory(ModuleWrapper moduleWrapper) {
    ModuleID moduleID = moduleWrapper.getPluginId();
    log.atDebug().log("Umount factories....{}", moduleID);
    this.moduleClassPathFactories.remove(moduleID);

  }

  @Override
  public void beforeInstall(ModuleFactory moduleFactory) {

  }

  @Override
  public boolean isApplicable(Class<?> target) {
    return false;
  }

  private void initialize() {
    if (allowScanClasspath) {
      onAutoScanEnabled();
    }
  }

  protected void onAutoScanEnabled() {
    log.atDebug().log("Load plugin factories from meta services ");
    ServiceLoader<ModuleFactory> providers = ServiceLoader.load(ModuleFactory.class);
    for (ModuleFactory provider : providers) {
      installFactory(provider);
    }
  }

  /**
   * Create new instance of {@link Module} implementation
   *
   * @param moduleWrapper
   * @return
   * @throws ReporterFactoryException
   */
  @Override
  public Module createModule(ModuleWrapper moduleWrapper) {
    Optional<Class<?>> moduleClass = moduleWrapper.getModuleClass();
    // Check if class is loaded from classloader
    if (!moduleClass.isPresent()) {
      log.atError().log("Required classname was null");
      return null;
    }
    Class moduleClassName = moduleWrapper.getModuleClass().get();
    String moduleClassNameString = moduleClassName.getCanonicalName();
    Optional<ModuleFactory> moduleFactoryResult = resolveFactory(moduleWrapper);

    log.atDebug().log("Create instance for plugin '{}' using '{}'", moduleClassName, moduleFactoryResult);
    if (moduleFactoryResult.isPresent()) {
      ModuleFactory moduleFactory = moduleFactoryResult.get();
      try {
        Module module = moduleFactory.createModule(moduleWrapper);
        module.injectWrapper(moduleWrapper);
        if (module != null) {
          return module;
        }
      } catch (Exception e) {
        log.atWarning().log("Primary factory {} return error  on build module  {}", moduleFactory, moduleClass, e);
      }
    }
//    // 2) When factory is not found in cache factories, search and intitialize cache when instance
//    // be successfully built
//    Collection<ModuleFactory> applicableFactories = moduleFactoryMap.values();
//    // Reporter all exceptions
//    List<Throwable> factoriesExceptions = new ArrayList<>();
//    log.atDebug().log("Attempt create plugin with total applicable factories strategies {}", applicableFactories.size());
//    Module newInstancePlugin = null;
//
//    for (Iterator<ModuleFactory> iter = applicableFactories.iterator(); iter.hasNext(); ) {
//      ModuleFactory moduleFactory = iter.next();
//
//      String factoryName = moduleFactory.getClass().getCanonicalName();
//      if (iter.hasNext() == false) {
//        log.atDebug().log("Last Attemp Using factory!!! {} ", moduleFactory);
//      } else {
//        log.atDebug().log("Attemp Using factory {}", moduleFactory);
//      }
//
//      try {
//        newInstancePlugin = moduleFactory.create(moduleWrapper);
//
//        // Factory return null value, test next strategy
//        if (newInstancePlugin != null) {
//          log.atDebug().log("A instance of {} was success build by {}", moduleClassName, factoryName);
//          return newInstancePlugin; // Return instance
//        } else if (!iter.hasNext()) { // IF LAST strategy return NULL
//          log.atWarning().log("Last factory {} return null when building instance of {}", factoryName, moduleClassName);
//          return null;
//        } else {
//          log.atTrace().log("Factory {} cannot build plugin {} ", factoryName, moduleClassName);
//        }
//
//      } catch (Exception ex) {
//        log.atTrace().log("could not have created pugin {} using factory {}", moduleClassName, factoryName, ex.getMessage());
//        if (ex instanceof NoSuchMethodException) {
//          log.atTrace().log("Factory bad constrcutor {}", factoryName, ex);
//        } else {
//          factoriesExceptions.add(ex);
//          ex.printStackTrace();
//        }
//      }
//    }

    throw new ReporterFactoryException(String.format("Imposible build module {%s} all [strategies] strategies return null instance", moduleClassName), new ArrayList<>());
  }


  @Override
  public ModuleFactoryManager register(ModuleFactory additive) {
    installFactory(additive);
    return this;
  }

  @Override
  public ModuleFactoryManager unregister(ModuleFactory additive) {
    globalClassPathFactories.remove(additive);
    return this;
  }

  @Override
  public DefaultModuleFactoryManager flow() {
    return this;
  }

  @Override
  public int countComponents() {
    return globalClassPathFactories.size();
  }

  protected final void installFactory(ModuleFactory moduleFactory) {
    if (moduleFactory.equals(this)) {
      log.atWarning().log("Recursive factory are not allowed");
      return;
    }
    String factoryFullName = moduleFactory.getClass().getCanonicalName();
    log.atTrace().log("Installing factory {}", factoryFullName);
    Objects.requireNonNull(moduleFactory);
    moduleFactory.bindManager(this.manager);
    ModuleFactoryID moduleFactoryId = moduleFactory.getFactoryIdentifier();
    if (globalClassPathFactories.containsKey(moduleFactoryId)) {
      getLogger().atError().log("El factory actualmente ya fue registrado {}", moduleFactoryId);
      return;
    }
    beforeInstall(moduleFactory);
    globalClassPathFactories.put(moduleFactoryId, moduleFactory);
  }

  protected final void installExternalFactory(ModuleID moduleID, ModuleFactory moduleFactory) {
    if (moduleFactory.equals(this) ) {
      log.atWarning().log("Recursive factory are not allowed");
      return;
    }
    if(globalClassPathFactories.containsKey(moduleFactory.getFactoryIdentifier())){
      log.atWarning().log("Currently this {} factory is register in globals",moduleFactory.getFactoryIdentifier());
      return;
    }
    String factoryFullName = moduleFactory.getClass().getCanonicalName();
    log.atTrace().log("Installing factory {}", factoryFullName);
    Objects.requireNonNull(moduleFactory);
    moduleFactory.bindManager(this.manager);
    ModuleFactoryID moduleFactoryId = moduleFactory.getFactoryIdentifier();
    if (globalClassPathFactories.containsKey(moduleFactoryId)) {
      getLogger().atError().log("El factory actualmente ya fue registrado {}", moduleFactoryId);
      return;
    }
    beforeInstall(moduleFactory);
    this.moduleClassPathFactories.put(moduleID, moduleFactory);
  }


  private Optional<ModuleFactory> resolveFactory(ModuleWrapper moduleWrapper) {
    ModuleDescriptor moduleDescriptor = moduleWrapper.getDescriptor();
    ModuleID moduleID = moduleWrapper.getPluginId();
    Optional<Class<?>> target = moduleWrapper.getModuleClass();
    Class<?> aclass = target.get();
    var ref = new Object() {
      ModuleFactoryID moduleFactoryID = moduleDescriptor.getModuleFactoryID();
    };

    if(ref.moduleFactoryID == null){

      BuildWith buildWithx = aclass.getAnnotation(BuildWith.class);
      ref.moduleFactoryID = ModuleFactoryID.of(buildWithx.value());
    }

    SourceModuleType moduleOrigen = moduleWrapper.getModuleOrigen();
    Optional<ModuleFactory> resultSearchFactory = Optional.empty();


    // Si el origen del modulo es externo al classpath original de JVM
    // buscar en las fabricas montadas
    if(moduleOrigen == SourceModuleType.FILE_SOURCE_MODULE){
      resultSearchFactory = this.moduleClassPathFactories.get(moduleID).stream()
      .filter(moduleFactory -> moduleFactory.getFactoryIdentifier().equals(ref.moduleFactoryID))
      .findFirst();
    }
    if(resultSearchFactory.isPresent()){
      return resultSearchFactory;
    }

    ModuleFactory factory = globalClassPathFactories.get(ref.moduleFactoryID);
    if (factory != null) {
      return Optional.of(factory);
    }
    log.atWarning().log("Factory was specified but this was not found in container");
    return globalClassPathFactories.values().stream()
      .filter(e -> e.isApplicable(aclass))
      .findFirst();


  }



  protected Logger getLogger() {
    return LoggerFactory.logger(this.getClass());
  }

}
