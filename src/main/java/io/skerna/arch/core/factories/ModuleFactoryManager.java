package io.skerna.arch.core.factories;

import io.skerna.arch.core.Flow;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.spi.ModuleFactory;

/**
 * Composite of Module Factories, extend behavior of module Factory and flow composite
 **/
public interface ModuleFactoryManager extends Flow<ModuleFactory, ModuleFactoryManager>, ModuleFactory {

  /**
   * bindManager, this method allow to extension functions access
   *
   * @param moduleManager
   */
  @Override
  void bindManager(ModuleManager moduleManager);


  /**
   * monta todos los factories proporcionado por un modulo wrapper
   * @param moduleWrapper
   */
  void mountFactory(ModuleWrapper moduleWrapper);


  /**
   * desomonta todos los factories proporcionado por un modulo wrapper
   * @param moduleWrapper
   */
  void umountFactory(ModuleWrapper moduleWrapper);


  void beforeInstall(ModuleFactory moduleFactory);

    @Override
    default Module createModule(ModuleWrapper moduleWrapper) throws Exception {
        return null;
    }

    @Override
  default boolean isApplicable(Class<?> target) {
    // Manager is applicable always
    return true;
  }
}
