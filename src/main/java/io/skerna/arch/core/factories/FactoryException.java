package io.skerna.arch.core.factories;

import io.skerna.arch.core.ArchException;
import org.jetbrains.annotations.PropertyKey;

public class FactoryException extends ArchException {
  public FactoryException() {
    super("ERROR_FACTORY_MODULE");
  }

  public FactoryException(@PropertyKey(resourceBundle = "io.skerna.arch.core.messages.i18nexceptions") String errorCode) {
    super(errorCode);
  }
}
