package io.skerna.arch.core.factories;

import io.skerna.arch.core.Module;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.spi.ModuleFactory;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Optional;

/**
 * AbstractModuleFactory, define base strategy to build {@link ModuleFactory}
 *
 */
public abstract class AbstractModuleFactory implements ModuleFactory {
  private static final Logger log = LoggerFactory.logger(AbstractModuleFactory.class);
  protected ModuleManager moduleManager;

  public AbstractModuleFactory() {
  }

  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.moduleManager = moduleManager;
  }

  @Override
  public Module createModule(ModuleWrapper moduleWrapper) throws Exception {
    return (Module) createModuleUsingEmptyConstructor(moduleWrapper);
  }

  /**
   * createModuleUsingEmptyConstructor, crea una intancia de un objecto usando un constructor por defector
   * @param moduleWrapper {@link ModuleWrapper}
   * @return ModuleFactory
   * @throws Exception
   */
  protected Module createModuleUsingEmptyConstructor(ModuleWrapper moduleWrapper) throws Exception {
    String moduleClassName = moduleWrapper.getDescriptor().getModuleClass();
    log.atDebug().log("Create instance for plugin '{}'", moduleClassName);

    Optional<Class<?>> moduleClassResult = moduleWrapper.getModuleClass();

    if (!moduleClassResult.isPresent()) {
      log.atError().log("Imposible load class plugin wrapper returns null value");
      return null;
    }

    Class<?> moduleClass = moduleClassResult.get();

    // once we have the class, we can do some checks on it to ensure
    // that it is a valid implementation of a plugin.
    int modifiers = moduleClass.getModifiers();
    if (Modifier.isAbstract(modifiers) || Modifier.isInterface(modifiers)) {
      log.atError().log("The plugin class '{}' is not valid", moduleClassName);
      return null;
    }
    // create the plugin instance
    try{
      Constructor<?> constructor = moduleClass.getConstructor();
      return (Module) constructor.newInstance();
    }catch (NoSuchMethodException exc){
      FactoryException exception =  new FactoryException();
      exception.appendMessage(exc.getMessage());
      throw  exception;
    }
  }

}
