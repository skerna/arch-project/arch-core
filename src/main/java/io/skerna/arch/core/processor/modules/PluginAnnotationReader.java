package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.BuildWith;
import io.skerna.arch.core.Dependency;
import io.skerna.arch.core.ModuleDefinition;
import io.skerna.arch.core.ModuleType;
import io.skerna.arch.core.util.StringUtils;
import io.skerna.arch.core.vo.ModuleFactoryID;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class PluginAnnotationReader {
  private Map<String, Object> objectMap;

  private PluginAnnotationReader(Map<String, Object> objectMap) {
    this.objectMap = objectMap;
  }

  private PluginAnnotationReader(ModuleDefinition pluginActivador, String className) {
    this.objectMap = readPluginActivadorInMap(pluginActivador, className);
  }

  public static PluginAnnotationReader ofClass(ModuleDefinition pluginActivador, String className) {
    return new PluginAnnotationReader(pluginActivador, className);
  }

  public static PluginAnnotationReader ofMap(Map<String, Object> pluginActivador) {
    return new PluginAnnotationReader(pluginActivador);
  }

  /**
   * Read PluginActivador annotation and set default values in {@link HashMap}
   * why a hasmap ---> mantener bajo acoplamiento entre componentes, map es una estructura generica
   * para compartir datos entre diferentes algoritmos de persistencia y lectura
   *
   * @param values
   * @param cannonicalClassname
   * @return
   */
  public static final Map<String, Object> readPluginActivadorInMap(ModuleDefinition values, String cannonicalClassname) {
    String moduleId = values.moduleId();
    if (moduleId.isEmpty()) {
      moduleId = cannonicalClassname;
    }
    String pluginDescription = values.pluginDescription();
    if (pluginDescription.isEmpty()) {
      pluginDescription = generateDescription(cannonicalClassname);
    }

    String moduleClass = values.moduleClass();

    // if plugin class is empty use class annotated with @Plugin
    if (moduleClass.replaceAll(" ", "").isEmpty()) {
      moduleClass = cannonicalClassname;
    }

    String version = values.version();
    String requires = values.requires();
    String provider = values.provider();
    String licence = values.license();
    ModuleType moduleType = values.moduleType();

    Dependency[] dependecies = values.dependecies();

    HashMap<String, Object> map = new HashMap();
    map.put("moduleId", moduleId);
    if (StringUtils.isNotNullOrEmpty(pluginDescription)) {
      map.put("pluginDescription", pluginDescription);
    }
    if (StringUtils.isNotNullOrEmpty(moduleClass)) {
      map.put("pluginclass", moduleClass);
    }
    if (StringUtils.isNotNullOrEmpty(version)) {
      map.put("pluginVersion", version);
    }
    if (StringUtils.isNotNullOrEmpty(requires)) {
      map.put("pluginRequires", requires);
    }
    if (StringUtils.isNotNullOrEmpty(provider)) {
      map.put("pluginProvider", provider);
    }
    if (StringUtils.isNotNullOrEmpty(licence)) {
      map.put("pluginLicence", licence);
    }
    map.put("moduleType",moduleType.toString());

    if (dependecies.length > 0) {
      StringJoiner joiner = new StringJoiner(",");
      for (Dependency dependecy : dependecies) {
        String dependecyID = dependecy.moduleId();
        String versionDependecy = dependecy.pluginVersionSupport();
        joiner.add(dependecyID + "@" + versionDependecy);
      }
      map.put("pluginDependencies", joiner.toString());
    }
    return map;

  }


  public static final Map<String, Object> readBuildWithInMap(BuildWith buildWith) {
    HashMap<String, Object> map = new HashMap();
    map.put("factory", ModuleFactoryID.of(buildWith.value()));
    return map;
  }

  private static String generateDescription(String cannonicalClassname) {
    StringBuilder builder = new StringBuilder();
    builder.append("Auto meta description:");
    builder.append("Plugin implement fullclassname is " + cannonicalClassname);
    builder.append("Generated at " + ZonedDateTime.now().toString());
    return builder.toString();
  }

  public String moduleId() {
    return (String) objectMap.get("moduleId");
  }

  public String pluginDescription() {
    return (String) objectMap.getOrDefault("pluginDescription", "");
  }

  public String moduleClass() {
    return (String) objectMap.getOrDefault("pluginclass", "");

  }

  public String version() {
    return (String) objectMap.getOrDefault("pluginVersion", "");
  }

  public String requires() {
    return (String) objectMap.getOrDefault("pluginRequires", "");
  }

  public String provider() {
    return (String) objectMap.getOrDefault("pluginProvider", "");
  }

  public String license() {
    return (String) objectMap.getOrDefault("pluginLicence", "");
  }

  public String dependecies() {
    return (String) objectMap.getOrDefault("pluginDependencies", "");
  }

  public String moduleType() {return (String) objectMap.getOrDefault("moduleType","");};
}
