package io.skerna.arch.core.processor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

public class Reporter {
  private boolean enableReporter = false;
  private ProcessingEnvironment processingEnv;

  private Reporter(ProcessingEnvironment processingEnv) {
    this.processingEnv = processingEnv;
  }

  public static final Reporter get(ProcessingEnvironment processingEnv) {
    return new Reporter(processingEnv);
  }

  public void error(String message, Object... args) {
    if(enableReporter) processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String.format(message, args));
  }

  public void error(Element element, String message, Object... args) {
    if(enableReporter)   processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String.format(message, args), element);
  }

  public void info(String message, Object... args) {
    if(enableReporter)  processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, String.format(message, args));
  }

  public void info(Element element, String message, Object... args) {
    if(enableReporter)processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, String.format(message, args), element);
  }

  public void warn(String message, Object... args) {
    if(enableReporter) processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, String.format(message, args));
  }

  public void warn(Element element, String message, Object... args) {
    if(enableReporter) processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, String.format(message, args), element);
  }
}
