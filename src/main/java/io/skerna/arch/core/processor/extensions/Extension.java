package io.skerna.arch.core.processor.extensions;

import java.lang.annotation.Annotation;

public interface Extension<T extends Annotation> {
  /**
   * get Target supported annotations
   * @return
   */
  public abstract Class<? extends T> getTarget();
}
