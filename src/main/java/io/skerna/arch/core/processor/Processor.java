package io.skerna.arch.core.processor;

import io.skerna.arch.core.processor.modules.ModuleProcessor;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;

public abstract class Processor<T extends Annotation> {
  protected ModuleProcessor rootProcessor;

  protected Reporter reporter;

  public Processor(ModuleProcessor rootProcessor) {
    this.rootProcessor= rootProcessor;
    this.reporter = rootProcessor.getReporter();
  }


  abstract public boolean processPlugins(Set<? extends TypeElement> set,
                                         RoundEnvironment roundEnvironment);

  /**
   * Helper method.
   */
  public Filer getFiler() {
    return rootProcessor.getProcessingEnvironment().getFiler();
  }

  public Reporter getReporter() {
    return reporter;
  }

  public ProcessingEnvironment processingEnvironment() {
    return rootProcessor.getProcessingEnvironment();
  }

  public ModuleProcessor getRootProcessor() {
    return rootProcessor;
  }

  public abstract List<String> getTargets();

}
