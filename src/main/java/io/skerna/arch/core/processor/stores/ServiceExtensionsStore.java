/*
package io.skerna.arch.core.processor.stores;

import io.skerna.arch.core.processor.AbstractExtensionStore;
import io.skerna.arch.core.processor.apts.ExtensionsProcessorCoordinator;

import javax.annotation.processing.FilerException;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.*;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class ServiceExtensionsStore extends AbstractExtensionStore {

  public static final String EXTENSIONS_RESOURCE = "META-INF/services";

  private static final Pattern COMMENT = Pattern.compile("#.*");
  private static final Pattern WHITESPACE = Pattern.compile("\\s+");
  private ExtensionsProcessorCoordinator extensionsProcessorCoordinator;

  public ServiceExtensionsStore(ExtensionsProcessorCoordinator processor,String outputDirectory ) {
    super(processor,outputDirectory);
    this.extensionsProcessorCoordinator = processor;
  }


  public static void read(Reader reader, Set<String> entries) throws IOException {
    BufferedReader bufferedReader = new BufferedReader(reader);

    String line;
    while ((line = bufferedReader.readLine()) != null) {
      line = COMMENT.matcher(line).replaceFirst("");
      line = WHITESPACE.matcher(line).replaceAll("");
      if (line.length() > 0) {
        entries.add(line);
      }
    }

    bufferedReader.close();
  }

  @Override
  public Map<String, Set<String>> read() {
    Map<String, Set<String>> extensions = new HashMap<>();

    for (String extensionPoint : extensionsProcessorCoordinator.getExtensions().keySet()) {
      try {
        FileObject file = getFiler().getResource(StandardLocation.CLASS_OUTPUT, "", EXTENSIONS_RESOURCE
          + "/" + extensionPoint);
        Set<String> entries = new HashSet<>();
        read(file.openReader(true), entries);
        extensions.put(extensionPoint, entries);
      } catch (FileNotFoundException | NoSuchFileException e) {
        // doesn't exist, ignore
      } catch (FilerException e) {
        // re-opening the file for reading or after writing is ignorable
      } catch (IOException e) {
        reporter.error(e.getMessage());
      }
    }

    return extensions;
  }

  @Override
  public void write(Map<String, Set<String>> extensions) {
    for (Map.Entry<String, Set<String>> entry : extensions.entrySet()) {
      String extensionPoint = entry.getKey();
      try {
        FileObject file = getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", EXTENSIONS_RESOURCE
          + "/" + extensionPoint);
        BufferedWriter writer = new BufferedWriter(file.openWriter());
        // write header
        writer.newLine();
        // write extensions
        for (String extension : entry.getValue()) {
          writer.write(extension);
          if (!isExtensionOld(extensionPoint, extension)) {
            writer.write(" # extension");
          }
          writer.newLine();
        }
        writer.close();
      } catch (FileNotFoundException e) {
        // it's the first time, create the file
      } catch (FilerException e) {
        // re-opening the file for reading or after writing is ignorable
      } catch (IOException e) {
        reporter.error(e.toString());
      }
    }
  }

  private boolean isExtensionOld(String extensionPoint, String extension) {
    return extensionsProcessorCoordinator.getOldExtensions().containsKey(extensionPoint)
      && extensionsProcessorCoordinator.getOldExtensions().get(extensionPoint).contains(extension);
  }
}
*/
