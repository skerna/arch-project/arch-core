package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.ModuleDefinition;

public class DefaultModuleProcessor extends AbstractModuleProcessor<ModuleDefinition> {

  @Override
  public ModuleDefinition adaptAnnotation(ModuleDefinition annotation) {
    return annotation;
  }

  @Override
  public Class<? extends ModuleDefinition> getTarget() {
    return ModuleDefinition.class;
  }
}
