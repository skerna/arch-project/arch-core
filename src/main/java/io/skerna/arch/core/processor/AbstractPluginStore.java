package io.skerna.arch.core.processor;

import io.skerna.arch.core.loaders.AbstractStore;

import java.util.Map;

public abstract class AbstractPluginStore extends AbstractStore {

  public AbstractPluginStore(Processor processor, String outputDirectory) {
    super(processor,outputDirectory);
  }

  public abstract Map<String, Object> read();

  public abstract void write(Map<String, Object> extensions, String fileName);

}
