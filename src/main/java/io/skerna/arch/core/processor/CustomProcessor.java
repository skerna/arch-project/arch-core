package io.skerna.arch.core.processor;

public interface CustomProcessor {
  String supportedAnnotation();

  Class aliasOf();

}
