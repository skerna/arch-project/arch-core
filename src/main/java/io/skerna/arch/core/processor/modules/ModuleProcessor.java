package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.processor.Reporter;
import io.skerna.arch.core.processor.Processor;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModuleProcessor extends AbstractProcessor {
  public static final String STORAGE_CLASS_NAME = "r.storageClassName";
  private List<Processor> processors = new ArrayList<>();
  private Reporter reporter;
  private ModuleProcessorCordinator moduleProcessorCordinator;


  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    this.reporter = Reporter.get(processingEnv);
    reporter.info("############### %s init processor", ModuleProcessor.class);

    this.moduleProcessorCordinator = new ModuleProcessorCordinator(this);
    processors.add(new ModuleProcessorCordinator(this));
  }

  @Override
  public SourceVersion getSupportedSourceVersion() {
    return SourceVersion.latest();
  }

  @Override
  public Set<String> getSupportedAnnotationTypes() {
    Set<String> annotationTypes = new HashSet<>();
    annotationTypes.addAll(this.moduleProcessorCordinator.getTargets());
    return annotationTypes;
  }

  @Override
  public Set<String> getSupportedOptions() {
    Set<String> options = new HashSet<>();
    options.add(STORAGE_CLASS_NAME);

    return options;
  }

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    reporter.info("JOOOOOOOOOOOOOOOOOOOOOOOOOOODDER");

    if (roundEnv.processingOver()) {
      return false;
    }
    processors.stream().map(abstractOperation -> abstractOperation.processPlugins(annotations, roundEnv)).forEach(o -> {
      System.out.println("Base rootProcessor end return " + o);
    });

    return false;
  }

  public ProcessingEnvironment getProcessingEnvironment() {
    return processingEnv;
  }

  public Reporter getReporter() {
    return reporter;
  }


}
