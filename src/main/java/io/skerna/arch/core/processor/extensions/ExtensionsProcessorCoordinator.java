/*
package io.skerna.arch.core.processor.apts;

import io.skerna.arch.core.processor.AbstractExtensionStore;
import com.arch.gen.serviceloader.ServiceProvider;
import io.skerna.arch.core.ExtensionPoint;
import io.skerna.arch.core.processor.AnnotationProcessor;
import io.skerna.arch.core.processor.Processor;
import io.skerna.arch.core.processor.stores.ServiceExtensionsStore;
import io.skerna.arch.core.util.ClassUtils;
import io.skerna.arch.core.util.IteratorUtils;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;

@ServiceProvider(Processor.class)
public class ExtensionsProcessorCoordinator extends Processor<Annotation> {
  public static final String STORAGE_CLASS_NAME = "reactor.storageClassName";
  private Map<String, Set<String>> extensions = new HashMap<>(); // the key is the extension point
  private Map<String, Set<String>> oldExtensions = new HashMap<>(); // the key is the extension point
  private Set<AbstractExtensionProcessor> abstractExtensionProcessors = new HashSet<>();
  private List<String> targetsAnnotations = new ArrayList<>();

  private AbstractExtensionStore storage;
  private ProcessingEnvironment processingEnv;

  public ExtensionsProcessorCoordinator(AnnotationProcessor rootProcessor) {
    super(rootProcessor);
    this.rootProcessor = rootProcessor;
    this.storage= createStorage();
    ServiceLoader<AbstractExtensionProcessor> serviceLoaderResult = ServiceLoader.load(AbstractExtensionProcessor.class,AnnotationProcessor.class.getClassLoader());
    Set<AbstractExtensionProcessor> services = IteratorUtils.toSet(serviceLoaderResult.iterator());
    this.abstractExtensionProcessors = services;
    this.targetsAnnotations = abstractExtensionProcessors.stream()
      .map(e->e.getTarget().getName())
      .collect(Collectors.toList());

  }


  @Override
  public boolean processPlugins(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
    for (AbstractExtensionProcessor aliasProcessor : abstractExtensionProcessors) {
      Class<? extends Annotation> targetAnnotation = aliasProcessor.getTarget();
      reporter.info("Processing @%s", targetAnnotation);
      for (Element element : roundEnvironment.getElementsAnnotatedWith(targetAnnotation)) {
        // check if @Extension is put on class and not on method or constructor
        if (!(element instanceof TypeElement)) {
          continue;
        }

        // check if class extends/implements an extension point
        if (!isExtension(element.asType())) {
          continue;
        }


        TypeElement extensionElement = (TypeElement) element;
//            Extension annotation = element.getAnnotation(Extension.class);
        List<TypeElement> extensionPointElements = findExtensionPoints(extensionElement,targetAnnotation);
        if (extensionPointElements.isEmpty()) {
          // TODO throw error ?
          continue;
        }

        String extension = getBinaryName(extensionElement);
        for (TypeElement extensionPointElement : extensionPointElements) {
          String extensionPoint = getBinaryName(extensionPointElement);
          Set<String> extensionPoints = extensions.get(extensionPoint);
          if (extensionPoints == null) {
            extensionPoints = new TreeSet<>();
            extensions.put(extensionPoint, extensionPoints);
          }
          extensionPoints.add(extension);
        }
      }
    }

    // read old extensions
    oldExtensions = storage.read();
    for (Map.Entry<String, Set<String>> entry : oldExtensions.entrySet()) {
      String extensionPoint = entry.getKey();
      if (extensions.containsKey(extensionPoint)) {
        extensions.get(extensionPoint).addAll(entry.getValue());
      } else {
        extensions.put(extensionPoint, entry.getValue());
      }
    }

    // write extensions
    storage.write(extensions);

    return false;
  }

  protected String generatePathFile(Class moduleClass){
    String name = moduleClass.getCanonicalName();
    return name;
  }

  @Override
  public List<String> getTargets() {
    return targetsAnnotations;
  }



  private List<TypeElement> findExtensionPoints(TypeElement extensionElement,Class<? extends Annotation> targetAnnotation) {
    List<TypeElement> extensionPointElements = new ArrayList<>();

    // use extension points, that were explicitly set in the extension annotation
    AnnotationValue annotatedExtensionPoints = ClassUtils.getAnnotationValue(extensionElement, targetAnnotation, "points");
    List<? extends AnnotationValue> extensionPointClasses = (annotatedExtensionPoints != null) ?
      (List<? extends AnnotationValue>) annotatedExtensionPoints.getValue() :
      null;
    if (extensionPointClasses != null && !extensionPointClasses.isEmpty()) {
      for (AnnotationValue extensionPointClass : extensionPointClasses) {
        String extensionPointClassName = extensionPointClass.getValue().toString();
        TypeElement extensionPointElement = processingEnv.getElementUtils().getTypeElement(extensionPointClassName);
        extensionPointElements.add(extensionPointElement);
      }
    }
    // detect extension points automatically, if they are not explicitly configured (default behaviour)
    else {
      // search in interfaces
      for (TypeMirror item : extensionElement.getInterfaces()) {
        boolean isExtensionPoint = processingEnv.getTypeUtils().isSubtype(item, getExtensionPointType());
        if (isExtensionPoint) {
          TypeElement extensionPointElement = (TypeElement) ((DeclaredType) item).asElement();
          extensionPointElements.add(extensionPointElement);
        }
      }

      // search in superclass
      TypeMirror superclass = extensionElement.getSuperclass();
      if (superclass.getKind() != TypeKind.NONE) {
        boolean isExtensionPoint = processingEnv.getTypeUtils().isSubtype(superclass, getExtensionPointType());
        if (isExtensionPoint) {
          TypeElement extensionPointElement = (TypeElement) ((DeclaredType) superclass).asElement();
          extensionPointElements.add(extensionPointElement);
        }
      }
    }

    return extensionPointElements;
  }

  public Map<String, Set<String>> getExtensions() {
    return extensions;
  }

  public Map<String, Set<String>> getOldExtensions() {
    return oldExtensions;
  }

  private TypeMirror getExtensionPointType() {
    return processingEnv.getElementUtils().getTypeElement(ExtensionPoint.class.getName()).asType();
  }

  public ProcessingEnvironment getProcessingEnvironment() {
    return processingEnv;
  }

  public String getBinaryName(TypeElement element) {
    return processingEnv.getElementUtils().getBinaryName(element).toString();
  }


  private boolean isExtension(TypeMirror typeMirror) {
    return processingEnv.getTypeUtils().isAssignable(typeMirror, getExtensionPointType());
  }

  @SuppressWarnings("unchecked")
  private AbstractExtensionStore createStorage() {
    AbstractExtensionStore storage = null;

    // search in processing options
    String storageClassName = processingEnv.getOptions().get(STORAGE_CLASS_NAME);
    if (storageClassName == null) {
      // search in system properties
      storageClassName = System.getProperty(STORAGE_CLASS_NAME);
    }

    if (storageClassName != null) {
      // use reflection to create the storage instance
      try {
        Class storageClass = getClass().getClassLoader().loadClass(storageClassName);
        Constructor constructor = storageClass.getConstructor(AnnotationProcessor.class);
        storage = (AbstractExtensionStore) constructor.newInstance(this);
      } catch (Exception e) {
        reporter.error(e.getMessage());
      }
    }

    if (storage == null) {
      // default storage
      storage = new ServiceExtensionsStore(this);
    }

    return storage;
  }

}
*/
