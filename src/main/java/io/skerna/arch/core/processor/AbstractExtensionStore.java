package io.skerna.arch.core.processor;

import io.skerna.arch.core.loaders.AbstractStore;

import java.util.Map;
import java.util.Set;

public abstract class AbstractExtensionStore extends AbstractStore {

  public AbstractExtensionStore(Processor processor, String outputDirectory ) {
    super(processor,outputDirectory);
  }

  public abstract Map<String, Set<String>> read();

  public abstract void write(Map<String, Set<String>> extensions);

}
