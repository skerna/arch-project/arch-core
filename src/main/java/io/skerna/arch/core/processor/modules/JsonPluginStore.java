package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.processor.AbstractPluginStore;
import io.skerna.arch.core.processor.Processor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class JsonPluginStore extends AbstractPluginStore {
  private static final Pattern COMMENT = Pattern.compile("#.*");
  private static final Pattern WHITESPACE = Pattern.compile("\\s+");

  public JsonPluginStore(Processor processor, String outputDirectory) {
    super(processor,outputDirectory);
  }

  public static void read(Reader reader, Set<String> entries) throws IOException {
    BufferedReader bufferedReader = new BufferedReader(reader);
    JSONTokener jsonTokener = new JSONTokener(reader);
    JSONArray plugins = new JSONArray();
    String line;
    while ((line = bufferedReader.readLine()) != null) {
      line = COMMENT.matcher(line).replaceFirst("");
      line = WHITESPACE.matcher(line).replaceAll("");
      if (line.length() > 0) {
        entries.add(line);
      }
    }

    bufferedReader.close();
  }

  @Override
  public Map<String, Object> read() {
    return null;
  }

  @Override
  public void write(Map<String, Object> extensions, String filename) {
    reporter.info("Write all map size %s", extensions.size());
    JSONArray arrayPlugins = new JSONArray();
    BufferedWriter writer = null;
    try {
      String uri = outputDirectory + "/" +filename;
      FileObject file = getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", uri);
      writer = new BufferedWriter(file.openWriter());
      JSONObject pluginEntry = new JSONObject(extensions);

      pluginEntry.write(writer, 3, 2);
      writer.close();
    } catch (Exception ex) {
      reporter.error(ex.toString());
    }
  }
}
