package io.skerna.arch.core.processor.extensions;

import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;

public abstract class AbstractExtensionProcessor<T extends Annotation> implements Extension<T> {
  /**
   * onCreate on create mehtod hook
   */
  public void onCreate(){}

  /**
   * Adapt interface to element Type
   * @param typeElement
   * @return
   */
  public abstract Extension adaptValue(TypeElement typeElement);


}
