package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.ModuleDefinition;
import io.skerna.arch.core.processor.extensions.Extension;

import java.lang.annotation.Annotation;

public abstract class AbstractModuleProcessor<T extends Annotation> implements Extension<T> {
  /**
   * onCreate on create mehtod hook
   */
  public void onCreate(){}


  public abstract ModuleDefinition adaptAnnotation(T annotation);


}
