package io.skerna.arch.core.processor.modules;

import io.skerna.arch.core.ModuleDefinition;
import io.skerna.arch.core.processor.AbstractPluginStore;
import io.skerna.arch.core.processor.Processor;
import io.skerna.arch.core.util.IteratorUtils;
import io.skerna.arch.core.Constants;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.Collectors;

public class ModuleProcessorCordinator extends Processor<Annotation> {
  private AbstractPluginStore abstractPluginStorage;
  private Set<AbstractModuleProcessor> abstractModuleProcessors;
  private List<String> targetsAnnotations;

  public ModuleProcessorCordinator(ModuleProcessor rootProcessor) {
    super(rootProcessor);
    this.abstractPluginStorage = createPluginStorage();
    ServiceLoader<AbstractModuleProcessor> serviceLoaderResult = ServiceLoader.load(AbstractModuleProcessor.class,ModuleProcessor.class.getClassLoader());
    Set<AbstractModuleProcessor> services = IteratorUtils.toSet(serviceLoaderResult.iterator());
    this.abstractModuleProcessors = services;
    this.targetsAnnotations = abstractModuleProcessors.stream()
      .map(e->e.getTarget().getName())
      .collect(Collectors.toList());

    reporter.info("# Total module Adapters %s",abstractModuleProcessors.size());
  }

  /**
   * Plugin-Class: x.y.Z
   * Plugin-Dependencies: x, y, z
   * Plugin-Id: welcome-plugin
   * Plugin-Provider: example
   * Plugin-Version: 0.0.1
   *
   * @param set
   * @param roundEnvironment
   * @return
   */
  public boolean processPlugins(Set<? extends TypeElement> set,
                                RoundEnvironment roundEnvironment) {
    Map<String, Map<String, Object>> plugins = new HashMap<>(); // the key is the extension point

    for (AbstractModuleProcessor moduleAnnotationAlias : abstractModuleProcessors) {
      Class<? extends Annotation> targetAnnotation  = moduleAnnotationAlias.getTarget();
      Set<? extends Element> elementsAnnotatedWithPlugin = roundEnvironment.getElementsAnnotatedWith(targetAnnotation);
      reporter.info("Total elements annotated with @%s %s", targetAnnotation, elementsAnnotatedWithPlugin.size());

      /**if(elementsAnnotatedWithPlugin.size() > 1){
       StringBuilder stringBuilder =  new StringBuilder();
       stringBuilder.append("Only allow a plugin per jar \n ");
       for (Element element : elementsAnnotatedWithPlugin) {
       stringBuilder.append("Find extension on " + element.getSimpleName() + "\n");
       }
       reporter.error(stringBuilder.toString());
       }**/
      for (Element annotatedElement : elementsAnnotatedWithPlugin) {
        if (annotatedElement.getKind() != ElementKind.CLASS) {
          reporter.error("Only Class can be annotated with @Plugin", annotatedElement);
          return false;
        }
        TypeElement typeElement = (TypeElement) annotatedElement;
        String cannonicalClassname = typeElement.getQualifiedName().toString();
        Annotation annotation = typeElement.getAnnotation(targetAnnotation);
        ModuleDefinition activador = moduleAnnotationAlias.adaptAnnotation(annotation);
        Map<String, Object> map = PluginAnnotationReader.readPluginActivadorInMap(activador, cannonicalClassname);

        plugins.put(cannonicalClassname, map);

        abstractPluginStorage.write(map,cannonicalClassname);
      }


    }
    /*if (!plugins.isEmpty()) {
      //abstractPluginStorage.write(plugins);
    } else {
      reporter.info("No modules annotated with", ModuleDefinition.class);
    }
    return false;*/
    return true;
  }

  @Override
  public List<String> getTargets() {
    return targetsAnnotations;
  }

  private AbstractPluginStore createPluginStorage() {
    // default storage
    return new JsonPluginStore(this, Constants.DEFAULT_OUTPUT_DIRECTORY);
  }

}
