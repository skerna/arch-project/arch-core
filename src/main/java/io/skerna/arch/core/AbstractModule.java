package io.skerna.arch.core;

import io.skerna.arch.core.runners.GenericModuleRunner;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.factories.DefaultModuleFactory;

@BuildWith(DefaultModuleFactory.class)
@RunWith(GenericModuleRunner.class)
public abstract class AbstractModule implements Module {
  protected ModuleWrapper moduleWrapper;
  protected ModuleManager manager;
  @Override
  public ModuleWrapper getWrapper() {
    return this.moduleWrapper;
  }

  @Override
  public void injectWrapper(ModuleWrapper pluginWrapper) {
    this.moduleWrapper= pluginWrapper;
    this.manager = pluginWrapper.getModuleManager();
  }


  /**
   * This method is called by the application when the moduleInstance is started.
   * See {@link ModuleManager#startModule(ModuleID)}.
   */
  public void start() throws Exception {
  }

  /**
   * This method is called by the application when the moduleInstance is stopped.
   * See {@link ModuleManager#stopModule(ModuleID)}.
   */
  public void stop() throws Exception {
  }

  @Override
  public void delete() throws ReactorException {

  }
}
