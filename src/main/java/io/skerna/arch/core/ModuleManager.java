package io.skerna.arch.core;

import io.skerna.arch.core.factories.ExtensionFactoryManager;
import io.skerna.arch.core.factories.ModuleFactoryManager;
import io.skerna.arch.core.loaders.PluginLoaderManager;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.vo.ChangeStateResult;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceModule;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Provides the functionality for moduleInstance management such as load,
 * start and stop the plugins.
 * <p>
 * *
 */
public interface ModuleManager extends Observable {

  /**
   * Retrieves all plugins.
   */
  List<ModuleWrapper> getModules();

  /**
   * Retrieves all plugins with this state.
   */
  List<ModuleWrapper> getModules(ModuleState pluginState);

  /**
   * Retrieves all resolved plugins (with resolved dependency).
   */
  List<ModuleWrapper> getResolvedModules();

  /**
   * Retrieves all unresolved plugins (with unresolved dependency).
   */
  List<ModuleWrapper> getUnresolvedModules();

  /**
   * Retrieves all started plugins.
   */
  List<ModuleWrapper> getStartedModules();

  /**
   * Retrieves the moduleInstance with this id, or null if the moduleInstance does not exist.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return A PluginWrapper object for this moduleInstance, or null if it does not exist.
   */
  ModuleWrapper getModule(ModuleID moduleId);

  /**
   * Load plugins.
   */
  void loadModules();

  /**
   * Load a moduleInstance.
   *
   * @param pluginPath the moduleInstance location
   * @return the moduleId of the installed moduleInstance as specified in
   * its {@linkplain ModuleDescriptor metadata}; or {@code null}
   */
  Set<ModuleID> loadModules(SourceModule sourceModule);


  /**
   * Start all active plugins.
   */
  CompletableFuture<Void> startModules();

  /**
   * Start the specified moduleInstance and its dependencies.
   *
   * @param moduleId
   * @return the moduleInstance state
   */
  CompletableFuture<ChangeStateResult> startModule(ModuleID moduleId);

  /**
   * Stop all active plugins.
   */
  void stopPlugins();


  /**
   * Stop the specified moduleInstance and its dependencies.
   *
   * @param moduleId
   * @return the moduleInstance state
   */
  CompletableFuture<ChangeStateResult> stopModule(ModuleID moduleId);

  /**
   * Unload a moduleInstance.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if the moduleInstance was unloaded
   */
  CompletableFuture<Boolean> unloadModule(ModuleID moduleId);

  /**
   * Disables a moduleInstance from being loaded.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if moduleInstance is disabled
   */
  CompletableFuture<Boolean> disableModule(ModuleID moduleId);

  /**
   * Enables a moduleInstance that has previously been disabled.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if moduleInstance is enabled
   */
  boolean enableModule(ModuleID moduleId);

  /**
   * Deletes a moduleInstance.
   *
   * @param moduleId the unique moduleInstance identifier, specified in its metadata
   * @return true if the moduleInstance was deleted
   */
  CompletableFuture<Boolean> deleteModule(ModuleID moduleId);


  CompletableFuture<Boolean> deleteModule(SourceModule sourceModule);

  List<Class<?>> getExtensionClasses(ModuleID moduleId);

  <T> List<Class<T>> getExtensionClasses(Class<T> type);

  <T> List<Class<T>> getExtensionClasses(Class<T> type, ModuleID moduleId);

  <T> List<T> getExtensions(Class<T> type);

  <T> List<T> getExtensions(Class<T> type, ModuleID moduleId);

  List getExtensions(ModuleID moduleId);

  Set<String> getExtensionClassNames(ModuleID moduleId);

  ExtensionFactoryManager getExtensionFactory();

  ModuleFactoryManager getPluginFactory();

  ModuleRunner getPluginRunner();

  PluginLoaderManager getPluginLoaderManager();

  /**
   * The runtime mode. Must currently be either DEVELOPMENT or DEPLOYMENT.
   */
  RuntimeMode getRuntimeMode();

  /**
   * Retrieves the {@link ModuleWrapper} that loaded the given class 'clazz'.
   *
   * @return
   */
  Optional<ModuleWrapper> whichModule(Class<?> clazz);

  Optional<ModuleWrapper> whichModule(String clazzName);

  /**
   * Returns the system version.
   *
   * @return the system version
   */
  String getSystemVersion();

  /**
   * Set the system version.  This is used to compare against the moduleInstance
   * requires attribute.  The default system version is 0.0.0 which
   * disables all version checking.
   *
   * @param version
   * @default 0.0.0
   */
  void setSystemVersion(String version);

  /**
   * Gets the path of the folder where plugins are installed
   *
   * @return Path of plugins root
   */
  Path getPluginsRoot();

  VersionManager getVersionManager();


}
