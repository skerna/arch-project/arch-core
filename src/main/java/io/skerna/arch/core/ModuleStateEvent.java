package io.skerna.arch.core;

import java.util.EventObject;

public class ModuleStateEvent extends EventObject {

  private ModuleWrapper plugin;
  private ModuleState oldState;

  public ModuleStateEvent(ModuleManager source, ModuleWrapper plugin, ModuleState oldState) {
    super(source);

    this.plugin = plugin;
    this.oldState = oldState;
  }

  @Override
  public ModuleManager getSource() {
    return (ModuleManager) super.getSource();
  }

  public ModuleWrapper getPlugin() {
    return plugin;
  }

  public ModuleState getPluginState() {
    return plugin.getModuleState();
  }

  public ModuleState getOldState() {
    return oldState;
  }

  @Override
  public String toString() {
    return "PluginStateEvent [moduleInstance=" + plugin.getPluginId() +
      ", newState=" + getPluginState() +
      ", oldState=" + oldState +
      ']';
  }

}
