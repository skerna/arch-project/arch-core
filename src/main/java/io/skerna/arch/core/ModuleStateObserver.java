package io.skerna.arch.core;

import java.util.EventListener;

/**
 * PluginStateListener defines the interface for an object that listens to moduleInstance state changes.
 * <p>
 */
public interface ModuleStateObserver extends EventListener {

  /**
   * Invoked when a moduleInstance's state (for example DISABLED, STARTED) is changed.
   */
  void pluginStateChanged(ModuleStateEvent event);

}
