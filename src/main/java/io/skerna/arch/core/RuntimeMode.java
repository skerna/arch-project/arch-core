package io.skerna.arch.core;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;


public enum RuntimeMode {

  DEVELOPMENT("development", "dev"), // development
  DEPLOYMENT("deployment", "prod"); // deployment

  private static final Map<String, RuntimeMode> map = new HashMap<>();

  static {
    for (RuntimeMode mode : RuntimeMode.values()) {
      map.put(mode.name, mode);
      for (String alias : mode.aliases) {
        map.put(alias, mode);
      }
    }
  }

  private final String name;
  private final String[] aliases;

  private RuntimeMode(final String name, final String... aliases) {
    this.name = name;
    this.aliases = aliases;
  }

  public static RuntimeMode byName(String name) {
    if (map.containsKey(name)) {
      return map.get(name);
    }

    throw new NoSuchElementException("Cannot found PF4J runtime mode with name '" + name + "'." +
      "Must be one value from '" + map.keySet() + ".");
  }

  @Override
  public String toString() {
    return name;
  }

}
