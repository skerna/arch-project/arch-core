package io.skerna.arch.core;


import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.vo.ModuleID;

/**
 * This class will be extended by all plugins and
 * serve as the common class between a moduleInstance and the application.
 * <p>
 * Since Runner moduleInstance is extensible using {@link ModuleRunner}
 * only method required is getWrapper() is Plugin context
 * <p>
 * <p>
 * *
 */
public interface Module {

  ModuleWrapper getWrapper();

  /**
   * Initialise the Plugin with the Wrapper of moduleInstance
   * <p>
   * This method is called by Plugin Factory o Verticle Factory when the instance is deployed with several instances.
   * You do not call it yourself.
   *
   * @param pluginWrapper the {@link ModuleWrapper} instance
   */
  void injectWrapper(ModuleWrapper pluginWrapper);

  /**
   * This method is called by the application when the moduleInstance is deleted.
   * See {@link ModuleManager#deleteModule(ModuleID)}.
   */
  void delete() throws ReactorException;

}
