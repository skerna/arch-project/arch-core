package io.skerna.arch.core;

import io.skerna.arch.core.runners.GenericModuleRunner;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.factories.DefaultModuleFactory;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

/**
 * This class will be extended by all plugins and
 * serve as the common class between a moduleInstance and the application.
 * <p>
 */
@RunWith(GenericModuleRunner.class)
@BuildWith(DefaultModuleFactory.class)
public class GenericModule implements Module {

  /**
   * Makes logging service available for descending classes.
   */
  protected final Logger log = LoggerFactory.logger(getClass());


  /**
   * Wrapper of the moduleInstance.
   */
  protected ModuleWrapper wrapper;

  /**
   * Retrieves the wrapper of this plug-in.
   */
  public final ModuleWrapper getWrapper() {
    return wrapper;
  }

  /**
   * inject module wrapper to be used by moduleInstance manager for moduleInstance instantiation.
   * Your plugins have to provide constructor with this exact signature to
   * be successfully loaded by manager.
   */
  @Override
  public void injectWrapper(ModuleWrapper pluginWrapper) {
    wrapper = pluginWrapper;
  }

  /**
   * This method is called by the application when the moduleInstance is started.
   * See {@link ModuleManager#startModule(ModuleID)}.
   */
  public void start() throws ReactorException {
  }

  /**
   * This method is called by the application when the moduleInstance is stopped.
   * See {@link ModuleManager#stopModule(ModuleID)}.
   */
  public void stop() throws ReactorException {
  }

  /**
   * This method is called by the application when the moduleInstance is deleted.
   * See {@link ModuleManager#deleteModule(ModuleID)}.
   */
  @Override
  public void delete() throws ReactorException {
  }

}
