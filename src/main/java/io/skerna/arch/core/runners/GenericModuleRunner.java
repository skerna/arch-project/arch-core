package io.skerna.arch.core.runners;

import io.skerna.arch.core.AbstractModule;
import io.skerna.arch.core.GenericModule;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.concurrent.CompletableFuture;

public class GenericModuleRunner implements ModuleRunner {
  private Logger logger = LoggerFactory.logger(GenericModule.class);

  /**
   * bindManager, this method allow to extension functions access
   *
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {

  }
  @Override
  public CompletableFuture<Boolean> startModule(ModuleWrapper pluginWrapper) {
    logger.atDebug().log("task:[GENERIC_RUNNER] Verticle Deploy plugin with ID: {} ", pluginWrapper.getDescriptor().getModuleID());
    CompletableFuture<Boolean> future = new CompletableFuture<>();
    try {
      AbstractModule plugin = (AbstractModule) pluginWrapper.getModuleInstance();
      plugin.start();
      future.complete(true);
    } catch (Exception e) {
      e.printStackTrace();
      future.completeExceptionally(e);
    }
    return future;
  }

  @Override
  public CompletableFuture<Boolean> stopModule(ModuleWrapper pluginWrapper) {
    logger.atDebug().log("task:[STOP_RUNNER] Undeploy plugin: " + pluginWrapper.getDescriptor().getModuleID());
    CompletableFuture<Boolean> future = new CompletableFuture<>();
    try {
      AbstractModule plugin = (AbstractModule) pluginWrapper.getModuleInstance();
      plugin.stop();
      future.complete(true);
    } catch (Exception ex) {
      future.completeExceptionally(ex);
    }
    return future;
  }

  @Override
  public Boolean isApplicable(Class targetPlugin) {
    return AbstractModule.class.isAssignableFrom(targetPlugin);
  }


}
