package io.skerna.arch.core.runners;

import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ModuleManager;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.ReactorException;
import io.skerna.arch.core.spi.ModuleRunner;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.ModuleRunnerID;
import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 *
 */
public class RunnerManager implements ModuleRunner {
  private static final Logger log = LoggerFactory.logger(RunnerManager.class);

  private ModuleManager pluginManager;
  private Boolean allowAutoScan = true;
  private List<String> excludeRunners;
  private Map<ModuleRunnerID, ModuleRunner> runnermap = new HashMap();

  public RunnerManager(Boolean allowAutoScan) {
    this.allowAutoScan = allowAutoScan;
    this.excludeRunners = new ArrayList<>();
  }


  public RunnerManager(Boolean allowAutoScan, List<String> excludeRunners) {
    this.allowAutoScan = allowAutoScan;
    this.excludeRunners = excludeRunners;
  }

  /**
   * bindManager, this method allow to extension functions access
   *
   * @param moduleManager
   */
  @Override
  public void bindManager(ModuleManager moduleManager) {
    this.pluginManager = moduleManager;
    this.init();
    Set<ModuleRunner> moduleRunners = createPluginRunners();
    moduleRunners.forEach(this::addPluginRunner);
    log.atDebug().log("All runnners registered  {} " , runnermap.size());
  }

  protected void init() {
    boolean hasExcludedRunners = true;
    if(excludeRunners == null || excludeRunners.isEmpty()){
      hasExcludedRunners = false;
    }
    log.atDebug().log("task:[COMPOUNDRUNNER] autoscan {} ", allowAutoScan);
    if (allowAutoScan) {
      try {
        ServiceLoader<ModuleRunner> services = ServiceLoader.load(ModuleRunner.class);
        for (ModuleRunner service : services) {
          if(hasExcludedRunners && excludeRunners.contains(service.getClass())){
              log.atDebug().log("Exclude  service runner of type {}", service.getClass().getCanonicalName());
              continue;
          }
          log.atDebug().log("Add new service runner of type {}", service.getClass().getCanonicalName());
          service.bindManager(pluginManager);
          ModuleRunnerID moduleRunnerID = service.getRunnerIdentifier();
          this.runnermap.put(moduleRunnerID,service);
        }
      } catch (ServiceConfigurationError ex) {
        log.atError().log("Imposible configurate services", ex);
      }
    }
  }

  @Override
  public CompletableFuture<Boolean> startModule(ModuleWrapper pluginWrapper) {
    log.atDebug().log("task:[RUN_MANAGER] Executing Plugin Runner operation startModule (start)");
    CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();

    Optional<ModuleRunner> resultFindRunner = findRunner(pluginWrapper);
    if (!resultFindRunner.isPresent()) {
      log.atError().log("task:[RUN_MANAGER] Impossible find runner for plugin {}", pluginWrapper);
      completableFuture.completeExceptionally(new ReactorException("Cannot find handler runner for run this plugin  " + pluginWrapper));
      return completableFuture;
    }
    try{
      ModuleRunner moduleRunner = resultFindRunner.get();

      log.atDebug().log("task:[RUN_MANAGER] One Plugin Runnner was found {} using it for {}", moduleRunner.getClass(), pluginWrapper);

      ModuleID pluginID = pluginWrapper.getPluginId();

      moduleRunner.startModule(pluginWrapper).whenComplete((aBoolean, throwable) -> {
        if (throwable != null) {
          log.atError().log("task:[RUN_MANAGER] Delegate runner ended execution with atError().log ", throwable);
          completableFuture.completeExceptionally(throwable);
          return;
        }
        log.atDebug().log("task:[RUN_MANAGER] Successfull plugin Run operation for pluginID {} of class {}", pluginID, pluginWrapper);
        completableFuture.complete(true);
      });

    }catch (Exception ex){
      completableFuture.completeExceptionally(ex);
    }

    return completableFuture;
  }

  @Override
  public CompletableFuture<Boolean> stopModule(ModuleWrapper pluginWrapper) {
    log.atDebug().log("task:[STOP_COMPOUND] Executing Plugin Runner operation stopModule (stop)");
    CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();

    Optional<ModuleRunner> findRunnerResult = findRunner(pluginWrapper);
    if (!findRunnerResult.isPresent()) {
      log.atError().log("task:[STOP_COMPOUND] Impossible find runner for plugin {}", pluginWrapper);
      completableFuture.completeExceptionally(new ReactorException("Cannot find handler runner for run this plugin  " + pluginWrapper));
      return completableFuture;
    }
    ModuleRunner runner = findRunnerResult.get();
    log.atDebug().log("task:[STOP_COMPOUND] One Plugin Runnner was found {} using it", runner.getClass());

    runner.stopModule(pluginWrapper).whenComplete((aBoolean, throwable) -> {
      if (throwable != null) {
        log.atDebug().log("task:[STOP_COMPOUND] Delegate runner ended execution with atError().log ", throwable);
        completableFuture.completeExceptionally(throwable);
        return;
      }
      log.atDebug().log("task:[STOP_COMPOUND] Successfull plugin Run operation for pluginID {}", pluginWrapper.getPluginId());
      completableFuture.complete(true);
    });

    return completableFuture;
  }


  // In this point Plugin is already initialized, getModule() invocation is cheap
  private Optional<ModuleRunner> findRunner(ModuleWrapper moduleWrapper) {
    ModuleDescriptor moduleDescriptor = moduleWrapper.getDescriptor();

    ModuleRunnerID moduleRunnerClass = moduleDescriptor.getModuleRunnerID();
    ModuleRunner moduleRunner = runnermap.get(moduleRunnerClass);
    if(moduleRunner == null){
      Class<?> moduleClass = moduleWrapper.getModuleClass().get();
      log.atDebug().log("task:[RUN_MANAGER] Search runner strategy total registered: {}", runnermap.size());
      Optional<ModuleRunner> moduleResultFind =  runnermap.values().stream()
        .filter(pluginRunner -> {
          Boolean isApplicable = pluginRunner.isApplicable(moduleClass);
          log.atDebug().log("task:[RUN_MANAGER] {} Plugin runner is applicable {}: {}", pluginRunner.getClass(), moduleClass, isApplicable);
          return isApplicable;
        })
        .findFirst();
      if(moduleResultFind.isPresent()){
        moduleRunner = moduleResultFind.get();
      }
    }
    return Optional.of(moduleRunner);


  }

  @Override
  public void addPluginRunner(ModuleRunner moduleRunner) {
    Objects.requireNonNull(moduleRunner, "nulls not allowed");
    ModuleRunnerID runnerKey = moduleRunner.getRunnerIdentifier();
    runnermap.putIfAbsent(runnerKey,moduleRunner);
  }

  @Override
  public void removePluginRunner(ModuleRunner moduleRunner) {
    ModuleRunnerID runnerkey = moduleRunner.getRunnerIdentifier();
    this.runnermap.remove(runnerkey);
  }

  @Override
  public int counterFactories() {
    return runnermap.size();
  }

  protected Set<ModuleRunner> createPluginRunners() {
    GenericModuleRunner genericModuleRunner = new GenericModuleRunner();

    Set runners = new HashSet();
    runners.add(genericModuleRunner);
    return runners;
  }

  @Override
  public Boolean isApplicable(Class targetPlugin) {
    return true;
  }

}
