package io.skerna.arch.core;

public interface Executable {
  void start();

  void stop();
}
