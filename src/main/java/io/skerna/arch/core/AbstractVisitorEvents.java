package io.skerna.arch.core;

import io.skerna.common.logger.Logger;
import io.skerna.common.logger.LoggerFactory;

public abstract class AbstractVisitorEvents implements VisitorEvents, ModuleStateObserver {
  private static final Logger log = LoggerFactory.logger(AbstractVisitorEvents.class);

  @Override
  public void pluginStateChanged(ModuleStateEvent event) {
    ModuleState eventType = event.getPluginState();

    log.atDebug().log("Event Fired to AbstractVisitorEvents with type ,[{}] ", eventType);

    switch (eventType) {
      case INSTALLED:
        onInstalled(event);
        break;
      case UNINSTALLED:
        onUninstalled(event);
        break;
      case CREATED:
        onCreated(event);
        break;
      case DISABLED:
        onDisabled(event);
        break;
      case RESOLVED:
        onResolved(event);
        break;
      case STARTED:
        onStarted(event);
        break;
      case STOPPED:
        onStopped(event);
        break;
    }
  }

  @Override
  public void onInstalled(ModuleStateEvent event) {

  }

  @Override
  public void onUninstalled(ModuleStateEvent event) {

  }

  @Override
  public void onCreated(ModuleStateEvent event) {

  }

  @Override
  public void onDisabled(ModuleStateEvent event) {

  }

  @Override
  public void onResolved(ModuleStateEvent event) {

  }

  @Override
  public void onStarted(ModuleStateEvent event) {

  }

  @Override
  public void onStopped(ModuleStateEvent event) {

  }

}
