package io.skerna.arch.core;

import org.atteo.classindex.IndexAnnotated;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@Target(TYPE)
@Inherited
@Documented
@IndexAnnotated
public @interface PreferredFactory {
  Class<ModuleDefinition> value();
}
