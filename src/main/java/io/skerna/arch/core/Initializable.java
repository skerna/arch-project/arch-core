package io.skerna.arch.core;

public interface Initializable {
  /**
   * bindManager, this method allow to extension functions access
   * @param moduleManager
   */
  void bindManager(ModuleManager moduleManager);
}
