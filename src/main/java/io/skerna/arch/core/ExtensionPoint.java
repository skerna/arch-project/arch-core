package io.skerna.arch.core;

/**
 * An extension point is a formal declaration in a moduleInstance (or in application API) where customization is allowed.
 * <p>
 * *
 */
public interface ExtensionPoint {
}
