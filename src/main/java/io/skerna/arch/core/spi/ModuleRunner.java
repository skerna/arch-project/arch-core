package io.skerna.arch.core.spi;

import io.skerna.arch.core.Initializable;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.vo.ModuleRunnerID;

import java.util.concurrent.CompletableFuture;

public interface ModuleRunner extends Initializable {

  CompletableFuture<Boolean> startModule(ModuleWrapper pluginWrapper);

  CompletableFuture<Boolean> stopModule(ModuleWrapper pluginWrapper);

  /**
   * Compount a Factory with several strategies
   *
   * @param pluginRunner
   */
  default void addPluginRunner(ModuleRunner pluginRunner) {
    throw new IllegalStateException("this implementation not allow composed Plugin Factory");
  }

  /**
   * Remove a plugin factory from composed
   *
   * @param pluginRunner
   */
  default void removePluginRunner(ModuleRunner pluginRunner) {
    throw new IllegalStateException("this implementation not allow composed Plugin Factory");
  }

  default int counterFactories() {
    throw new IllegalStateException("this implementation not allow composed Plugin Factory");
  }

  /**
   * Discriminator, determina when applicate or not one factory from set one and only one
   *
   * @param targetPlugin
   * @return
   */
  Boolean isApplicable(Class targetPlugin);

  /**
   * getRunnerIdentifier , retorna un identifcador unico para el runner
   * @return
   */
  default ModuleRunnerID getRunnerIdentifier(){
    return ModuleRunnerID.of(this);
  }
}
