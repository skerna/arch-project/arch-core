package io.skerna.arch.core.spi;

import io.skerna.arch.core.ModuleManager;

/**
 * This interface define the base of all injectable service,
 * all external service searched through of ServiceLoader have to implement
 * this interface {@link BaseService}
 **/
public interface BaseService {

  /**
   * bind current instance of {@link ModuleManager} to service
   *
   * @param instance of type {@link ModuleManager}
   */
  void bind(ModuleManager instance);

}
