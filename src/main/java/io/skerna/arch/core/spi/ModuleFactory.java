package io.skerna.arch.core.spi;

import io.skerna.arch.core.Initializable;
import io.skerna.arch.core.Module;
import io.skerna.arch.core.ModuleWrapper;
import io.skerna.arch.core.vo.ModuleFactoryID;

/**
 * ModuleFactory, Define strategy
 * Creates a plugin instance.
 */
public interface ModuleFactory extends Initializable {

  /**
   * Create new Plugin
   *
   * @param moduleWrapper
   * @return Plugin
   */
  Module createModule(ModuleWrapper moduleWrapper) throws Exception;

  /**
   * Return boolean
   * Indicates if this factory is applicable to target class
   *
   * @param target
   * @return
   */
  boolean isApplicable(Class<?> target);

  /**
   * getFactoryIdentifier,returns {@link ModuleFactory} identifier
   * @return ModuleFactoryID
   */
  default ModuleFactoryID getFactoryIdentifier(){  return ModuleFactoryID.of(this); }



}
