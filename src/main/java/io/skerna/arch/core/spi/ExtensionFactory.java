package io.skerna.arch.core.spi;

/**
 * Creates an extension instance.
 */
public interface ExtensionFactory {

  Object create(Class<?> extensionClass) throws Exception;


  /**
   * Return boolean
   * Indicates if this factory is applicable to target class
   *
   * @param target
   * @return
   */
  boolean isApplicable(Class<?> target);

}
