package io.skerna.arch.core;


public interface Observable {

  void registerObserver(ModuleStateObserver observer);

  void removeObserve(ModuleStateObserver observer);
}
