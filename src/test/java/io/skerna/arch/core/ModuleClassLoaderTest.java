package io.skerna.arch.core;

import io.skerna.arch.core.factories.TestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class ModuleClassLoaderTest extends TestBase {
  private ModuleClassLoader moduleClassLoader;

  @BeforeEach
  void setUp() {
    ModuleDescriptor moduleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .build();
    this.moduleClassLoader = new ModuleClassLoader(moduleManager, moduleDescriptor, getClass().getClassLoader(), false);
  }

  @Test
  @DisplayName("Class should be loaded using all strategies")
  void loadClass() throws ClassNotFoundException {
    Class<?> clazz = moduleClassLoader.loadClass(ModuleTestA.class.getCanonicalName());
    assertNotNull(clazz);
  }


  @Test
  @DisplayName("ClassNotFoundException should be triggered when unknow class is loaded and this class" +
    "is not found in parent , local or dependencies classloader")
  void loadUnknowClass() {
    assertThrows(ClassNotFoundException.class, () -> {
      Class<?> clazz = moduleClassLoader.loadClass("com.example.class.unknow");
    });
  }

  @Test()
  @DisplayName("Attempt load class without jar in classloader, them add orgJSON to classloader and load " +
    "org.json.JSONObject")
  void loadJsonClassFromJarFile() throws ClassNotFoundException {
    String className = "org.json.JSONObject";
    assertThrows(ClassNotFoundException.class,()->{
      moduleClassLoader.loadClassFromLocal(className);
    }," Expected, class not found when jar is not found");
    // JAR FILE
    File file = new File("./src/test/resources/json.jar");
    this.moduleClassLoader.addFile(file);

    Class aclass = moduleClassLoader.loadClassFromLocal(className);
    assertNotNull(aclass,"Instance of type class should be returned");

  }

  @Test
  @DisplayName("Attempt load class from io.skerna.arch package")
  void loadClassUsinPacketArch() throws ClassNotFoundException {
    String className = "org.json.JSONObject";
    File file = new File("./src/test/resources/json.jar");
    this.moduleClassLoader.addFile(file);
    Class<?> aClazzRuntimeMode = moduleClassLoader.loadClass("io.skerna.arch.core.RuntimeMode");
    ClassLoader classLoader = aClazzRuntimeMode.getClassLoader();
    ClassLoader runtimeModeClassLoader = RuntimeMode.class.getClassLoader();
    assertEquals(classLoader,runtimeModeClassLoader, "Runtimemode is expected to be charged from app classloader");

    Class<?> aClazzJson = moduleClassLoader.loadClass(className);
    ClassLoader moduleClassLoader = aClazzJson.getClassLoader();
    assertNotEquals(moduleClassLoader,runtimeModeClassLoader);
    assertEquals(moduleClassLoader.getClass(),ModuleClassLoader.class);
  }
}
