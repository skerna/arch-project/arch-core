package io.skerna.arch.core;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RuntimeModeTest {

  @Test
  @DisplayName("Runtimemode, resolve by name")
  void byName() {
    RuntimeMode runtimeMode = RuntimeMode.byName("development");
    Assert.assertEquals(runtimeMode,RuntimeMode.DEVELOPMENT);

    runtimeMode = RuntimeMode.byName("dev");
    Assert.assertEquals(runtimeMode,RuntimeMode.DEVELOPMENT);

    runtimeMode = RuntimeMode.byName("deployment");
    Assert.assertEquals(runtimeMode,RuntimeMode.DEPLOYMENT);

    runtimeMode = RuntimeMode.byName("prod");
    Assert.assertEquals(runtimeMode,RuntimeMode.DEPLOYMENT);
  }


}
