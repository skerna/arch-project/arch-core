package io.skerna.arch.core.processor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;

/**
 * ReporterTest, this test check reporter message methods
 * using level NOTE , WARMING Y ERROR
 */
class ReporterTest {
  private static final String REPORTER_TEST_MESSAGE = "Mensaje test";
  private Reporter reporter;
  private Messager messager;

  @BeforeEach
  void setUp() {
    AbstractProcessor processorSpy = Mockito.spy(AbstractProcessor.class);
    ProcessingEnvironment processingEnvironment = Mockito.spy(ProcessingEnvironment.class);
    messager = Mockito.spy(Messager.class);
    Mockito.when(processingEnvironment.getMessager()).thenReturn(messager);
    reporter =  Reporter.get(processingEnvironment);
  }


  //@Test
  @DisplayName("When error message is invoked, diganostic expected type Kind.ERROR REPORTER_TEST_MESSAGE")
  void error() {
    reporter.error(REPORTER_TEST_MESSAGE);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR,REPORTER_TEST_MESSAGE);

  }

  //@Test
  void errorWithParamters() {
    reporter.error("Element %d",1);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR,"Element 1");


    reporter.error("Element %s-%s-%s","a","b","c");
    Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR,"Element a-b-c");

    reporter.error("Element %b",true);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR,"Element true");

    reporter.error("Element %d-%d",1,2);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.ERROR,"Element 1-2");
  }

  //@Test
  @DisplayName("When error message is invoked, diganostic expected type Kind.NOTE REPORTER_TEST_MESSAGE")
  void info() throws InterruptedException {
    Thread.sleep(1000);
    reporter.info("Element %d",1);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.NOTE,"Element 1");

    reporter.info("Element %s-%s-%s","a","b","c");
    Mockito.verify(messager).printMessage(Diagnostic.Kind.NOTE,"Element a-b-c");

    reporter.info("Element %b",true);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.NOTE,"Element true");

    reporter.info("Element %d-%d",1,2);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.NOTE,"Element 1-2");
  }


  // @Test
  void warnrWithParamters() {
    reporter.warn("Element %d",1);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.WARNING,"Element 1");


    reporter.warn("Element %s-%s-%s","a","b","c");
    Mockito.verify(messager).printMessage(Diagnostic.Kind.WARNING,"Element a-b-c");

    reporter.warn("Element %b",true);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.WARNING,"Element true");

    reporter.warn("Element %d-%d",1,2);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.WARNING,"Element 1-2");
  }

  void infoWithParamters() {
    reporter.info("Element %d",1);
    Mockito.verify(messager).printMessage(Diagnostic.Kind.NOTE,"Element 1");
  }
}
