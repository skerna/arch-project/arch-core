package io.skerna.arch.core.processor;

import com.google.common.base.Joiner;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourcesSubjectFactory;
import io.skerna.arch.core.processor.modules.ModuleProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.tools.JavaFileObject;
import java.net.URISyntaxException;
import java.util.Arrays;

class DefaultModuleProcessorTest {

  private final JavaFileObject input = JavaFileObjects.forSourceString(
    "io.skerna.bolt.sample.TestModule",
    Joiner.on("\n").join(
      "package io.skerna.bolt.sample;",
      "import io.skerna.arch.core.AbstractModule;",
      "import io.skerna.arch.core.ModuleDefinition;",
      "import io.skerna.common.logger.Logger;",
      "import io.skerna.common.logger.LoggerFactory;",
      "@ModuleDefinition",
      "class TestModuleJava extends AbstractModule {",
      "  Logger logger = LoggerFactory.logger(\"TestModule.class\");",
      "  @Override",
      "  public void start() throws Exception {",
      "    for (int i = 0; i < 100; i++) {",
      "      System.out.println(\"DO SOMETHING WITH JAVA \" + i);",
      "    }",
      "  }",
      " ",
      "}"
    )
  );

  @BeforeEach
  void setUp() {

  }

  @Test
  void adaptAnnotation() throws URISyntaxException {
    JavaFileObject javaFileObjectOut = JavaFileObjects.forSourceLines("");

    String result = Truth.assert_()
      .about(JavaSourcesSubjectFactory.javaSources())
      .that(Arrays.asList(input))
      .processedWith(new ModuleProcessor())
      .compilesWithoutError()
    .toString();

    System.out.println("_----------------------------->");
    //System.out.println(result.lines().collect(Collectors.joining()));

  }

}
