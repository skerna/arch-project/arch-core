package io.skerna.arch.core.finders;

import io.skerna.arch.core.ModuleDescriptor;
import io.skerna.arch.core.ReactorException;
import io.skerna.arch.core.processor.modules.DefaultModuleProcessor;
import io.skerna.arch.core.vo.SourceClasspathModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ClasspathModuleDescriptorFinderTest {
  private ClasspathModuleDescriptorFinder classpathModuleDescriptorFinder;

  @BeforeEach
  void setUp() {
    this.classpathModuleDescriptorFinder = new ClasspathModuleDescriptorFinder();
    this.classpathModuleDescriptorFinder.addModuleProcessor(new DefaultModuleProcessor());

  }

  @Test
  @DisplayName("Expected classpathModuleDescriptorFinder contains one or many descriptors")
  void find() throws ReactorException {
    Set<ModuleDescriptor> finderResult = classpathModuleDescriptorFinder.find(new SourceClasspathModule());
    for (ModuleDescriptor moduleDescriptor : finderResult) {
      System.out.println(moduleDescriptor);
    }
    assertTrue(finderResult.size() > 0);
  }

  @Test
  @DisplayName("Expected true, ClasspathModuleDescriptorFinder is only applicable to Constants.VIRTUAL_PATH")
  void isApplicable() {
    boolean isApplicable = classpathModuleDescriptorFinder.isApplicable(new SourceClasspathModule());
    assertTrue(isApplicable);

  }
}
