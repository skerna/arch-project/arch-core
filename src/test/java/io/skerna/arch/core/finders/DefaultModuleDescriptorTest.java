package io.skerna.arch.core.finders;

import io.skerna.arch.core.*;
import io.skerna.arch.core.factories.ModuleTestFactory;
import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.ModuleID;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DefaultModuleDescriptorTest {

  private DefaultModuleDescriptor defaultModuleDescriptor;

  @BeforeEach
  void setUp() {
    defaultModuleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .setModuleFactoryID(ModuleFactoryID.of(ModuleTestFactory.class))
      .setModuleClass(ModuleTestA.class)
      .setModuleDescription("Test Description A")
      .setLicence("MIT")
      .setPath(Constants.VIRTUAL_PATH)
      .setVersion("1.0.0")
      .addDependency(new ModuleDependecy(ModuleID.of(ModuleTestB.class)))
      .setProvider("SKERNA")
      .setModuleType(ModuleType.STANDARD)
      .setRequires("1.0.0")
      .build();
  }


  @Test
  void getModuleID() {
    Assert.assertEquals(defaultModuleDescriptor.getModuleID(),ModuleID.of(ModuleTestA.class));
  }

  @Test
  void getDescription() {
    Assert.assertEquals(defaultModuleDescriptor.getDescription(),"Test Description A");
  }

  @Test
  void getModuleClass() {
    Assert.assertEquals(defaultModuleDescriptor.getModuleClass(),ModuleTestA.class.getCanonicalName());
  }

  @Test
  void getVersion() {
    Assert.assertEquals(defaultModuleDescriptor.getVersion(),"1.0.0");
  }

  @Test
  void getRequires() {
    Assert.assertEquals(defaultModuleDescriptor.getRequires(),"1.0.0");
  }

  @Test
  void getProvider() {
    Assert.assertEquals(defaultModuleDescriptor.getProvider(),"SKERNA");
  }

  @Test
  void getLicense() {
    Assert.assertEquals(defaultModuleDescriptor.getLicense(),"MIT");
  }

  @Test
  void getDependencies() {
    for (ModuleDependecy dependency : defaultModuleDescriptor.getDependencies()) {
      Assert.assertEquals(dependency.getPluginId(),ModuleID.of(ModuleTestB.class));
    }
  }

  @Test
  void source() {
  }

  @Test
  void getModuleFactoryID() {
    Assert.assertEquals(defaultModuleDescriptor.getModuleFactoryID(),ModuleFactoryID.of(ModuleTestFactory.class));
  }

  @Test
  void getModuleRunnerID() {

  }

  @Test
  void getModuleType() {
    Assert.assertEquals(defaultModuleDescriptor.getModuleType(),ModuleType.STANDARD);
  }

}
