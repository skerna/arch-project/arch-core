package io.skerna.arch.core;

@ModuleDefinition()
class ModuleA extends AbstractModule {
}

@ModuleDefinition(dependecies = {
  @Dependency(moduleId = "io.skerna.arch.core.ModuleB"),
  @Dependency(moduleId = "io.skerna.arch.core.ModuleC")
})
class ModuleB extends AbstractModule {
}

@ModuleDefinition()
class ModuleC extends AbstractModule {
}

@ModuleDefinition()
class ModuleD extends AbstractModule {
}

@ModuleDefinition(dependecies = {
  @Dependency(moduleId = "io.skerna.arch.core.ModuleF"),
})
class ModuleE extends AbstractModule {
}

@ModuleDefinition()
class ModuleF extends AbstractModule {
}
