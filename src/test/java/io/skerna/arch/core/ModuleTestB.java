package io.skerna.arch.core;

@ModuleDefinition(
  dependecies = {@Dependency(moduleId = "io.skerna.arch.core.ModuleTestA")}
)
public class ModuleTestB extends AbstractModule {
  public ModuleTestB(String someParameter) {
  }

  void test(){
    System.out.println("Module test");
  }
}
