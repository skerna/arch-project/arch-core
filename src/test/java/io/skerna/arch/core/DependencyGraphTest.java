package io.skerna.arch.core;

import io.skerna.arch.core.finders.ClasspathModuleDescriptorFinder;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceClasspathModule;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DependencyGraphTest {
  private DependencyGraph dependencyGraph;
  private List<ModuleDescriptor> moduleDescriptors = new ArrayList<>();


  @Test
  void resolve() {
    ClasspathModuleDescriptorFinder classpathModuleDescriptorFinder = new ClasspathModuleDescriptorFinder();
    Set<ModuleDescriptor> descriptors = classpathModuleDescriptorFinder.find(new SourceClasspathModule());
    moduleDescriptors.addAll(descriptors);
    dependencyGraph = new DependencyGraph(new DefaultVersionManager());
    dependencyGraph.resolve(moduleDescriptors);
    List<ModuleID> dependecies = dependencyGraph.getDependencies(ModuleID.of(ModuleTestB.class));
    System.out.println("-> SIzE " + dependecies.size());
      assertEquals(true,dependecies.size()>0);
    for (ModuleID dependecy : dependecies) {
      System.out.println("---->  " + dependecy );
    }
    assertTrue(dependecies.contains(ModuleID.of(ModuleTestA.class)));
  }

  @Test
  void getDependencies() {
  }

  @Test
  void getDependents() {
  }

  @Test
  void checkDependencyVersion() {
  }
}
