package io.skerna.arch.core.vo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModuleIDTest {
  private ModuleID moduleID;
  @BeforeEach
  void setUp() {
    this.moduleID = ModuleID.of("test");
  }

  @Test
  void of() {
    String moduleID =null;
    assertThrows(IdentifierException.class,()->{
      ModuleID.of(moduleID);
    });
  }

  @Test
  void value() {
    assertEquals(moduleID,"test");
  }

  @Test
  void testEquals() {
    assertEquals(moduleID,"test");
  }

  @Test
  void testHashCode() {
    ModuleID moduleID1 = ModuleID.of("1");
    ModuleID moduleID2 = ModuleID.of("2");
    assertFalse(moduleID1.equals(moduleID2));
  }

  @Test
  void testToString() {
    ModuleID moduleID = ModuleID.of("test");
    assertTrue(moduleID.equals("test"));
  }

  @Test
  void compareTo() {
    ModuleID moduleID = ModuleID.of("test");
    int compareResult = moduleID.compareTo("test");
    assertTrue(compareResult == 0);
  }
}
