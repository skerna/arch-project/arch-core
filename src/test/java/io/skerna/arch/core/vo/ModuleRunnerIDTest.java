package io.skerna.arch.core.vo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModuleRunnerIDTest {
  private ModuleRunnerID moduleRunnerID;
  @BeforeEach
  void setUp() {
    this.moduleRunnerID = ModuleRunnerID.of("test");
  }


  @Test
  void value() {
    assertEquals(moduleRunnerID,"test");
  }

  @Test
  void testEquals() {
    assertEquals(moduleRunnerID,"test");
    assertNotEquals(moduleRunnerID,"test2");
  }

  @Test
  void testHashCode() {
    ModuleRunnerID ModuleRunnerID1 = ModuleRunnerID.of("1");
    ModuleRunnerID ModuleRunnerID2 = ModuleRunnerID.of("2");
    assertFalse(ModuleRunnerID1.equals(ModuleRunnerID2));
  }

  @Test
  void testToString() {
    ModuleRunnerID moduleRunnerID = ModuleRunnerID.of(("test"));
    assertTrue(moduleRunnerID.equals("test"));
  }

  @Test
  void compareTo() {
    ModuleRunnerID moduleRunnerID = ModuleRunnerID.of("test");
    int compareResult = moduleRunnerID.compareTo("test");
    assertTrue(compareResult == 0);
  }

}
