package io.skerna.arch.core;

import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ArchExceptionTest {

  /**
   * Test I18NExceptions Locale.ENGLISH
   */
  @Test
    void tesi18nExceptionEn(){
      Locale.setDefault(Locale.ENGLISH);
      ExpectedException expectedException = ExpectedException.none();
      expectedException.expect(ArchException.class);
      expectedException.expectMessage("Runtime Error");
      assertThrows(ArchException.class, () -> {
        ArchException targetException = new ArchException("RUNTIME_ERROR");
        throw  targetException;
      });
    }

  /**
   * Test I18NExceptions Locale.ES
   */
  @Test
  void tesi18nExceptionEs(){
    Locale.setDefault(Locale.forLanguageTag("ES"));
    ExpectedException expectedException = ExpectedException.none();
    expectedException.expect(ArchException.class);
    expectedException.expectMessage("Error tiempo de ejecucion");
    assertThrows(ArchException.class, () -> {
      ArchException targetException = new ArchException("RUNTIME_ERROR");
      throw  targetException;
    });
  }

}
