package io.skerna.arch.core;

import io.skerna.arch.core.factories.ModuleTestFactory;
import io.skerna.arch.core.factories.TestBase;
import io.skerna.arch.core.spi.ModuleFactory;
import io.skerna.arch.core.vo.SourceClasspathModule;
import io.skerna.arch.core.vo.SourceModuleType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;

class ModuleWrapperTest extends TestBase {
  private ModuleWrapper moduleWrapper;

  @BeforeEach
  void setUp() {
    DefaultModuleDescriptor descriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .build();
    ModuleFactory moduleFactory = new ModuleTestFactory();
    this.moduleWrapper = new ModuleWrapper(moduleManager,
      descriptor,
      new SourceClasspathModule(),
      getClass().getClassLoader(),
      moduleFactory,
      RuntimeMode.DEVELOPMENT,
      SourceModuleType.CLASSPATH_SOURCE_MODULE);
  }

  @Test
  void getModuleManager() throws InterruptedException {
    Thread.sleep(1000);
    Assert.assertNotNull(moduleWrapper.getModuleManager());
  }

  @Test
  void getDescriptor() {
    assertNotNull(moduleWrapper.getDescriptor());
  }

  //@Test
  void getModulePath() {
    Assertions.assertEquals(moduleWrapper.getSourceModule(), Constants.VIRTUAL_PATH);
  }

  @Test
  void getPluginClassLoader() throws InterruptedException {
    Thread.sleep(1000);
  }

  @Test
  void getModuleInstance() {
    assertNotNull(moduleWrapper.getModuleInstance());
  }

  @Test
  void getNewModuleInstance() {
    Module module = moduleWrapper.getNewModuleInstance();
    Module newModuleInstance = moduleWrapper.getNewModuleInstance();
    assertNotEquals(module,newModuleInstance);
  }

  @Test
  void getModuleClass() {
  }

  @Test
  void getModuleState() {
    assertEquals(moduleWrapper.getModuleState(), ModuleState.CREATED);
  }

  @Test
  void getRuntimeMode() throws InterruptedException {
    Thread.sleep(1000);
    assertEquals(moduleWrapper.getRuntimeMode(),RuntimeMode.DEVELOPMENT);
  }
  @Test
  void testEquals() {
  }
}
