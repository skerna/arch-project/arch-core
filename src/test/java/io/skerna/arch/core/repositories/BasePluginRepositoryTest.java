package io.skerna.arch.core.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BasePluginRepositoryTest {
  private BasePluginRepository basePluginRepository;

  @BeforeEach
  void setUp() {
  }

  @Test
  void setFilter() {
  }

  @Test
  void setComparator() {
  }

  @Test
  void getPluginPaths() {
  }

  @Test
  void deletePluginPath() {
  }
}
