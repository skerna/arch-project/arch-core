package io.skerna.arch.core;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ModulePathWatcherTest {
  private ModulePathWatcher modulePathWatcher;
  private ListenerPath listenerPath;
  private ExecutorService executorService;
  private Path pathWatch = Paths.get(System.getProperty("java.io.tmpdir"));
  private String fileName = "file.tmp";

  @BeforeEach
  void setUp() {

    this.executorService = Executors.newFixedThreadPool(2);
    this.listenerPath = new ListenerPath();
    this.modulePathWatcher =  ModulePathWatcher.builder()
      .addDirectory(pathWatch)
      .setExecutorService(executorService)
      .build(listenerPath);


  }

  @AfterEach
  void tearDown() {
    executorService.shutdownNow();
  }

  @Test
  void setListener() {
  }

  @Test
  void start() {
    this.modulePathWatcher.start();
  }

  @Test
  void stop() {
    this.modulePathWatcher.stop();
  }

  @Test
  void testFileChange() throws IOException, InterruptedException {

  }

  class ListenerPath implements ModulePathWatcher.Listener{
    private ModulePathWatcher.Event lastEvent;

    @Override
    public void onChange(ModulePathWatcher.Event event, Path dir, Path path) {
      this.lastEvent = event;
    }
  }
}
