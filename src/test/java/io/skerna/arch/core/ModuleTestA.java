package io.skerna.arch.core;

import io.skerna.arch.core.factories.DefaultModuleFactory;

@BuildWith(DefaultModuleFactory.class)
@ModuleDefinition
public class ModuleTestA extends AbstractModule {
  public ModuleTestA() {
  }

  void testAction(){
    System.out.println("TEST ACTION");
  }
}
