package io.skerna.arch.core.factories;

import io.skerna.arch.core.DefaultModuleManager;
import io.skerna.arch.core.ModuleManager;

public class TestBase {
  protected ModuleManager moduleManager;

  public TestBase() {
    moduleManager = new DefaultModuleManager(
      DefaultModuleManager.builder()
    );
  }
}
