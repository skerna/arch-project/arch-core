package io.skerna.arch.core.factories;

import io.skerna.arch.core.Module;
import io.skerna.arch.core.*;
import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.ModuleID;
import io.skerna.arch.core.vo.SourceClasspathModule;
import io.skerna.arch.core.vo.SourceModuleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultModuleFactoryManagerTest extends TestBase{
  private ModuleFactoryManager factoryManager;
  @BeforeEach
  void setUp() {
    this.factoryManager = new DefaultModuleFactoryManager(false);
    this.factoryManager.bindManager(moduleManager);
  }


  @Test
  void onAutoScanEnabled() {
    DefaultModuleFactoryManager defaultModuleFactoryManager = new DefaultModuleFactoryManager(false);
    defaultModuleFactoryManager.bindManager(moduleManager);
    assertEquals(0,this.factoryManager.countComponents(),"Scan disabled 0 factories");
  }

  @Test
  void countComponents() {
    factoryManager.register(new ModuleTestFactory());
    assertEquals(1,this.factoryManager.countComponents(),"Expected 1 factory");
  }

  @Test
  void installFactory() {
    factoryManager.register(new ModuleTestFactory());
    factoryManager.register(new ModuleTestFactory());
    factoryManager.register(new ModuleTestFactory());
    factoryManager.register(new ModuleTestFactory());
    assertEquals(1,this.factoryManager.countComponents(),"Only 1 factory of same type");
  }


  @Test
  void buildModuleType() throws Exception {
    factoryManager.register(new ModuleTestFactory());
    factoryManager.register(new DefaultModuleFactory());
    DefaultModuleDescriptor descriptor = DefaultModuleDescriptor.builder(ModuleID.of(ModuleTestA.class), ModuleTestA.class.getCanonicalName())
      .setModuleFactoryID(ModuleFactoryID.of(ModuleTestFactory.class))
      .build();
    ModuleWrapper moduleWrapper = new ModuleWrapper(moduleManager,descriptor, new SourceClasspathModule(),this.getClass().getClassLoader(),factoryManager, RuntimeMode.DEVELOPMENT, SourceModuleType.CLASSPATH_SOURCE_MODULE);
    Module instance = factoryManager.createModule(moduleWrapper);
    assertNotNull(instance,"Module instance should be not null");
  }

  @Test
  void buildModuleFail() throws Exception {
    factoryManager.register(new ModuleTestFactory());
    factoryManager.register(new DefaultModuleFactory());
    DefaultModuleDescriptor descriptor = DefaultModuleDescriptor.builder(ModuleID.of(ModuleTestB.class), ModuleTestB.class.getCanonicalName())
      .setModuleFactoryID(ModuleFactoryID.of(ModuleTestFactory.class))
      .build();
    ModuleWrapper moduleWrapper = new ModuleWrapper(moduleManager,descriptor, new SourceClasspathModule(),this.getClass().getClassLoader(),factoryManager, RuntimeMode.DEVELOPMENT, SourceModuleType.CLASSPATH_SOURCE_MODULE);
    assertThrows(ReporterFactoryException.class,()->{
      Module instance = factoryManager.createModule(moduleWrapper);
    });
  }
}
