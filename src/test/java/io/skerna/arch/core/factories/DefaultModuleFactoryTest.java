package io.skerna.arch.core.factories;

import io.skerna.arch.core.Module;
import io.skerna.arch.core.*;
import io.skerna.arch.core.vo.ModuleFactoryID;
import io.skerna.arch.core.vo.SourceClasspathModule;
import io.skerna.arch.core.vo.SourceModuleType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DefaultModuleFactoryTest extends TestBase {

  private DefaultModuleFactory moduleFactory;

  @BeforeEach
  void setUp() {
    this.moduleFactory = new DefaultModuleFactory();
    moduleFactory.bindManager(moduleManager);
  }

  @Test
  public void testCreateMethod() {
    DefaultModuleDescriptor moduleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .setModuleFactoryID(ModuleFactoryID.of(DefaultModuleFactory.class))
      .build();

    ModuleWrapper moduleWrapper = new ModuleWrapper(moduleManager, moduleDescriptor, new SourceClasspathModule(), this.getClass().getClassLoader(), moduleFactory, RuntimeMode.DEVELOPMENT,SourceModuleType.CLASSPATH_SOURCE_MODULE);
    Module moduleInstance = moduleWrapper.getModuleInstance();
    assertNotNull(moduleInstance, "Module instance should be created");
  }

  @Test
  public void testCreateMethodUnknow() {
    DefaultModuleDescriptor moduleDescriptor = DefaultModuleDescriptor.builder(ModuleTestB.class)
      .setModuleFactoryID(ModuleFactoryID.of(DefaultModuleFactory.class))
      .build();

    ModuleWrapper moduleWrapper = new ModuleWrapper(moduleManager, moduleDescriptor,new SourceClasspathModule(), this.getClass().getClassLoader(), moduleFactory, RuntimeMode.DEVELOPMENT, SourceModuleType.CLASSPATH_SOURCE_MODULE);
    assertThrows(FactoryException.class, () -> {
      Module moduleInstance = moduleWrapper.getModuleInstance();
      assertNotNull(moduleInstance, "Module instance should be created");
    });

  }


  @Test
  public void testCreateMethodA() {
    DefaultModuleDescriptor moduleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .setModuleFactoryID(ModuleFactoryID.of(DefaultModuleFactory.class))
      .build();

//    ModuleWrapper moduleWrapper = new ModuleWrapper(moduleManager, moduleDescriptor, moduleFactory, RuntimeMode.DEVELOPMENT);
//    assertThrows(FactoryException.class, () -> {
//      Module moduleInstance = moduleWrapper.getModuleInstance();
//      assertNotNull(moduleInstance, "Module instance should be created");
//    });

  }
}
