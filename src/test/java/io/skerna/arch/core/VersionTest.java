package io.skerna.arch.core;

import com.github.zafarkhaja.semver.Version;
import com.vdurmont.semver4j.Semver;

public class VersionTest {

  void test(){
    String sv1 = "28.1-android";
    String sv2 = "28.2-jre";
    Semver sem1 = new Semver(sv1, Semver.SemverType.NPM); // Defaults to STRICT mode
    Semver sem2 = new Semver(sv2, Semver.SemverType.NPM); // Specify the mode

    Semver.VersionDiff result = sem1.diff(sem2);
    System.out.println(result);

    Version v1 = Version.valueOf(sv1);
    Version v2 = Version.valueOf(sv2);

    int resultAware = Version.BUILD_AWARE_ORDER.compare(v1, v2);  // < 0

    int resultCompare     = v1.compareTo(v2);            // = 0
    boolean resultEquals = v1.equals(v2);               // true
    int resultCompareWithBuild     = v1.compareWithBuildsTo(v2);  // < 0


    System.out.println(resultAware);
    System.out.println(resultCompare);
    System.out.println(resultEquals);
    System.out.println(resultCompareWithBuild);

    System.out.println("---------------");
    System.out.println(v2.compareWithBuildsTo(v1));
  }

}
