package io.skerna.arch.core.loaders;

import io.skerna.arch.core.DefaultModuleDescriptor;
import io.skerna.arch.core.ModuleClassLoader;
import io.skerna.arch.core.ModuleTestA;
import io.skerna.arch.core.factories.TestBase;
import io.skerna.arch.core.vo.SourceFileModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JarPluginLoaderTest extends TestBase {

  private JarPluginLoader jarPluginLoader;
  private String JAR_FILE_PATH = "./src/test/resources/json.jar";
  @BeforeEach
  void setUp() {
    this.jarPluginLoader = new JarPluginLoader();
    this.jarPluginLoader.bindManager(moduleManager);
  }

  @Test
  @DisplayName("Expected true when jar path is specified")
  void isApplicable() throws InterruptedException {
    Thread.sleep(5000);
    DefaultModuleDescriptor defaultModuleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .build();
    Boolean isApplicable = jarPluginLoader.isApplicable(new SourceFileModule(Path.of(JAR_FILE_PATH)));
    assertTrue(isApplicable,"jar plugin loader expected is applicable");
  }

  @Test
  void loadPlugin() throws ClassNotFoundException, InterruptedException {
    Thread.sleep(5000);
    String className = "org.json.JSONObject";
    DefaultModuleDescriptor defaultModuleDescriptor = DefaultModuleDescriptor.builder(ModuleTestA.class)
      .build();
    ModuleClassLoader moduleClassLoader = (ModuleClassLoader) jarPluginLoader.loadPlugin(new SourceFileModule(Paths.get(JAR_FILE_PATH)),defaultModuleDescriptor);
    Class aClassJsonObject = moduleClassLoader.loadClass(className);
    assertNotNull(aClassJsonObject);
  }
}
