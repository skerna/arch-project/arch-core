package io.skerna.arch.core.loaders;

import io.skerna.arch.core.factories.TestBase;
import io.skerna.arch.core.vo.SourceFileModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultPluginLoaderManangerTest extends TestBase {
  private File file;
  private DefaultPluginLoaderMananger loaderMananger;

  @BeforeEach
  void setUp() throws URISyntaxException {
    this.loaderMananger = new DefaultPluginLoaderMananger();
    this.file = new File("./src/test/resources/json.jar");
  }

  @Test
  @DisplayName("when new classLoader is registered, size classloaders manager is increased")
  void add() {
    this.loaderMananger.add(new JarPluginLoader());
    assertTrue(loaderMananger.size()>0);
  }

  @Test
  @DisplayName("when new classLoader is registered, size classloaders manager is increased")
  void size() {
    this.loaderMananger.add(new JarPluginLoader());
    assertTrue(loaderMananger.size()>0);
  }

  @Test
  void isApplicable() {
    this.loaderMananger.add(new JarPluginLoader());
    boolean isApplicable = loaderMananger.isApplicable(new SourceFileModule(file.toPath()));
    assertTrue(isApplicable);
  }

  @Test
  void loadPlugin() {
  }

  @Test
  @DisplayName("No class loader was registed for externar file")
  void hasClassloaderForpath() {
    boolean hasClassLaoder  = this.loaderMananger.hasClassloaderForpath(new SourceFileModule(file.toPath()));
    assertFalse(hasClassLaoder);
  }

  @Test
  @DisplayName("class loader was registed for externar file")
  void hasClassloaderForpathUsingExt() {
    JarPluginLoader jloader = new JarPluginLoader();
    this.loaderMananger.add(jloader);
    boolean hasClassLaoder  = this.loaderMananger.hasClassloaderForpath(new SourceFileModule(file.toPath()));
    assertFalse(hasClassLaoder);
  }


  @Test
  void removeClassLoader() {
  }

  @Test
  void testRemoveClassLoader() {
  }
}
