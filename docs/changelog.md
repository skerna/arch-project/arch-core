# Changes

- Introduce forkjoin as Graph  Module Runner
- Introduce Futures as Async actions
- Cacheable Factories 
- Add ClassIndex Annotation processor, default @Annotations
- Replace compound by Managers
- Add JSON finder Modules
- Add VO critical module identifiers
- Add JSON mmultimodule Metadata information
- Modification Module class Path, allow load modules using AppClasspath
- Extensible Module Factories (META-INF/services)
- Extensible Module Runners(META-INF/services)
- Add Filewatcher auto install Modules from directory
- Allow multiple Modules peer JAR OR ZIP
- Add @Module annotation everywhere
- Rearrange package to improve read code
- Improve annotation processor
- Replace Slf4j by Skerna Logger

