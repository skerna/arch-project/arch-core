# SKERNA ARCH
![Gitter](https://img.shields.io/gitter/room/sk3rna/community)

ARCH is the core to create extensible applications, 
the idea is to use it with vertices (VERTX) to create modules with auto deploy and define dependencies among them, 
without worry about being deployed each reactiveModule


![BOLT](./docs/Untitled%20Diagram.png)

## Nota


The project was a fork of pf4j, although certain components are maintained, many parts were also rewritten.
Este proyecto originalmente fue un fork de pf4j [muchas gracias :), por la base original de código ], yo tambien puse mi granito con
algunos cambios y mejoras para adaptarlos a mis necesidades, los cambios puedes encontrarlos [aqui](./docs/changelog.md)

## Nota

Desde que se introdujeron muchos cambios, las pruebas esta rotas, asi que estmos
en proceso para reescribir estas con junit 5

## ROADMAP 

- Rewrite test using junit 5
- Improve runners code
- Improve Vertx Integration

# Changes V 0.1.3
- Dev TEST suite , add 74 test cases
- Introduce forkjoin as Graph  Module Runner
- Introduce Futures as Async actions
- Cacheable Factories 
- Add ClassIndex Annotation processor, default @Annotations
- Replace compound by Managers
- Add JSON finder Modules
- Add VO critical module identifiers
- Add JSON mmultimodule Metadata information
- Replace Module metadata by JSON metadescriptor
- Modification Module class Path, allow load modules using AppClasspath
- Extensible Module Factories (META-INF/services)
- Extensible Module Runners(META-INF/services)
- Add Filewatcher auto install Modules from directory
- Allow multiple Modules peer JAR OR ZIP
- Add @Module annotation everywhere
- Rearrange package to improve read code
- Improve annotation processor
- Rename Plugin by Module (only conceptual approach)
- Replace Slf4j by Skerna Logger
