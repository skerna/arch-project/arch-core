pluginManagement {
  val kotlinVersion: String by settings
  val version: String by settings

  repositories {
    mavenLocal()
    google()
    gradlePluginPortal()
    mavenCentral()
    jcenter()
  }
  plugins {
    id("org.jetbrains.kotlin.jvm").version(kotlinVersion)
    id("io.skerna.libs.gradle.base").version("1.0.0")
  }
  pluginManagement {
    includeBuild("../gradle-plugins/plugin-build")
  }
  resolutionStrategy {
    eachPlugin {
      if (requested.id.id == "io.skerna.libs.gradle.base") {
        useModule("io.skerna.libs.gradle:library-base:1.0.0")
      }
    }
  }
}

dependencyResolutionManagement {
  repositories {
    maven {
      setUrl("https://skerna.jfrog.io/artifactory/skerna-gradle-dev-local")
    }
    google()
    gradlePluginPortal()
    mavenCentral()
    jcenter()
    mavenLocal()
  }
}

//includeBuild("../gradle-plugins/plugin-build")
if (file(".composite").exists()) {
  includeBuild("../arch-platform") {
    dependencySubstitution {
      substitute(module("io.skerna.arch:arch-platform")).with(project(":"))
    }
  }
}
includeBuild("../gradle-plugins/plugin-build")